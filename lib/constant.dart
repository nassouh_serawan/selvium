import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kInputContainerDecoration = InputDecoration(
  errorStyle: TextStyle(color: Colors.red),
  errorMaxLines: 2,
  hintStyle: TextStyle(
      fontFamily: 'Kufam',
      fontWeight: FontWeight.normal,
      color: Color(0xFFA48C57)),
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(
        color: Color(0x00A48C57), width: 1.0, style: BorderStyle.none),
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
  errorBorder: OutlineInputBorder(
    borderSide: BorderSide(
        color: Color(0x00A48C57), width: 1.0, style: BorderStyle.none),
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
        color: Color(0x00A48C57), width: 1.0, style: BorderStyle.none),
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
  disabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
        color: Color(0x00A48C57), width: 1.0, style: BorderStyle.none),
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(
        color: Color(0x00A48C57), width: 1.0, style: BorderStyle.none),
    borderRadius: BorderRadius.all(Radius.circular(2.0)),
  ),
);
var kContainerDecoration = ShapeDecoration(
  color: Colors.white,
  shape: RoundedRectangleBorder(
    side: BorderSide(
        color: Colors.grey.withOpacity(0.5),
        width: 1.0,
        style: BorderStyle.solid),
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
  ),
);
var kContainerDecoration5 = ShapeDecoration(
  color: Color(0x0fff0000),
  shape: RoundedRectangleBorder(
    side: BorderSide(color: Colors.black, width: 1.0, style: BorderStyle.solid),
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
  ),
);
var kContainerDecoration6 = BoxDecoration(
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3), // changes position of shadow
    ),
  ],
  shape: BoxShape.rectangle,
  color: Colors.red,
  borderRadius: BorderRadius.all(Radius.circular(15.0)),
);
var kContainerDecoration7 = BoxDecoration(
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 5,
      blurRadius: 7,
      offset: Offset(0, 3), // changes position of shadow
    ),
  ],
  shape: BoxShape.rectangle,
  color: Colors.white,
  borderRadius: BorderRadius.all(Radius.circular(15.0)),
);
const kContainerDecoration2 = ShapeDecoration(
  color: Colors.white,
  shape: RoundedRectangleBorder(
    side: BorderSide(color: Colors.black, width: 1.0, style: BorderStyle.none),
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
);
const kContainerDecoration3 = ShapeDecoration(
  color: Colors.white,
  shape: RoundedRectangleBorder(
    side: BorderSide(color: Colors.black, width: 1.0, style: BorderStyle.solid),
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
  ),
);
const kContainerDecoration4 = ShapeDecoration(
  color: Colors.white,
  shape: RoundedRectangleBorder(
    side: BorderSide(color: Colors.grey, width: 1.0, style: BorderStyle.solid),
    borderRadius: BorderRadius.all(Radius.circular(15.0)),
  ),
);
var kContainerButtonDecoration = BoxDecoration(
    gradient: LinearGradient(colors: <Color>[
      kColor2,
      kColor1,
    ]),
    border:
        Border.all(color: Colors.black, width: 0.5, style: BorderStyle.solid),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ));
var kContainerButtonDecoration2 = BoxDecoration(
    color: Colors.white,
    border:
        Border.all(color: Colors.black, width: 0.5, style: BorderStyle.solid),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ));
// const kBackgroundColor = Color(0xfff4f7fa);
const kBackgroundColor = Colors.white;
// const kColor1 = Color(0xffFF3D3D);
const kColor1 = Color(0xffF34949);
const kColor2 = Color(0xffC71E5A);
const viewAll = Color(0xffC71E5A);

const kHintTextStyle = TextStyle(
    fontFamily: 'Cairo',
    fontWeight: FontWeight.w400,
    color: Colors.grey,
    fontSize: 15);
const kTitleTextStyle = TextStyle(
    fontFamily: 'Cairo',
    fontWeight: FontWeight.w500,
    color: Colors.black,
    fontSize: 18);
const kTitleTextStyle2 = TextStyle(
    fontFamily: 'Cairo',
    fontWeight: FontWeight.w100,
    color: Colors.white,
    fontSize: 18);
const kTitleTextStyle3 = TextStyle(
    fontFamily: 'Cairo',
    fontWeight: FontWeight.w700,
    color: Colors.white,
    fontSize: 20);
const kTitleTextStyle4 = TextStyle(
    fontFamily: 'Cairo',
    fontWeight: FontWeight.w100,
    color: Colors.black,
    fontSize: 13);
const kTitleTextStyle5 = TextStyle(
    fontFamily: 'Cairo',
    fontWeight: FontWeight.w100,
    color: Colors.black,
    fontSize: 18);
const baseUrl = "www.selvium.com";
const ck = 'consumer_key';
const cs = 'consumer_secret';
const ckCode = 'ck_0054e08913b236f3d2a2432da8531cef295154fa';
const csCode = 'cs_b2b4bf51b6518dc1b4c4a9d6553eac1b9d84dfd0';
