import 'package:selvium/home_screen.dart';
import 'package:selvium/sign_in_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'constant.dart';
import 'package:selvium/services/api.dart';
import 'widgets/network_widget.dart';
import 'package:flushbar/flushbar.dart';
class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool problem=false;
  String message;
  bool loading=false;
  bool _autoValidate = false;
  ApiProvider _provider = ApiProvider();
  String validateName(value) {
    if (value.length < 1)
      return 'الحقل مطلوب';
    else
      return null;
  }
  String validatePass(value){
    if (value.length < 6)
      return 'كلمة السر يجب ان تكون أكثر من 6 أحرف';
    else
      return null;
  }
  String validateEmail(value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'الرجاء ادخال عنوان بريد الكتروني صالح';
    else
      return null;
  }
  Flushbar flush;
  final TextEditingController _user = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pass = TextEditingController();
  register()async{
    if(_formKey.currentState.validate()){
      _formKey.currentState.save();
      setState(() {
        loading=true;
      });
      var data = {
        "username":_user.text,
        "email":_email.text,
        "password":_pass.text
      };
      try{
        final response = await _provider.register(
            "wp-json/wc/v2/customers", {ck: ckCode, cs: csCode}, data);
        if (response['id'] != null) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => SignInScreen()));
          flush = Flushbar(
              title: 'أهلا وسهلا بك في سيلفيوم',
              message: 'الرجاء تسجيل دخولك بحسابك الجديد',
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.green,
              boxShadows: [
                BoxShadow(
                  color: Colors.green[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        } else {
          setState(() {
            problem = true;
            response['code'].toString() == 'registration-error-email-exists'
                ? message = response['message'].toString().substring(0, 41)
                : message = response['message'].toString();
            loading = false;
          });
        }
      }catch(e){
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: e.toString(),
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading=false;
        });
      }
    }
    else{setState(() {
      _autoValidate = true;
    });}

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: problem?Colors.red:Color(0xffffffff),
        title: problem? Row(
          mainAxisAlignment:MainAxisAlignment.start,
          children: [
            Text(message,textAlign:TextAlign.center, style: kTitleTextStyle2),

          ],
        ):Row(
          mainAxisAlignment:MainAxisAlignment.spaceBetween ,
          children: [
            Text('التسجيل',textAlign:TextAlign.center, style: kTitleTextStyle,),
            FlatButton(
                onPressed:(){Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>SignInScreen()));} ,
                child: Text('تسجيل الدخول',textAlign:TextAlign.center, style: kTitleTextStyle,)),
          ],
        ),
      ),
      body: loading?Loading(loadingMessage: 'الرجاء الانتظار'):Container(
        height:
        MediaQuery.of(context).orientation==Orientation.portrait?
        MediaQuery.of(context).size.height>800?
        MediaQuery.of(context).size.height*0.47:
        MediaQuery.of(context).size.height*0.7:
        MediaQuery.of(context).size.height>800?
        MediaQuery.of(context).size.height*0.75:
        MediaQuery.of(context).size.height*0.85,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          color: Color(0xffffffff),
          borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
        ),
        child:SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('أهلا بك في سيلفيوم',style: kTitleTextStyle.copyWith(fontSize: 30,fontWeight: FontWeight.w700),),
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*0.003,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('من فضلك قم بتسجيل حساب جديد من هنا',style: kTitleTextStyle,),
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*0.003,),
                      Form(
                        key: _formKey,
                        child:Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                height: 55,
                                decoration: kContainerDecoration,
                                child: TextFormField(
                                  decoration: kInputContainerDecoration.copyWith(hintText:"اسم المستخدم" ,hintStyle:kHintTextStyle,errorStyle: TextStyle(fontSize: 10)  ),
                                  validator: validateName,
                                  onSaved: (value) {
                                    _user.text=value;
                                  },
                                  controller: _user,
                                )
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height*0.005,),
                            Container(
                                height: 55,
                                decoration: kContainerDecoration,
                                child: TextFormField(
                                  decoration: kInputContainerDecoration.copyWith(hintText:"االبريد الاكتروني" ,hintStyle:kHintTextStyle,errorStyle: TextStyle(fontSize: 10) ),
                                  validator: validateEmail,
                                  onSaved: (value) {
                                    _email.text=value;
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  controller: _email,
                                )
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height*0.005,),
                            Container(
                                height: 55,
                                decoration: kContainerDecoration,
                                child: TextFormField(
                                  decoration: kInputContainerDecoration.copyWith(hintText:"كلمة السر" ,hintStyle:kHintTextStyle, errorStyle: TextStyle(fontSize: 10) ),
                                  validator: validatePass,
                                  onSaved: (value) {
                                    _pass.text=value;
                                  },
                                  obscureText: true,
                                  controller: _pass,
                                )
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*0.03,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FloatingActionButton(
                              child: Container(
                                  width: double.infinity,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          kColor2,kColor1
                                        ]),
                                  ),
                                  child: Icon(Icons.arrow_forward)),
                              onPressed: register,
                            ),
                          ),

                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ) ,
      ),
    );
  }
}
