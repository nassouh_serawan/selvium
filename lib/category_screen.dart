import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:selvium/perfume_screen.dart';
import 'package:selvium/search_screen.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'constant.dart';
import 'package:badges/badges.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/cupertino.dart';
import 'package:selvium/models/product.dart';
import 'package:selvium/bloc/brand_product_bloc.dart';
import 'package:selvium/repository/brand_product_repo.dart';
import 'package:selvium/bloc/event.dart';
import 'package:selvium/bloc/product_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selvium/services/api.dart';
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

var userId;

class CategoryScreen extends StatefulWidget {
  final int categoryId;
  final String categoryName;
  CategoryScreen(this.categoryId, this.categoryName);
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  double high = kToolbarHeight;
  bool search = false;
  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    setState(() {
      userId = jsonDecode(localStorage.getString('userId'));
    });
  }

  @override
  void initState() {
    getUser();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BrandProductBloc(
        parameter1: '_',
        value1: '_',
        parameter2: 'category',
        value2: widget.categoryId.toString(),
        brandProductRepository: BrandProductRepository(),
      )..add(FetchEvent()),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          leading: IconButton(
              icon: SvgPicture.asset(
                'images/BackArrow-White-icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.pop(context);
              }),
          actions: [
            IconButton(
              icon: SvgPicture.asset(
                'images/Search-white-icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              },
            ),
          ],
          title: Text(
            'التصنيفات',
            style: kTitleTextStyle3,
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[kColor2, kColor1])),
          ),
        ),
        body: ProductBody(widget.categoryName),
      ),
    );
  }
}

class ProductBody extends StatelessWidget {
  final List<Product> _products = [];
  final ScrollController _scrollController = ScrollController();
  final String categoryName;
  ProductBody(this.categoryName);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocConsumer<BrandProductBloc, ProductState>(
        listener: (context, productState) {
          if (productState is ProductLoadingState) {
            //Scaffold.of(context)
            //  .showSnackBar(SnackBar(content: Text(productState.message)));
          } else if (productState is ProductSuccessState &&
              productState.products.isEmpty) {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text('لا مزيد من المنتجات')));
          } else if (productState is ProductErrorState) {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text(productState.error)));
            context.read<BrandProductBloc>().isFetching = false;
          }
          return;
        },
        builder: (context, productState) {
          if (productState is ProductInitialState ||
              productState is ProductLoadingState && _products.isEmpty) {
            return Loading(
              loadingMessage: 'جاري تحميل المنتجات',
            );
          } else if (productState is ProductSuccessState) {
            _products.addAll(productState.products);
            context.watch<BrandProductBloc>().isFetching = false;
            Scaffold.of(context).hideCurrentSnackBar();
          } else if (productState is ProductErrorState && _products.isEmpty) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    context.read<BrandProductBloc>()
                      ..isFetching = true
                      ..add(FetchEvent());
                  },
                  icon: Icon(Icons.refresh),
                ),
                const SizedBox(height: 15),
                Text(
                  productState.error,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red, fontSize: 9),
                ),
              ],
            );
          }
          return Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    '   التصنيفات - ' + categoryName,
                    style: kTitleTextStyle,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              Flexible(
                child: GridView.builder(
                  controller: _scrollController
                    ..addListener(() {
                      if (_scrollController.offset ==
                              _scrollController.position.maxScrollExtent &&
                          !context.read<BrandProductBloc>().isFetching) {
                        context.read<BrandProductBloc>()
                          ..isFetching = true
                          ..add(FetchEvent());
                      }
                    }),
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? 3
                          : 4,
                      childAspectRatio: 0.7),
                  itemBuilder: (context, index) =>
                      ProductListItem(_products[index]),
                  itemCount: _products.length,
                ),
              ),
              context.watch<BrandProductBloc>().isFetching
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Loading(
                        loadingMessage: ' تحميل المزيد من المنتجات',
                      ),
                    )
                  : Container(),
            ],
          );
        },
      ),
    );
  }
}

class ProductListItem extends StatelessWidget {
  final Product product;

  const ProductListItem(this.product);
  addToWish(int id, String price, var context) async {
    Flushbar flush;
    ApiProvider _provider = ApiProvider();
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'addWishItem',
      'prod_id': '$id',
      'user_id': userId.toString(),
      'wish_id': '1',
      'quantity': '1',
      'original_price': '$price',
      'original_currency': 'SAR'
    };
    try {
      final response = await _provider.get("CustomAPI.php", queryParameters);
      if (response == 1) {
        flush = Flushbar(
            titleText: Text(
              'تمت الاضافة للمفضلة',
              style: TextStyle(color: Colors.green),
            ),
            messageText: Text(
              'تم اضافة عنصر جديد الى المفضلة بنجاح',
              style: TextStyle(color: Colors.green),
            ),
            // showProgressIndicator: true,
            duration: Duration(seconds: 4),
            // leftBarIndicatorColor: viewAll,
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.green,
            backgroundColor: Colors.white,
            // backgroundGradient: LinearGradient(
            //         begin: Alignment.centerLeft,
            //         end: Alignment.centerRight,
            //         colors: <Color>[kColor2, kColor1]),
            boxShadows: [
              BoxShadow(
                color: Colors.green,
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.green,
              ),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else if (response == 2) {
        flush = Flushbar(
            titleText: Text(
              'تم الحذف من المفضلة',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'تم حذف العنصر من المفضلة بنجاح',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.redAccent.shade700,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            titleText: Text(
              'حدث خطأ ما',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'الرجاء المحاولة مرة أخرى',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.red,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => PerfumeScreen(product.id)));
      },
      child: Column(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: viewAll, width: 1.0, style: BorderStyle.solid),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              // borderRadius: BorderRadius.circular(5.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? MediaQuery.of(context).size.height > 900
                              ? MediaQuery.of(context).size.height * 0.22
                              : MediaQuery.of(context).size.height * 0.12
                          : MediaQuery.of(context).size.width > 700
                              ? MediaQuery.of(context).size.width * 0.17
                              : MediaQuery.of(context).size.width * 0.12,
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: product.images[0].src == null
                      ? Image.asset("images/p3.png")
                      : Image.network(product.images[0].src),
                ),
                GestureDetector(
                  onTap: () {
                    addToWish(product.id, product.price, context);
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Icon(
                              CupertinoIcons.heart,
                              color: viewAll,
                            ),
                          ),
                        ],
                      )),
                ),
              ],
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Text(
                product.name.length > 12
                    ? product.name.substring(0, 12) + '...'
                    : product.name,
                style: TextStyle(
                  fontSize: 10,
                ),
                textAlign: TextAlign.center,
              )),
          Container(
              width: MediaQuery.of(context).size.width * 0.23,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    product.price + ' ر.س',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w500,
                        color: viewAll),
                  ),
                  product.regularPrice != ''
                      ? Text(
                          product.regularPrice + ' ر.س',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: MediaQuery.of(context).size.height > 850
                                  ? 10
                                  : 7,
                              decoration: TextDecoration.lineThrough,
                              color: Colors.red),
                        )
                      : Text(''),
                ],
              ))
        ],
      ),
    );
  }
}
