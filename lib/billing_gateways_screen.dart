import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:selvium/bloc/payment_bloc.dart';
import 'package:selvium/home_screen.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'map_screen.dart';
import 'package:selvium/models/payment.dart';
import 'package:selvium/models/cupon.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/models/shipping.dart';
import 'package:selvium/models/item.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/widgets/network_widget.dart';
class BillingGatewayScreen extends StatefulWidget {
  @override
  _BillingGatewayScreenState createState() => _BillingGatewayScreenState();
}

class _BillingGatewayScreenState extends State<BillingGatewayScreen> {
  PaymentBloc _PBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _PBloc = PaymentBloc();

  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _PBloc.dispose();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        leading:IconButton(icon: SvgPicture.asset('images/BackArrow-White-icon.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
        title: Text('الدفع',style: kTitleTextStyle3,),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[
                    kColor2,kColor1
                  ])
          ),
        ),
      ),
      body:Card(shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width*0.95,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text('طرق الدفع',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),),
                            ],
                          ),
                          RefreshIndicator(
                            onRefresh: () => _PBloc.fetchPayment(),
                            child: StreamBuilder<Response<Payments>>(
                              stream: _PBloc.paymentListStream,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  switch (snapshot.data.status) {
                                    case Status.LOADING:
                                      return Loading(loadingMessage: snapshot.data.message);
                                      break;
                                    case Status.COMPLETED:
                                      return PaymentList(payments: snapshot.data.data);
                                      break;
                                    case Status.ERROR:
                                      return Error(
                                        errorMessage: snapshot.data.message,
                                        onRetryPressed: () => _PBloc.fetchPayment(),
                                      );
                                      break;
                                  }
                                }
                                return Container();
                              },
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
    );
  }
}
class PaymentList extends StatefulWidget {
  final Payments payments;
  const PaymentList({Key key, this.payments}) : super(key: key);
  @override
  _PaymentListState createState() => _PaymentListState();
}

class _PaymentListState extends State<PaymentList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.payments.payments.length,
        itemBuilder: (BuildContext context, int index) =>Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.only(left: 10),
                    width: MediaQuery.of(context).size.width*0.09,
                    height: MediaQuery.of(context).size.height*0.055,
                    child: Image.asset('images/visa.png')),
                Text(widget.payments.payments[index].title,style: TextStyle(fontSize: 15,fontWeight: FontWeight.w700),),
              ],
            ),
          ],
        ));
  }
}
