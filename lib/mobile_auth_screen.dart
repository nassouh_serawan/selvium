import 'package:flutter/material.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_screen.dart';
class MobileAuthScreen extends StatefulWidget {
  @override
  _MobileAuthScreenState createState() => _MobileAuthScreenState();
}

class _MobileAuthScreenState extends State<MobileAuthScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
          leading:IconButton(icon: SvgPicture.asset('images/Back-Arrow-Black.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title:Text('التسجيل',style: kTitleTextStyle)
      ),
      body: Container(
        height:
        MediaQuery.of(context).orientation==Orientation.portrait?
        MediaQuery.of(context).size.height>800?
        MediaQuery.of(context).size.height*0.5:
        MediaQuery.of(context).size.height*0.6:
        MediaQuery.of(context).size.width>800?
        MediaQuery.of(context).size.width*0.9:
        MediaQuery.of(context).size.width*0.4,

        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          color: Color(0xffffffff),
          borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
        ),
        child:SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child:
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(height:
                      MediaQuery.of(context).size.height>900?
                      MediaQuery.of(context).size.height*0.06:
                      MediaQuery.of(context).size.height*0.015,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('الاتصال بالموبايل',style: TextStyle(fontWeight: FontWeight.w700,fontSize: 20),)
                      ],
                    ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('توثيق الاتصال بالموبايل اونلاين',style: TextStyle(fontWeight: FontWeight.w100,fontSize: 15),)
                        ],
                      ),
                    Container(
                        decoration: kContainerDecoration,
                        child: TextFormField(decoration: kInputContainerDecoration.copyWith(hintText:"الاسم" ,hintStyle:kHintTextStyle ),)),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Container(
                        decoration: kContainerDecoration,
                        child: TextFormField(decoration: kInputContainerDecoration.copyWith(hintText:"االبريد الاكتروني" ,hintStyle:kHintTextStyle ),)),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Container(
                        decoration: kContainerDecoration,
                        child: TextFormField(decoration: kInputContainerDecoration.copyWith(hintText:"NIC/Passport Number" ,hintStyle:kHintTextStyle ),)),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Container(
                        decoration: kContainerDecoration,
                        child: TextFormField(decoration: kInputContainerDecoration.copyWith(hintText:"NIC/Passport Number" ,hintStyle:kHintTextStyle, ),)),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FloatingActionButton(
                            child: Container(
                                width: double.infinity,
                                height: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        kColor2,kColor1
                                      ]),
                                ),
                                child: Icon(Icons.check)),
                            onPressed: () {Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>AccountScreen()));},
                          ),
                        ),

                      ],
                    ),
                  ],),

                ),
              )
            ],
          ),
        ) ,
      ),
    );
  }
}
