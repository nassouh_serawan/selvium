import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:selvium/bloc/search_product_bloc.dart';
import 'package:selvium/perfume_screen.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'models/product.dart';
import 'package:selvium/bloc/all_product_bloc.dart';
import 'package:selvium/services/response.dart';
import 'widgets/network_widget.dart';
import 'package:selvium/services/api.dart';
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

var userId;

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  AllProductBloc _pBloc;
  SearchProductBloc _sBloc;
  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    setState(() {
      userId = jsonDecode(localStorage.getString('userId'));
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pBloc.dispose();
    _sBloc.dispose();
  }

  List<int> cardList = [1, 2, 3, 4, 5];
  bool show = false;
  bool checked = false;
  bool check = false;
  String searchString = '';
  String catId;
  String title;
  Color c1 = Colors.black;
  Color c2 = Colors.black;
  Color c3 = Colors.black;
  Color c4 = Colors.black;
  Color c5 = Colors.black;
  Color c6 = Colors.black;
  bool result = true;
  bool search = false;
  bool line = false;
  double high = kToolbarHeight;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          toolbarHeight: high,
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[kColor2, kColor1])),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: search
                      ? EdgeInsets.only(top: 40.0)
                      : EdgeInsets.only(top: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            IconButton(
                              icon: SvgPicture.asset(
                                'images/BackArrow-White-icon.svg',
                                height: 20,
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'الخطوط العطرية',
                              style: kTitleTextStyle3.copyWith(
                                  fontSize:
                                      MediaQuery.of(context).size.width < 350
                                          ? 13
                                          : 16),
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButton(
                                  icon: SvgPicture.asset(
                                    'images/Search-white-icon.svg',
                                    height: 20,
                                  ),
                                  color: Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      if (search == false) {
                                        search = true;
                                        high = kToolbarHeight * 2.3;
                                      } else {
                                        search = false;
                                        high = kToolbarHeight;
                                      }
                                    });
                                  }),
                            ],
                          ))
                    ],
                  ),
                ),
                search
                    ? Flexible(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Container(
                            height: 35,
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            decoration: kContainerDecoration2,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(
                                    CupertinoIcons.search,
                                    color: Colors.grey,
                                  ),
                                  onPressed: () {
                                    if (searchString != '') {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      setState(() {
                                        checked = true;
                                        title = 'نتائج البحث';
                                        _sBloc =
                                            SearchProductBloc('$searchString');
                                        show = true;
                                        line = false;
                                        c1 = c2 =
                                            c3 = c4 = c5 = c6 = Colors.black;
                                      });
                                    }
                                  },
                                ),
                                Flexible(
                                    child: TextFormField(
                                  //controller:_search ,
                                  onChanged: (value) {
                                    searchString = value;
                                  },
                                  decoration: kInputContainerDecoration,
                                )),
                                // Icon(
                                //   CupertinoIcons.photo_camera,
                                //   color: Colors.grey,
                                // ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ),
        body: Column(
          children: [
            Flexible(
                flex: MediaQuery.of(context).orientation == Orientation.portrait
                    ? MediaQuery.of(context).size.height > 800
                        ? 3
                        : 3
                    : MediaQuery.of(context).size.height > 800
                        ? 3
                        : 4,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: MediaQuery.of(context).size.width < 350
                            ? EdgeInsets.symmetric(horizontal: 50)
                            : EdgeInsets.symmetric(horizontal: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  checked = true;
                                  title = 'عطور الزهور';
                                  c1 = Colors.grey;
                                  catId = '5029';
                                  _pBloc = AllProductBloc('category', catId);
                                  show = true;
                                  line = true;
                                  c2 = c3 = c4 = c5 = c6 = Colors.black;
                                });
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    side: BorderSide(
                                        style: BorderStyle.solid, color: c1)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'عطور الزهور',
                                    style:
                                        MediaQuery.of(context).size.width < 400
                                            ? TextStyle(fontSize: 8)
                                            : TextStyle(fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  checked = true;
                                  title = 'عطور الفواكه';
                                  c2 = Colors.grey;
                                  catId = '5031';
                                  _pBloc = AllProductBloc('category', catId);
                                  show = true;
                                  line = true;
                                  c1 = c3 = c4 = c5 = c6 = Colors.black;
                                });
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    side: BorderSide(
                                        style: BorderStyle.solid, color: c2)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'عطور الفواكه',
                                    style:
                                        MediaQuery.of(context).size.width < 400
                                            ? TextStyle(fontSize: 8)
                                            : TextStyle(fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  checked = true;
                                  title = 'عطور الأخشاب';
                                  c3 = Colors.grey;
                                  catId = '5034';
                                  _pBloc = AllProductBloc('category', catId);
                                  show = true;
                                  line = true;
                                  c1 = c2 = c4 = c5 = c6 = Colors.black;
                                });
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    side: BorderSide(
                                        style: BorderStyle.solid, color: c3)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'عطور الأخشاب',
                                    style:
                                        MediaQuery.of(context).size.width < 400
                                            ? TextStyle(fontSize: 8)
                                            : TextStyle(fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: MediaQuery.of(context).size.width < 350
                            ? EdgeInsets.symmetric(horizontal: 50)
                            : EdgeInsets.symmetric(horizontal: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  checked = true;
                                  title = 'عطور الانتعاش';
                                  c4 = Colors.grey;
                                  catId = '5030';
                                  _pBloc = AllProductBloc('category', catId);
                                  show = true;
                                  line = true;
                                  c2 = c3 = c1 = c5 = c6 = Colors.black;
                                });
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    side: BorderSide(
                                        style: BorderStyle.solid, color: c4)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'عطور الانتعاش',
                                    style:
                                        MediaQuery.of(context).size.width < 400
                                            ? TextStyle(fontSize: 8)
                                            : TextStyle(fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  checked = true;
                                  title = 'عطور الحمضيات';
                                  c5 = Colors.grey;
                                  catId = '5032';
                                  _pBloc = AllProductBloc('category', catId);
                                  show = true;
                                  line = true;
                                  c2 = c3 = c4 = c1 = c6 = Colors.black;
                                });
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    side: BorderSide(
                                        style: BorderStyle.solid, color: c5)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'عطور الحمضيات',
                                    style:
                                        MediaQuery.of(context).size.width < 400
                                            ? TextStyle(fontSize: 8)
                                            : TextStyle(fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  checked = true;
                                  title = 'عطور شرقية';
                                  c6 = Colors.grey;
                                  catId = '5028';
                                  _pBloc = AllProductBloc('category', catId);
                                  show = true;
                                  line = true;
                                  c2 = c3 = c4 = c5 = c1 = Colors.black;
                                });
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    side: BorderSide(
                                        style: BorderStyle.solid,
                                        color: Colors.black)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'عطور شرقية',
                                    style:
                                        MediaQuery.of(context).size.width < 400
                                            ? TextStyle(fontSize: 8)
                                            : TextStyle(fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
            show
                ? line
                    ? Flexible(
                        flex: 19,
                        child: StreamBuilder<Response<Products>>(
                          stream: _pBloc.productListStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              switch (snapshot.data.status) {
                                case Status.LOADING:
                                  return Loading(
                                      loadingMessage: snapshot.data.message);
                                  break;
                                case Status.COMPLETED:
                                  return ProductList(
                                    productList: snapshot.data.data,
                                    categoryName: title,
                                  );
                                  break;
                                case Status.ERROR:
                                  return Error(
                                    errorMessage: snapshot.data.message,
                                    onRetryPressed: () => _sBloc
                                        .fetchSearchProducts(searchString),
                                  );
                                  break;
                              }
                            }
                            return Container();
                          },
                        ),
                      )
                    : Flexible(
                        flex: 9,
                        child: StreamBuilder<Response<Products>>(
                          stream: _sBloc.productListStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              switch (snapshot.data.status) {
                                case Status.LOADING:
                                  return Loading(
                                      loadingMessage: snapshot.data.message);
                                  break;
                                case Status.COMPLETED:
                                  return ProductList(
                                    productList: snapshot.data.data,
                                    categoryName: title + ' : ' + searchString,
                                  );
                                  break;
                                case Status.ERROR:
                                  return Error(
                                    errorMessage: snapshot.data.message,
                                    onRetryPressed: () => _sBloc
                                        .fetchSearchProducts(searchString),
                                  );
                                  break;
                              }
                            }
                            return Container();
                          },
                        ),
                      )
                : Flexible(flex: 9, child: Container()),
          ],
        ));
  }
}

class ProductList extends StatelessWidget {
  final Products productList;
  final String categoryName;
  const ProductList({Key key, this.productList, this.categoryName})
      : super(key: key);
  addToWish(int id, String price, var context) async {
    Flushbar flush;
    ApiProvider _provider = ApiProvider();
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'addWishItem',
      'prod_id': '$id',
      'user_id': userId.toString(),
      'wish_id': '1',
      'quantity': '1',
      'original_price': '$price',
      'original_currency': 'SAR'
    };
    try {
      final response = await _provider.get("CustomAPI.php", queryParameters);
      if (response == 1) {
        flush = Flushbar(
            titleText: Text(
              'تمت الاضافة للمفضلة',
              style: TextStyle(color: Colors.green),
            ),
            messageText: Text(
              'تم اضافة عنصر جديد الى المفضلة بنجاح',
              style: TextStyle(color: Colors.green),
            ),
            // showProgressIndicator: true,
            duration: Duration(seconds: 4),
            // leftBarIndicatorColor: viewAll,
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.green,
            backgroundColor: Colors.white,
            // backgroundGradient: LinearGradient(
            //         begin: Alignment.centerLeft,
            //         end: Alignment.centerRight,
            //         colors: <Color>[kColor2, kColor1]),
            boxShadows: [
              BoxShadow(
                color: Colors.green,
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.green,
              ),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else if (response == 2) {
        flush = Flushbar(
            titleText: Text(
              'تم الحذف من المفضلة',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'تم حذف العنصر من المفضلة بنجاح',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.redAccent.shade700,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            titleText: Text(
              'حدث خطأ ما',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'الرجاء المحاولة مرة أخرى',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.red,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'الخطوط العطرية',
                  style: TextStyle(fontWeight: FontWeight.w700),
                ),
                Text('-'),
                Text(categoryName)
              ],
            ),
          ),
          Flexible(
            flex: 22,
            child: GridView.builder(
                itemCount: productList.products.length,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? 3
                        : 4,
                    childAspectRatio: 0.7),
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Card(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => PerfumeScreen(
                                            productList.products[index].id)));
                              },
                              child: Container(
                                height: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? MediaQuery.of(context).size.height > 900
                                        ? MediaQuery.of(context).size.height *
                                            0.22
                                        : MediaQuery.of(context).size.height *
                                            0.12
                                    : MediaQuery.of(context).size.width > 700
                                        ? MediaQuery.of(context).size.width *
                                            0.17
                                        : MediaQuery.of(context).size.width *
                                            0.12,
                                width: 125,
                                child:
                                    productList.products[index].images[0].src ==
                                            null
                                        ? Image.asset("images/p3.png")
                                        : Image.network(productList
                                            .products[index].images[0].src),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                addToWish(productList.products[index].id,
                                    productList.products[index].price, context);
                              },
                              child: Container(
                                  width: 150,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            right: 5.0, bottom: 5),
                                        child: Icon(
                                          CupertinoIcons.heart,
                                          color: viewAll,
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          width: 125,
                          child: Text(
                            productList.products[index].name.length > 12
                                ? productList.products[index].name
                                    .substring(0, 12)
                                : productList.products[index].name,
                            style: TextStyle(fontSize: 10),
                          )),
                      Container(
                          width: 125,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                productList.products[index].price + 'ر.س',
                                textAlign: TextAlign.right,
                                style: TextStyle(fontSize: 12),
                              ),
                              productList.products[index].salePrice == ""
                                  ? Container()
                                  : Text(
                                      productList.products[index].salePrice +
                                          'ر.س',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 10,
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: Colors.red),
                                    ),
                            ],
                          ))
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }
}
