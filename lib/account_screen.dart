import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_edit_screen.dart';
import 'package:selvium/add_card_screen.dart';
import 'package:selvium/add_mobile_screen.dart';
import 'package:selvium/billing_gateways_screen.dart';
import 'package:selvium/billing_methods_screen.dart';
import 'package:selvium/bloc/customer_bloc.dart';
import 'package:selvium/change_password_screen.dart';
import 'package:selvium/country_screen.dart';
import 'package:selvium/delivery_address_screen.dart';
import 'package:selvium/mobile_auth_screen.dart';
import 'package:selvium/rates_screen.dart';
import 'package:flutter/material.dart';
import 'package:selvium/splash_screen.dart';
import 'constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'models/customer.dart';
import 'services/response.dart';
import 'widgets/network_widget.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  CustomerBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = CustomerBloc();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
            icon: SvgPicture.asset(
              'images/BackArrow-White-icon.svg',
              height: 20,
            ),
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          'حسابي',
          style: kTitleTextStyle3,
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[kColor2, kColor1])),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchCustomerData(),
        child: StreamBuilder<Response<Customer>>(
          stream: _bloc.customerDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return AccountView(customer: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchCustomerData(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class AccountView extends StatefulWidget {
  final Customer customer;

  const AccountView({Key key, this.customer}) : super(key: key);
  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  bool loading = false;
  Future<void> _handleClickMe() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('تسجيل الخروج'),
          content: Text('هل تريد بالتأكيد تسجيل الخروج؟'),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(
                'الغاء',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
              child:
                  Text('تسجيل الخروج', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                setState(() {
                  loading = true;
                });
                SharedPreferences localStorage =
                    await SharedPreferences.getInstance();
                localStorage.remove('userId');
                localStorage.remove('token');
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => SplashScreen()),
                    (route) => false);
                setState(() {
                  loading = false;
                });
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(FontAwesomeIcons.pen),
            color: Color(0xfff34949),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          AccountEditScreen(widget.customer.id)));
            },
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title: Text('سلفيوم', style: kTitleTextStyle)),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.4,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                color: Color(0xffffffff),
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20.0),
                    bottomLeft: Radius.circular(20.0)),
              ),
              child: SafeArea(
                child: CustomScrollView(
                  slivers: [
                    SliverFillRemaining(
                      hasScrollBody: false,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundColor: Colors.transparent,
                                    radius: MediaQuery.of(context).size.width >
                                            500
                                        ? MediaQuery.of(context).size.width *
                                            0.1
                                        : MediaQuery.of(context).size.width *
                                            0.15,
                                    child: ClipOval(
                                      child: Image(
                                        image: NetworkImage(
                                            widget.customer.avatarUrl),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(widget.customer.username,
                                          textAlign: TextAlign.center,
                                          style: kTitleTextStyle),
                                      Text(widget.customer.email,
                                          textAlign: TextAlign.center,
                                          style: kTitleTextStyle),
                                      Directionality(
                                          textDirection: TextDirection.ltr,
                                          child: Text(
                                              '+' +
                                                  widget.customer.billing.phone,
                                              textAlign: TextAlign.center,
                                              style: kTitleTextStyle)),
                                    ],
                                  ),
                                ],
                              )
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(widget.customer.username,
                                          textAlign: TextAlign.center,
                                          style: kTitleTextStyle),
                                      Text(widget.customer.email,
                                          textAlign: TextAlign.center,
                                          style: kTitleTextStyle),
                                      Directionality(
                                          textDirection: TextDirection.ltr,
                                          child: Text(
                                              '+' +
                                                  widget.customer.billing.phone,
                                              textAlign: TextAlign.center,
                                              style: kTitleTextStyle)),
                                    ],
                                  ),
                                  CircleAvatar(
                                    backgroundColor: Colors.transparent,
                                    radius: MediaQuery.of(context).size.width *
                                        0.07,
                                    child: ClipOval(
                                      child: Image(
                                        image: NetworkImage(
                                            widget.customer.avatarUrl),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            ChangePasswordScreen(widget.customer.id)));
              },
              child: ListTile(
                leading: Icon(Icons.lock),
                title: Text('تغيير كلمة السر', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ChangePasswordScreen(widget.customer.id)));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AddMobileScreen(widget.customer.id)));
              },
              child: ListTile(
                leading: Icon(Icons.phone_iphone),
                title: Text('اتصال بالموبايل', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                AddMobileScreen(widget.customer.id)));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CountryScreen(widget.customer.id)));
              },
              child: ListTile(
                leading: Icon(Icons.flag),
                title: Text('البلد', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CountryScreen(widget.customer.id)));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DeliveryAddressScreen(widget.customer.id)));
              },
              child: ListTile(
                leading: Icon(Icons.location_on),
                title: Text('عنوان التوصيل', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                DeliveryAddressScreen(widget.customer.id)));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: _handleClickMe,
              child: ListTile(
                leading: Icon(Icons.subdirectory_arrow_left),
                title: Text('تسجيل الخروج', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: _handleClickMe,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
