import 'dart:convert';
import 'models/attribute.dart';
import 'package:flutter/widgets.dart';
import 'package:selvium/bloc/attribute_bloc.dart';
import 'package:selvium/brands_screen.dart';
import 'package:selvium/categories_screen.dart';
import 'package:selvium/category_screen.dart';
import 'package:selvium/constant.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/perfume_screen.dart';
import 'package:selvium/search_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'models/category.dart';
import 'models/notification.dart' as not;
import 'package:flushbar/flushbar.dart';
import 'models/product.dart';
import 'services/response.dart';
import 'bloc/category_bloc.dart';
import 'bloc/tag_bloc.dart';
import 'package:selvium/brand_screen.dart';
import 'bloc/product_bloc.dart';
import 'widgets/network_widget.dart';
import 'package:selvium/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

var userId;

class MarketScreen extends StatefulWidget {
  @override
  _MarketScreenState createState() => _MarketScreenState();
}

class _MarketScreenState extends State<MarketScreen> {
  CategoryBloc _cbloc;
  AttributeBloc _tbloc;
  ProductBloc _monthDealBloc;
  ProductBloc _specialPerfumeBloc;
  ProductBloc _makeUpBloc;
  ProductBloc _onSaleBloc;
  ApiProvider _provider = ApiProvider();
  List<not.Notification> nots = [];

  getNotification() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    setState(() {
      userId = jsonDecode(localStorage.getString('userId'));
    });
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'getNotificationSystem'
    };
    try {
      final response = await _provider.get2("CustomAPI.php", queryParameters);
      setState(() {
        for (Map i in response) {
          nots.add(not.Notification.fromJson(i));
        }
      });
    } catch (e) {}
    print(userId);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNotification();
    _cbloc = CategoryBloc();
    _tbloc = AttributeBloc(2);
    _monthDealBloc = ProductBloc('category', '6633');
    _specialPerfumeBloc = ProductBloc('category', '2562');
    _makeUpBloc = ProductBloc('category', '2546');
    _onSaleBloc = ProductBloc('category', '3222');
  }

  @override
  void dispose() {
    _cbloc.dispose();
    _tbloc.dispose();
    _monthDealBloc.dispose();
    _makeUpBloc.dispose();
    _onSaleBloc.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          leading: IconButton(
              icon: SvgPicture.asset(
                'images/Notifcation-Icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                return showGeneralDialog(
                  context: context,
                  barrierDismissible: true,
                  transitionDuration: Duration(milliseconds: 500),
                  barrierLabel: MaterialLocalizations.of(context).dialogLabel,
                  barrierColor: Colors.black.withOpacity(0.5),
                  pageBuilder: (context, _, __) {
                    return SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0)),
                            ),
                            child: SafeArea(
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Card(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(20.0),
                                        bottomLeft: Radius.circular(20.0)),
                                  ),
                                  child: Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            IconButton(
                                                icon: Icon(CupertinoIcons.bell),
                                                color: Colors.black,
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                }),
                                            Text(
                                              'الاشعارات',
                                              style: kTitleTextStyle,
                                            ),
                                            IconButton(
                                              icon: Icon(CupertinoIcons.search),
                                              color: Colors.black,
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: nots.length,
                                        itemBuilder:
                                            (BuildContext context, int index) =>
                                                Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(10),
                                              decoration: kContainerDecoration,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        nots[index].title,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            color: Colors.red),
                                                      ),
                                                    ],
                                                  ),
                                                  Text(
                                                    nots[index].message,
                                                    style: TextStyle(
                                                        color: Colors.grey),
                                                  )
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                  transitionBuilder:
                      (context, animation, secondaryAnimation, child) {
                    return SlideTransition(
                      position: CurvedAnimation(
                        parent: animation,
                        curve: Curves.easeOut,
                      ).drive(Tween<Offset>(
                        begin: Offset(0, -1.0),
                        end: Offset.zero,
                      )),
                      child: child,
                    );
                  },
                );
              }),
          actions: [
            IconButton(
              icon: SvgPicture.asset(
                'images/Search-white-icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              },
            ),
          ],
          title: Text(
            'سلفيوم',
            style: kTitleTextStyle3,
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[kColor2, kColor1])),
          ),
        ),
        body: CustomScrollView(slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'كل التصنيفات',
                                  style: kTitleTextStyle,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(Icons.list_sharp),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CategoriesScreen()));
                            },
                            child: Container(
                              child: Row(
                                children: [
                                  Text(
                                    'مشاهدة الكل',
                                    style: kTitleTextStyle4.copyWith(
                                        color: viewAll,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(Icons.view_comfortable, color: viewAll),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 700
                                ? MediaQuery.of(context).size.height * 0.146
                                : MediaQuery.of(context).size.height * 0.16
                            : MediaQuery.of(context).size.width > 700
                                ? MediaQuery.of(context).size.width * 0.25
                                : MediaQuery.of(context).size.width * 0.26,
                        child: RefreshIndicator(
                          onRefresh: () => _cbloc.fetchCategories(),
                          child: StreamBuilder<Response<Categories>>(
                            stream: _cbloc.categoryListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                switch (snapshot.data.status) {
                                  case Status.LOADING:
                                    return Loading(
                                        loadingMessage: snapshot.data.message);
                                    break;
                                  case Status.COMPLETED:
                                    return CategoryList(
                                        categoryList: snapshot.data.data);
                                    break;
                                  case Status.ERROR:
                                    return Error(
                                      errorMessage: snapshot.data.message,
                                      onRetryPressed: () =>
                                          _cbloc.fetchCategories(),
                                    );
                                    break;
                                }
                              }
                              return Container();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'الماركات',
                                  style: kTitleTextStyle,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(Icons.star_border),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BrandsScreen()));
                            },
                            child: Container(
                              child: Row(
                                children: [
                                  Text(
                                    'مشاهدة الكل',
                                    style: kTitleTextStyle4.copyWith(
                                        color: viewAll,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(Icons.view_comfortable, color: viewAll),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 900
                                ? MediaQuery.of(context).size.height * 0.29
                                : MediaQuery.of(context).size.height * 0.265
                            : MediaQuery.of(context).size.width > 900
                                ? MediaQuery.of(context).size.width * 0.35
                                : MediaQuery.of(context).size.width * 0.28,
                        child: RefreshIndicator(
                          onRefresh: () => _tbloc.fetchAttributes(2),
                          child: StreamBuilder<Response<Attributes>>(
                            stream: _tbloc.attributeListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                switch (snapshot.data.status) {
                                  case Status.LOADING:
                                    return Loading(
                                        loadingMessage: snapshot.data.message);
                                    break;
                                  case Status.COMPLETED:
                                    return BrandList(
                                        brandList: snapshot.data.data);
                                    break;
                                  case Status.ERROR:
                                    return Error(
                                      errorMessage: snapshot.data.message,
                                      onRetryPressed: () =>
                                          _tbloc.fetchAttributes(2),
                                    );
                                    break;
                                }
                              }
                              return Container();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'صفقة الشهر',
                                  style: kTitleTextStyle,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(Icons.local_offer_outlined),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CategoryScreen(6633, 'صفقة الشهر')));
                            },
                            child: Container(
                              child: Row(
                                children: [
                                  Text(
                                    'مشاهدة الكل',
                                    style: kTitleTextStyle4.copyWith(
                                        color: viewAll,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(Icons.view_comfortable, color: viewAll),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 900
                                ? MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.29
                                    : MediaQuery.of(context).size.height * 0.255
                                : MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.265
                                    : MediaQuery.of(context).size.height * 0.215
                            : MediaQuery.of(context).size.width > 800
                                ? MediaQuery.of(context).size.width * 0.26
                                : MediaQuery.of(context).size.width * 0.295,
                        child: RefreshIndicator(
                          onRefresh: () =>
                              _monthDealBloc.fetchProducts('category', '6633'),
                          child: StreamBuilder<Response<Products>>(
                            stream: _monthDealBloc.productListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                switch (snapshot.data.status) {
                                  case Status.LOADING:
                                    return Loading(
                                        loadingMessage: snapshot.data.message);
                                    break;
                                  case Status.COMPLETED:
                                    return MonthList(
                                        monthList: snapshot.data.data);
                                    break;
                                  case Status.ERROR:
                                    return Error(
                                      errorMessage: snapshot.data.message,
                                      onRetryPressed: () => _monthDealBloc
                                          .fetchProducts('category', '6633'),
                                    );
                                    break;
                                }
                              }
                              return Container();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'العطور المميزة والأكثر مبيعا',
                                  style: kTitleTextStyle,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                SvgPicture.asset(
                                  'images/Famous-Perfume.svg',
                                  color: Colors.black,
                                  width: 20,
                                ),
                                // Icon(
                                //   Icons.trending_up,
                                //   textDirection: TextDirection.ltr,
                                // ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CategoryScreen(2562,
                                          'العطور المميزة والأكثر مبيعا"')));
                            },
                            child: Container(
                              child: Row(
                                children: [
                                  Text(
                                    'مشاهدة الكل',
                                    style: kTitleTextStyle4.copyWith(
                                        color: viewAll,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(Icons.view_comfortable, color: viewAll),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 900
                                ? MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.285
                                    : MediaQuery.of(context).size.height * 0.25
                                : MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.26
                                    : MediaQuery.of(context).size.height * 0.21
                            : MediaQuery.of(context).size.width > 800
                                ? MediaQuery.of(context).size.width * 0.255
                                : MediaQuery.of(context).size.width * 0.29,
                        child: RefreshIndicator(
                          onRefresh: () => _specialPerfumeBloc.fetchProducts(
                              'category', '2562'),
                          child: StreamBuilder<Response<Products>>(
                            stream: _specialPerfumeBloc.productListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                switch (snapshot.data.status) {
                                  case Status.LOADING:
                                    return Loading(
                                        loadingMessage: snapshot.data.message);
                                    break;
                                  case Status.COMPLETED:
                                    return MonthList(
                                        monthList: snapshot.data.data);
                                    break;
                                  case Status.ERROR:
                                    return Error(
                                      errorMessage: snapshot.data.message,
                                      onRetryPressed: () => _specialPerfumeBloc
                                          .fetchProducts('category', '2562'),
                                    );
                                    break;
                                }
                              }
                              return Container();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'المكياجات',
                                  style: kTitleTextStyle,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                SvgPicture.asset(
                                  'images/makeup-Icon.svg',
                                  color: Colors.black,
                                  width: 20,
                                ),
                                // Icon(
                                //   Icons.trending_up,
                                //   textDirection: TextDirection.ltr,
                                // ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CategoryScreen(2546, 'المكياجات')));
                            },
                            child: Container(
                              child: Row(
                                children: [
                                  Text(
                                    'مشاهدة الكل',
                                    style: kTitleTextStyle4.copyWith(
                                        color: viewAll,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(Icons.view_comfortable, color: viewAll),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 900
                                ? MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.285
                                    : MediaQuery.of(context).size.height * 0.25
                                : MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.26
                                    : MediaQuery.of(context).size.height * 0.21
                            : MediaQuery.of(context).size.width > 800
                                ? MediaQuery.of(context).size.width * 0.255
                                : MediaQuery.of(context).size.width * 0.29,
                        child: RefreshIndicator(
                          onRefresh: () =>
                              _makeUpBloc.fetchProducts('category', '2546'),
                          child: StreamBuilder<Response<Products>>(
                            stream: _makeUpBloc.productListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                switch (snapshot.data.status) {
                                  case Status.LOADING:
                                    return Loading(
                                        loadingMessage: snapshot.data.message);
                                    break;
                                  case Status.COMPLETED:
                                    return MonthList(
                                        monthList: snapshot.data.data);
                                    break;
                                  case Status.ERROR:
                                    return Error(
                                      errorMessage: snapshot.data.message,
                                      onRetryPressed: () => _makeUpBloc
                                          .fetchProducts('category', '2546'),
                                    );
                                    break;
                                }
                              }
                              return Container();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'التخفيضات',
                                  style: kTitleTextStyle,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                SvgPicture.asset(
                                  'images/Sales-Icon.svg',
                                  color: Colors.black,
                                  width: 20,
                                ),
                                // Icon(
                                //   Icons.trending_up,
                                //   textDirection: TextDirection.ltr,
                                // ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CategoryScreen(3222, 'التخفيضات')));
                            },
                            child: Container(
                              child: Row(
                                children: [
                                  Text(
                                    'مشاهدة الكل',
                                    style: kTitleTextStyle4.copyWith(
                                        color: viewAll,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(Icons.view_comfortable, color: viewAll),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 900
                                ? MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.285
                                    : MediaQuery.of(context).size.height * 0.25
                                : MediaQuery.of(context).size.height < 700
                                    ? MediaQuery.of(context).size.height * 0.26
                                    : MediaQuery.of(context).size.height * 0.21
                            : MediaQuery.of(context).size.width > 800
                                ? MediaQuery.of(context).size.width * 0.255
                                : MediaQuery.of(context).size.width * 0.29,
                        child: RefreshIndicator(
                          onRefresh: () =>
                              _onSaleBloc.fetchProducts('category', '3222'),
                          child: StreamBuilder<Response<Products>>(
                            stream: _onSaleBloc.productListStream,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                switch (snapshot.data.status) {
                                  case Status.LOADING:
                                    return Loading(
                                        loadingMessage: snapshot.data.message);
                                    break;
                                  case Status.COMPLETED:
                                    return MonthList(
                                        monthList: snapshot.data.data);
                                    break;
                                  case Status.ERROR:
                                    return Error(
                                      errorMessage: snapshot.data.message,
                                      onRetryPressed: () => _onSaleBloc
                                          .fetchProducts('category', '3222'),
                                    );
                                    break;
                                }
                              }
                              return Container();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ]));
  }
}

class CategoryList extends StatelessWidget {
  final Categories categoryList;
  const CategoryList({Key key, this.categoryList}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: categoryList.categories.length,
        itemBuilder: (BuildContext context, int index) => Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CategoryScreen(
                            categoryList.categories[index].id,
                            categoryList.categories[index].name)));
              },
              child: Card(
                child: Container(
                  // decoration: kContainerDecoration6,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: categoryList.categories[index].image == null
                        ? Image.asset('images/p1.png')
                        : Image.network(
                            categoryList.categories[index].image.src),
                  ),
                  width:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? MediaQuery.of(context).size.width * 0.25
                          : MediaQuery.of(context).size.height * 0.6,
                  height:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? MediaQuery.of(context).size.height * 0.1
                          : MediaQuery.of(context).size.width * 0.2,
                ),
                elevation: 5.0,
                shape: CircleBorder(
                  side: BorderSide(
                      color: viewAll, width: 1.0, style: BorderStyle.solid),
                ),
                clipBehavior: Clip.antiAlias,
              ),
            ),
            Text(
                categoryList.categories[index].name.length > 15
                    ? categoryList.categories[index].name.substring(0, 15)
                    : categoryList.categories[index].name,
                style: kTitleTextStyle4.copyWith(
                    fontSize: 12, fontWeight: FontWeight.w500)),
          ],
        ),
      ),
    );
  }
}

class BrandList extends StatelessWidget {
  final Attributes brandList;
  const BrandList({
    Key key,
    this.brandList,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: brandList.attributes.length,
        itemBuilder: (BuildContext context, int index) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BrandScreen(
                          brandList.attributes[index].id,
                          brandList.attributes[index].name)));
            },
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Image.asset('images/p1.png'),
                      ),
                    ),
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.width > 350
                            ? MediaQuery.of(context).size.width * 0.5
                            : MediaQuery.of(context).size.width * 0.5
                        : MediaQuery.of(context).size.height > 350
                            ? MediaQuery.of(context).size.height * 0.6
                            : MediaQuery.of(context).size.height * 0.6,
                    height: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.height > 800
                            ? MediaQuery.of(context).size.height * 0.165
                            : MediaQuery.of(context).size.height * 0.14
                        : MediaQuery.of(context).size.width > 900
                            ? MediaQuery.of(context).size.width * 0.23
                            : MediaQuery.of(context).size.width * 0.15,
                  ),
                  Container(
                    width: MediaQuery.of(context).orientation ==
                            Orientation.portrait
                        ? MediaQuery.of(context).size.width > 350
                            ? MediaQuery.of(context).size.width * 0.5
                            : MediaQuery.of(context).size.width * 0.5
                        : MediaQuery.of(context).size.height > 350
                            ? MediaQuery.of(context).size.height * 0.6
                            : MediaQuery.of(context).size.height * 0.6,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(brandList.attributes[index].name,
                            style: kTitleTextStyle4.copyWith(
                                fontSize: 12, fontWeight: FontWeight.w500)),
                      ],
                    ),
                  ),
                ],
              ),
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: viewAll, width: 1.0, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(2.0)),
                // borderRadius: BorderRadius.circular(5.0),
              ),
              clipBehavior: Clip.antiAlias,
            ),
          ),
        ),
      ),
    );
  }
}

class MonthList extends StatelessWidget {
  final Products monthList;
  const MonthList({Key key, this.monthList}) : super(key: key);

  addToWish(int id, String price, var context) async {
    Flushbar flush;
    ApiProvider _provider = ApiProvider();
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'addWishItem',
      'prod_id': '$id',
      'user_id': userId.toString(),
      'wish_id': '1',
      'quantity': '1',
      'original_price': '$price',
      'original_currency': 'SAR'
    };
    try {
      final response = await _provider.get("CustomAPI.php", queryParameters);
      if (response == 1) {
        flush = Flushbar(
            titleText: Text(
              'تمت الاضافة للمفضلة',
              style: TextStyle(color: Colors.green),
            ),
            messageText: Text(
              'تم اضافة عنصر جديد الى المفضلة بنجاح',
              style: TextStyle(color: Colors.green),
            ),
            // showProgressIndicator: true,
            duration: Duration(seconds: 4),
            // leftBarIndicatorColor: viewAll,
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.green,
            backgroundColor: Colors.white,
            // backgroundGradient: LinearGradient(
            //         begin: Alignment.centerLeft,
            //         end: Alignment.centerRight,
            //         colors: <Color>[kColor2, kColor1]),
            boxShadows: [
              BoxShadow(
                color: Colors.green,
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.green,
              ),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else if (response == 2) {
        flush = Flushbar(
            titleText: Text(
              'تم الحذف من المفضلة',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'تم حذف العنصر من المفضلة بنجاح',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.redAccent.shade700,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            titleText: Text(
              'حدث خطأ ما',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'الرجاء المحاولة مرة أخرى',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.red,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isfav = false;
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: monthList.products.length,
        itemBuilder: (BuildContext context, int index) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PerfumeScreen(
                                    monthList.products[index].id)));
                      },
                      child: Container(
                        child: monthList.products[index].images != null
                            ? Image.network(
                                monthList.products[index].images[0].src)
                            : Image.asset('images/p1.png'),
                        width: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.width > 350
                                ? MediaQuery.of(context).size.width * 0.3
                                : MediaQuery.of(context).size.width * 0.3
                            : MediaQuery.of(context).size.height > 350
                                ? MediaQuery.of(context).size.height * 0.47
                                : MediaQuery.of(context).size.height * 0.35,
                        height: MediaQuery.of(context).orientation ==
                                Orientation.portrait
                            ? MediaQuery.of(context).size.height > 900
                                ? MediaQuery.of(context).size.height * 0.16
                                : MediaQuery.of(context).size.height * 0.1
                            : MediaQuery.of(context).size.width > 900
                                ? MediaQuery.of(context).size.width * 0.14
                                : MediaQuery.of(context).size.width * 0.14,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 5, bottom: 5),
                      width: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.width > 350
                              ? MediaQuery.of(context).size.width * 0.3
                              : MediaQuery.of(context).size.width * 0.3
                          : MediaQuery.of(context).size.height > 350
                              ? MediaQuery.of(context).size.height * 0.47
                              : MediaQuery.of(context).size.height * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          GestureDetector(
                              onTap: () {
                                isfav = true;
                                addToWish(monthList.products[index].id,
                                    monthList.products[index].price, context);
                              },
                              child: Icon(
                                CupertinoIcons.heart,
                                color: viewAll,
                                size: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? MediaQuery.of(context).size.height > 800
                                        ? 30
                                        : 20
                                    : MediaQuery.of(context).size.width > 800
                                        ? 30
                                        : 20,
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
                elevation: 5.0,
                shape: RoundedRectangleBorder(
                  // borderRadius: BorderRadius.circular(5.0),
                  side: BorderSide(
                      color: viewAll, width: 1.0, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                ),
                clipBehavior: Clip.antiAlias,
              ),
              Container(
                  width:
                      MediaQuery.of(context).orientation == Orientation.portrait
                          ? MediaQuery.of(context).size.width > 350
                              ? MediaQuery.of(context).size.width * 0.3
                              : MediaQuery.of(context).size.width * 0.3
                          : MediaQuery.of(context).size.height > 350
                              ? MediaQuery.of(context).size.height * 0.47
                              : MediaQuery.of(context).size.height * 0.35,
                  child: Text(
                      monthList.products[index].name.length > 12
                          ? monthList.products[index].name.substring(0, 12) +
                              '...'
                          : monthList.products[index].name,
                      textAlign: TextAlign.center,
                      style: kTitleTextStyle4.copyWith(
                          fontSize: 9, fontWeight: FontWeight.w500))),
              Container(
                width:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.width > 350
                            ? MediaQuery.of(context).size.width * 0.3
                            : MediaQuery.of(context).size.width * 0.3
                        : MediaQuery.of(context).size.height > 350
                            ? MediaQuery.of(context).size.height * 0.47
                            : MediaQuery.of(context).size.height * 0.35,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(monthList.products[index].price + ' ر.س',
                        textAlign: TextAlign.right,
                        style: kTitleTextStyle4.copyWith(
                            fontSize: 9,
                            color: viewAll,
                            fontWeight: FontWeight.w500)),
                    Padding(
                      padding: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? MediaQuery.of(context).size.height > 900
                              ? EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * 0.15,
                                )
                              : EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * 0.025,
                                )
                          : MediaQuery.of(context).size.width > 900
                              ? EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * 0.2,
                                )
                              : EdgeInsets.only(
                                  right:
                                      MediaQuery.of(context).size.width * 0.03,
                                ),
                      child: Text(
                          monthList.products[index].regularPrice != ''
                              ? monthList.products[index].regularPrice + ' ر.س'
                              : '',
                          textAlign: TextAlign.left,
                          style: kTitleTextStyle4.copyWith(
                              fontSize: 8,
                              decoration: TextDecoration.lineThrough,
                              color: Colors.red)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
