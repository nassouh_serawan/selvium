import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/services/api.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_screen.dart';
import 'package:selvium/bloc/customer_bloc.dart';
import 'constant.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/services/response.dart';
class CountryScreen extends StatefulWidget {
  final int customerId;
  CountryScreen(this.customerId);
  @override
  _CountryScreenState createState() => _CountryScreenState();
}

class _CountryScreenState extends State<CountryScreen> {

  CustomerBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = CustomerBloc();
  }
  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
          leading:IconButton(icon: SvgPicture.asset('images/Back-Arrow-Black.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title:Text('البلد',style: kTitleTextStyle)
      ),
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchCustomerData(),
        child: StreamBuilder<Response<Customer>>(
          stream: _bloc.customerDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return CountryView(customer: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () =>
                        _bloc.fetchCustomerData(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}
class CountryView extends StatefulWidget {
  final Customer customer;

  const CountryView({Key key, this.customer}) : super(key: key);
  @override
  _CountryViewState createState() => _CountryViewState();
}

class _CountryViewState extends State<CountryView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  Flushbar flush;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  final TextEditingController _country = TextEditingController();
  String validatePass(value){
    if (value.length < 1)
      return 'لطفا" أدخل اسم البلد';
    else
      return null;
  }
  changeCountry()async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      var data = {"billing": {
        "country": _country.text
      },};
      try{
        final response = await _provider.put(
            'wp-json/wc/v3/customers/' + widget.customer.id.toString(),
            {ck: ckCode, cs: csCode},
            data);
        var cus = Customer.fromJson(response);
        if (cus != null) {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'تم تغيير البلد',
              message: 'تم تغيير البلد بنجاح',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.green,
              boxShadows: [
                BoxShadow(
                  color: Colors.green[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        } else {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'حدث خطأ ما',
              message: 'الرجاء المحاولة مرة أخرى',
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.red,
              boxShadows: [
                BoxShadow(
                  color: Colors.red[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        }
      }catch(e){
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: e.toString(),
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading = false;
        });
      }
    }else{setState(() {
      _autoValidate = true;
    });}
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _country.text=widget.customer.billing.country;
  }
  @override
  Widget build(BuildContext context) {
    return loading?Loading(loadingMessage: 'الرجاء الانتظار',):Container(
      height:
      MediaQuery.of(context).orientation==Orientation.portrait?
      MediaQuery.of(context).size.height>800?
      MediaQuery.of(context).size.height*0.23:
      MediaQuery.of(context).size.height*0.3:
      MediaQuery.of(context).size.width>800?
      MediaQuery.of(context).size.width*0.3:
      MediaQuery.of(context).size.width*0.4,

      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        color: Color(0xffffffff),
        borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
      ),
      child:SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child:
                Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(height: MediaQuery.of(context).size.height*0.03,),
                    Form(
                      key: _formKey,
                      child: Container(
                          decoration: kContainerDecoration,
                          child: TextFormField(
                            onSaved:(value) {
                              _country.text=value;
                            },
                            validator: validatePass,
                            controller: _country,
                            decoration: kInputContainerDecoration.copyWith(hintText:"أدخل اسم البلد الذي تعيش فيه" ,hintStyle:kHintTextStyle ,),)),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FloatingActionButton(
                            child: Container(
                                width: double.infinity,
                                height: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        kColor2,kColor1
                                      ]),
                                ),
                                child: Icon(Icons.check)),
                            onPressed: changeCountry,
                          ),
                        ),

                      ],
                    ),
                  ],),

              ),
            )
          ],
        ),
      ) ,
    );
  }
}
