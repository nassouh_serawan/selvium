import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:selvium/category_screen.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/perfume_screen.dart';
import 'package:selvium/search_screen.dart';
import 'constant.dart';
import 'package:badges/badges.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/cupertino.dart';
import 'package:selvium/models/category.dart';
import 'package:selvium/bloc/all_category_bloc.dart';
import 'package:selvium/repository/all_category_repo.dart';
import 'package:selvium/bloc/event.dart';
import 'package:selvium/bloc/category_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selvium/widgets/network_widget.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  double high = kToolbarHeight;
  bool search = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AllCategoryBloc(
        allCategoryRepository: AllCategoryRepository(),
      )..add(FetchEvent()),
      child: Scaffold(
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          leading: IconButton(
              icon: SvgPicture.asset(
                'images/BackArrow-White-icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              }),
          actions: [
            IconButton(
              icon: SvgPicture.asset(
                'images/Search-white-icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              },
            ),
          ],
          title: Text(
            'التصنيفات',
            style: kTitleTextStyle3,
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[kColor2, kColor1])),
          ),
        ),
        body: CategoryBody(),
      ),
    );
  }
}

class CategoryBody extends StatelessWidget {
  final List<Category> _categories = [];
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocConsumer<AllCategoryBloc, CategoryState>(
        listener: (context, categoryState) {
          if (categoryState is CategoryLoadingState) {
            //Scaffold.of(context)
            //  .showSnackBar(SnackBar(content: Text(categoryState.message)));
          } else if (categoryState is CategorySuccessState &&
              categoryState.categories.isEmpty) {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text('لا مزيد من التصنيفات')));
          } else if (categoryState is CategoryErrorState) {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text(categoryState.error)));
            context.read<AllCategoryBloc>().isFetching = false;
          }
          return;
        },
        builder: (context, categoryState) {
          if (categoryState is CategoryInitialState ||
              categoryState is CategoryLoadingState && _categories.isEmpty) {
            return Loading(
              loadingMessage: 'جاري تحميل التصنيفات',
            );
          } else if (categoryState is CategorySuccessState) {
            _categories.addAll(categoryState.categories);
            context.watch<AllCategoryBloc>().isFetching = false;
            Scaffold.of(context).hideCurrentSnackBar();
          } else if (categoryState is CategoryErrorState &&
              _categories.isEmpty) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    context.read<AllCategoryBloc>()
                      ..isFetching = true
                      ..add(FetchEvent());
                  },
                  icon: Icon(Icons.refresh),
                ),
                const SizedBox(height: 15),
                Text(
                  categoryState.error,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red, fontSize: 9),
                ),
              ],
            );
          }
          return Column(
            children: [
              Flexible(
                child: GridView.builder(
                  controller: _scrollController
                    ..addListener(() {
                      if (_scrollController.offset ==
                              _scrollController.position.maxScrollExtent &&
                          !context.read<AllCategoryBloc>().isFetching) {
                        context.read<AllCategoryBloc>()
                          ..isFetching = true
                          ..add(FetchEvent());
                      }
                    }),
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? 3
                          : 4,
                      childAspectRatio: 0.8),
                  itemBuilder: (context, index) =>
                      CategoryListItem(_categories[index]),
                  itemCount: _categories.length,
                ),
              ),
              context.watch<AllCategoryBloc>().isFetching
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Loading(
                        loadingMessage: ' تحميل المزيد من التصنيفات',
                      ),
                    )
                  : Container(),
            ],
          );
        },
      ),
    );
  }
}

class CategoryListItem extends StatelessWidget {
  final Category category;

  const CategoryListItem(this.category);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    CategoryScreen(category.id, category.name)));
      },
      child: Column(
        children: [
          Card(
            margin: EdgeInsets.all(4.0),
            child: Padding(
                padding: const EdgeInsets.all(30),
                child: category.image == null
                    ? Image.asset('images/p1.png')
                    : Image.network(category.image.src)),
            shape: CircleBorder(
              side: BorderSide(
                  color: viewAll, width: 1.0, style: BorderStyle.solid),
            ),
          ),
          Text(
            category.name.length > 20
                ? category.name.substring(0, 20) + '...'
                : category.name,
            style: TextStyle(fontSize: 14),
          )
        ],
      ),
    );
  }
}
