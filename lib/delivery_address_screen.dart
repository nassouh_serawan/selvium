import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:selvium/services/api.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_screen.dart';
import 'package:selvium/bloc/customer_bloc.dart';
import 'constant.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/map_screen.dart';
import 'package:nominatim_location_picker/nominatim_location_picker.dart';
class DeliveryAddressScreen extends StatefulWidget {
  final int customerId;
  DeliveryAddressScreen(this.customerId);
  @override
  _DeliveryAddressScreenState createState() => _DeliveryAddressScreenState();
}

class _DeliveryAddressScreenState extends State<DeliveryAddressScreen> {
  CustomerBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = CustomerBloc();
  }
  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
          leading:IconButton(icon: SvgPicture.asset('images/Back-Arrow-Black.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title:Text('عنوان التوصيل',style: kTitleTextStyle)
      ),
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchCustomerData(),
        child: StreamBuilder<Response<Customer>>(
          stream: _bloc.customerDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return AddressView(customer: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () =>
                        _bloc.fetchCustomerData(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}
class AddressView extends StatefulWidget {
  final Customer customer;
  const AddressView({Key key, this.customer}) : super(key: key);
  @override
  _AddressViewState createState() => _AddressViewState();
}

class _AddressViewState extends State<AddressView> {
  final TextEditingController _address = TextEditingController();
  String validatePass(value){
    if (value.length < 1)
      return 'لطفا" أدخل عنوانك المفضل لعملية التوصيل';
    else
      return null;
  }
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  Flushbar flush;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  Future getLocationWithNominatim() async {
    Map result = await showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return NominatimLocationPicker(
            searchHint: '    بحث',
            awaitingForLocation: "جاري تحديث الموقع",
            customMarkerIcon: Image.asset(
              "images/loc.PNG",
            ),
          );
        });
    if (result != null) {
      print(result);
      setState(() => _address.text = result.toString().substring(4,result.toString().length-1));
    } else {
      return;
    }
  }
  changeAddress()async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      var data = {"shipping": {
        "address_1": _address.text
      },};
      final response = await _provider.put(
          'wp-json/wc/v3/customers/' + widget.customer.id.toString(),
          {ck: ckCode, cs: csCode},
          data);
      var cus = Customer.fromJson(response);
      if (cus != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => AccountScreen()));
        flush = Flushbar(
            title: 'تم تغيير عنوان التوصيل',
            message: 'تم تغيير عنوان التوصيل بنجاح',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.green,
            boxShadows: [
              BoxShadow(
                color: Colors.green[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading = false;
        });
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => AccountScreen()));
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: 'الرجاء المحاولة مرة أخرى',
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading = false;
        });
      }
    }else{setState(() {
      _autoValidate = true;
    });}
  }
  _navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MapScreen()),
    );
    setState(() {
      _address.text=result;
    });
  }
  void initState() {
    // TODO: implement initState
    super.initState();
    _address.text=widget.customer.shipping.address1;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height:
      MediaQuery.of(context).orientation==Orientation.portrait?
      MediaQuery.of(context).size.height>800?
      MediaQuery.of(context).size.height*0.23:
      MediaQuery.of(context).size.height*0.3:
      MediaQuery.of(context).size.width>800?
      MediaQuery.of(context).size.width*0.3:
      MediaQuery.of(context).size.width*0.4,

      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        color: Color(0xffffffff),
        borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
      ),
      child:SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child:
                Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(height: MediaQuery.of(context).size.height*0.03,),
                    Form(
                      key: _formKey,
                      child: Container(
                          decoration: kContainerDecoration,
                          child: TextFormField(
                            onSaved: (val){
                              _address.text=val;
                            },
                            validator: validatePass,
                            controller: _address,
                            decoration: kInputContainerDecoration.copyWith(hintText:"أدخل عنوانك المفضل لعملية التوصيل" ,hintStyle:kHintTextStyle ,suffixIcon: IconButton(icon:Icon(CupertinoIcons.location),onPressed:() {
                              FocusScope.of(context).requestFocus(FocusNode());
                              getLocationWithNominatim();
                             // _navigateAndDisplaySelection(context);
                            },)),)),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FloatingActionButton(
                            child: Container(
                                width: double.infinity,
                                height: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        kColor2,kColor1
                                      ]),
                                ),
                                child: Icon(Icons.check)),
                            onPressed: changeAddress,
                          ),
                        ),

                      ],
                    ),
                  ],),

              ),
            )
          ],
        ),
      ) ,
    );
  }
}
