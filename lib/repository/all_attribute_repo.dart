import 'package:selvium/services/api.dart';
import 'package:selvium/models/attribute.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
import 'package:flutter/foundation.dart';
class AllAttributeRepository1 {
  ApiProvider _provider = ApiProvider();

  Future<Attributes> fetchAllAttributeData(int id) async {
    var queryParameters1 = {
      ck: ckCode,
      cs: csCode,
      'page':'1',
      'per_page':'100',
      'order':'desc'
    };
    var queryParameters2 = {
      ck: ckCode,
      cs: csCode,
      'page':'2',
      'per_page':'100',
      'order':'desc'
    };
    var queryParameters3 = {
      ck: ckCode,
      cs: csCode,
      'page':'3',
      'per_page':'100',
      'order':'desc'
    };
    var queryParameters4 = {
      ck: ckCode,
      cs: csCode,
      'page':'4',
      'per_page':'100',
      'order':'desc'
    };
    final response1= await _provider.get("wp-json/wc/v2/products/attributes/$id/terms",queryParameters1);
    final response2= await _provider.get("wp-json/wc/v2/products/attributes/$id/terms",queryParameters2);
    final response3= await _provider.get("wp-json/wc/v2/products/attributes/$id/terms",queryParameters3);
    final response4= await _provider.get("wp-json/wc/v2/products/attributes/$id/terms",queryParameters4);
    return Attributes.fromJson(response1+response2+response3+response4);
  }
}

class AllAttributeRepository {
  ApiProvider _provider = ApiProvider();
  static final AllAttributeRepository _allAttributeRepository = AllAttributeRepository._();
  AllAttributeRepository._();
  factory AllAttributeRepository() {
    return _allAttributeRepository;
  }

  Future<dynamic> getAttributes({
    @required String page,
    @required String id,
  }) async {

    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'page':page,
      'per_page':'12',
      'order':'desc'
    };
    try {
      return await _provider.getList("wp-json/wc/v2/products/attributes/$id/terms",queryParameters);
    } catch (e) {
      return e.toString();
    }
  }
}