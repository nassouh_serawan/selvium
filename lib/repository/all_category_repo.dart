import 'package:selvium/services/api.dart';
import 'package:selvium/models/category.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
import 'package:flutter/foundation.dart';
class AllCategoryRepository {
  ApiProvider _provider = ApiProvider();
  static final AllCategoryRepository _allCategoryRepository = AllCategoryRepository._();
  AllCategoryRepository._();
  factory AllCategoryRepository() {
    return _allCategoryRepository;
  }

  Future<dynamic> getCategories({
    @required String page,
  }) async {

    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'page':page,
      'per_page':'18',
    };
    try {
      return await _provider.getList("wp-json/wc/v2/products/categories",queryParameters);
    } catch (e) {
      return e.toString();
    }
  }
}