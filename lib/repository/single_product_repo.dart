import 'package:selvium/services/api.dart';
import 'package:selvium/models/product.dart';
import 'dart:async';
import 'dart:convert';
import 'package:selvium/constant.dart';
class SingleProductRepository {
  ApiProvider _provider = ApiProvider();

  Future<Product> fetchProductData(int id) async {
    final response = await _provider.get("wp-json/wc/v2/products/$id",{ck: ckCode, cs: csCode,});
    return Product.fromJson(response);
  }
}