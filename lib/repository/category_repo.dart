import 'package:selvium/services/api.dart';
import 'package:selvium/models/category.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class CategoryRepository {
  ApiProvider _provider = ApiProvider();
  var queryParameters = {
    ck: ckCode,
    cs: csCode,

  };
  Future<Categories> fetchCategoryData() async {
    final response = await _provider.get("wp-json/wc/v2/products/categories",queryParameters);
    return Categories.fromJson(response);
  }

}