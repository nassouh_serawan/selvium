import 'package:selvium/services/api.dart';
import 'package:selvium/models/favorite.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
class FavoriteRepository {
  ApiProvider _provider = ApiProvider();
  var userId;

  Future<Favorites> fetchFavoriteData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    userId=jsonDecode(localStorage.getString('userId'));
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun':'getWishList',
      'user_id':'$userId'
    };
    final response = await _provider.get("CustomAPI.php",queryParameters);
    return Favorites.fromJson(response);
  }

}