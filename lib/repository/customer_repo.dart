import 'package:selvium/services/api.dart';
import 'package:selvium/models/customer.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:selvium/constant.dart';
class CustomerRepository {
  ApiProvider _provider = ApiProvider();
  var userId;
  Future<Customer> fetchCustomerData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    userId=jsonDecode(localStorage.getString('userId'));
    final response = await _provider.get("wp-json/wc/v2/customers/$userId",{ck: ckCode, cs: csCode,});
    return Customer.fromJson(response);
  }
}