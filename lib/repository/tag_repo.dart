import 'package:selvium/services/api.dart';
import 'package:selvium/models/tag.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class TagRepository {
  ApiProvider _provider = ApiProvider();
  var queryParameters = {
    ck: ckCode,
    cs: csCode,
    'orderby':'term_group',
    'order':'desc'
  };
  Future<Tags> fetchTagData() async {
    final response = await _provider.get("wp-json/wc/v2/products/tags",queryParameters);
    return Tags.fromJson(response);
  }
}