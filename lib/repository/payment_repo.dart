import 'package:selvium/services/api.dart';
import 'package:selvium/models/payment.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class PaymentRepository {
  ApiProvider _provider = ApiProvider();

  Future<Payments> fetchPayment() async {
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
    };
    final response= await _provider.get("wp-json/wc/v3/payment_gateways",queryParameters);

    return Payments.fromJson(response);
  }


}