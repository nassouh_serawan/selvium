import 'package:selvium/services/api.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:selvium/constant.dart';

class BrandProductRepository {
  ApiProvider _provider = ApiProvider();
  static final BrandProductRepository _brandProductRepository = BrandProductRepository._();

  BrandProductRepository._();

  factory BrandProductRepository() {
    return _brandProductRepository;
  }

  Future<dynamic> getBrandProduct({
    @required String page,
    @required String parameter1,
    @required String value1,
    String parameter2,
    String value2,
  }) async {

    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      parameter1:value1,
      'page':page,
      'per_page':'12',
      parameter2:value2
    };
    try {
      return await _provider.getList("wp-json/wc/v2/products",queryParameters);
    } catch (e) {
      return e.toString();
    }
  }
}