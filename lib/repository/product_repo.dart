import 'package:selvium/services/api.dart';
import 'package:selvium/models/product.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class ProductRepository {
  ApiProvider _provider = ApiProvider();

  Future<Products> fetchProductData(String parameter,String value) async {
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      parameter:value
    };
    final response = await _provider.get("wp-json/wc/v2/products",queryParameters);
    return Products.fromJson(response);
  }

}