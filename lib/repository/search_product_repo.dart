import 'package:selvium/services/api.dart';
import 'package:selvium/models/product.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class SearchProductRepository {
  ApiProvider _provider = ApiProvider();
  Future<Products> fetchSearchProductData(String value) async {
    var queryParameters1 = {
      'search':value,
      ck: ckCode,
      cs: csCode,
      'per_page':'100'
    };
    final response = await _provider.get("wp-json/wc/v2/products",queryParameters1);
    return Products.fromJson(response);
  }

}