import 'package:selvium/services/api.dart';
import 'package:selvium/models/product.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class AllProductRepository {
  ApiProvider _provider = ApiProvider();


  Future<Products> fetchAllProductData(String parameter,String value) async {
    var queryParameters1 = {
      ck: ckCode,
      cs: csCode,
      'page':'1',
      'per_page':'100',
      parameter:value
    };
    var queryParameters2 = {
      ck: ckCode,
      cs: csCode,
      'page':'2',
      'per_page':'100',
      parameter:value
    };
    //var queryParameters3 = {
     // ck: ckCode,
     // cs: csCode,
     // 'page':'3',
     // 'per_page':'100',
     // parameter:value
    //};
    //var queryParameters4 = {
    //  ck: ckCode,
    //  cs: csCode,
    //  'page':'4',
    //  'per_page':'100',
    //  parameter:value
    //};
    final response1 = await _provider.get("wp-json/wc/v2/products",queryParameters1);
    final response2 = await _provider.get("wp-json/wc/v2/products",queryParameters2);
    //final response3 = await _provider.get("wp-json/wc/v2/products",queryParameters3);
    //final response4 = await _provider.get("wp-json/wc/v2/products",queryParameters4);
    return Products.fromJson(response1+response2);
  }

}