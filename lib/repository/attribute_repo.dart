import 'package:selvium/services/api.dart';
import 'package:selvium/models/attribute.dart';
import 'dart:async';
import 'package:selvium/constant.dart';
class AttributeRepository {
  ApiProvider _provider = ApiProvider();

  Future<Attributes> fetchAttributeData(int id) async {
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'page':'1',
      'per_page':'10',
      'order':'desc'
    };
    final response= await _provider.get("wp-json/wc/v2/products/attributes/$id/terms",queryParameters);

    return Attributes.fromJson(response);
  }


}