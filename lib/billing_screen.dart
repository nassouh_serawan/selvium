import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:selvium/bloc/payment_bloc.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/make_payment.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'map_screen.dart';
import 'package:selvium/models/payment.dart';
import 'package:selvium/models/cupon.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/models/shipping.dart';
import 'package:selvium/models/item.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/services/api.dart';
import 'package:flushbar/flushbar.dart';
import 'dart:convert';
import 'package:nominatim_location_picker/nominatim_location_picker.dart';
String payment="cod";
String paymentName="الدفع عند الاستلام";
class BillingScreen extends StatefulWidget {
  final Customer customer;
  final List<Item> items;
  final List<int> prices;
  BillingScreen(this.customer,this.items,this.prices);
  @override
  _BillingScreenState createState() => _BillingScreenState();
}

class _BillingScreenState extends State<BillingScreen> {
  bool loading=true;
  double opcty=0;
  double high=0;
  DateTime selectedTime=DateTime.now();
  DateTime selectedDate=DateTime.now();
  bool _autoValidate = false;
  Flushbar flush;
  Shippings shippings;
  Coupons coupons;
  Shipping selectedShip;
  double sold=0;
  double total=0;
  double finalPrice;
  int numberOfPerfume=0;
  String problemShipping='';
  bool loadingCheck=false;
  bool loadingShipping=false;
  ApiProvider _provider = ApiProvider();
  _navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MapScreen()),
    );
    setState(() {
      _address.text=result;
    });
  }
  Future getLocationWithNominatim() async {
    Map result = await showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return NominatimLocationPicker(
            searchHint: '    بحث',
            awaitingForLocation: "جاري تحديث الموقع",
            customMarkerIcon: Image.asset(
              "images/loc.PNG",
            ),
          );
        });
    if (result != null) {
      print(result);
      setState(() => _address.text = result.toString().substring(4,result.toString().length-1));
    } else {
      return;
    }
  }
  getShipping()async{
    setState(() {
      problemShipping="";
      loadingShipping = true;
    });
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
    };
    try{
      final response = await _provider.get(
          "wp-json/wc/v3/shipping_methods", queryParameters);
      setState(() {
        shippings = Shippings.fromJson(response);
        selectedShip = shippings.shippings[0];
      });
      setState(() {
        loadingShipping = false;
      });
    }catch(e){
      setState(() {
        problemShipping=e.toString();
        loadingShipping=false;
      });
    }
  }
  createOrder()async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      var data = {
        "payment_method": payment,
        "payment_method_title": paymentName,
        "set_paid": false,
        "billing": {
          "first_name": widget.customer.billing.firstName,
          "last_name": widget.customer.billing.lastName,
          "address_1": widget.customer.billing.address1,
          "address_2": widget.customer.billing.address2,
          "city":  widget.customer.billing.city,
          "state": widget.customer.billing.state,
          "postcode": widget.customer.billing.postcode,
          "country": widget.customer.billing.country,
          "email": widget.customer.billing.email,
          "phone": widget.customer.billing.phone
        },
        "shipping": {
          "first_name": widget.customer.shipping.firstName,
          "last_name": widget.customer.shipping.lastName,
          "address_1":  _address.text,
          "address_2":  widget.customer.shipping.address2,
          "city": widget.customer.shipping.city,
          "state": widget.customer.shipping.state,
          "postcode": widget.customer.shipping.postcode,
          "country": widget.customer.shipping.country,
        },
        "line_items":jsonEncode(widget.items.map((e) => e.toJson()).toList()),

        "shipping_lines": [
          {
            "method_id": selectedShip.id,
            "method_title": selectedShip.title,
            "total": "10.00"
          }
        ]
      };
      try{
        final response = await _provider.post(
            '/wp-json/wc/v3/orders', {ck: ckCode, cs: csCode}, data);
        if (response['id'] != null) {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>MakePayment(total.toString(),response['id'].toString())));
          flush = Flushbar(
              title: 'تم تسجيل طلبك',
              message: 'يرجى اتمام عملية الدفع',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.green,
              boxShadows: [
                BoxShadow(
                  color: Colors.green[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        } else {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomeScreen()));
          flush = Flushbar(
              title: 'حدث خطأ ما',
              message: response["message"],
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.red,
              boxShadows: [
                BoxShadow(
                  color: Colors.red[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        }
      }catch(e){
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: e.toString(),
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    }else{setState(() {
      _autoValidate = true;
    });}
  }
  getPrice(){
    for(int i=0;i<widget.prices.length;i++)
      {
        setState(() {
          total=total+widget.prices[i];
        });
      }
  }
  getNum(){
    for(int i=0;i<widget.items.length;i++)
    {
      setState(() {
        numberOfPerfume=numberOfPerfume+widget.items[i].quantity;
      });
    }
  }
  Future<void> _handleClickMe() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('رسالة الطلب'),
          content: Text('لم تقم بكتابة رسالة, هل تريد اضافة شيء آخر؟'),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text('لا',style: TextStyle(color: Colors.black),),
              onPressed: () {
                createOrder();
              },
            ),
            CupertinoDialogAction(
              child: Text('نعم',style: TextStyle(color: Colors.black)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  getCoupon()async{
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
    };
    try{
      final response =
          await _provider.get("wp-json/wc/v3/coupons", queryParameters);
      setState(() {
        coupons = Coupons.fromJson(response);
      });
    }catch(e){
      setState(() {
        //problem=e.toString();
        loading=false;
      });
    }
  }
  checkCoupon(){
    setState(() {
      loadingCheck=true;
    });
    for(int i=0;i<coupons.coupons.length;i++)
      {
        if (couponCode==coupons.coupons[i].code)
          {
              setState(() {
                sold=double.parse(coupons.coupons[i].amount);
              });
          }

      }
    setState(() {
      loadingCheck=false;
    });
  }
  String selectedNum;
  bool detail=false;
  bool basket=true;
  bool info=false;
  bool date=false;
  bool instruction=false;
  bool billing=false;
  bool contact=false;
  PaymentBloc _PBloc;
  String message='';
  String couponCode='';
  final TextEditingController _address = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _num = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getCoupon();
    getPrice();
    getNum();
    getShipping();
    _PBloc = PaymentBloc();
    _num.text=widget.customer.billing.phone.length<3?'':widget.customer.billing.phone.substring(0,3);
    _phone.text=widget.customer.billing.phone.length<4?'':widget.customer.billing.phone.substring(3);
    _name.text=widget.customer.firstName;
    _address.text=widget.customer.shipping.address1;
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _PBloc.dispose();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          leading:IconButton(icon: SvgPicture.asset('images/BackArrow-White-icon.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
          title: Text('الدفع',style: kTitleTextStyle3,),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[
                      kColor2,kColor1
                    ])
            ),
          ),
    ),
      body:CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Card(
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width*0.95,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  onTap: (){
                                    if(basket){setState(() {
                                      basket=false;
                                    });}
                                    else{setState(() {
                                      basket=true;
                                    });}
                                  },
                                  child: Text('معلومات السلة',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),)),
                            ],
                          ),
                          basket?Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('المجموع الكلي '+numberOfPerfume.toString()+' عناصر'),
                                  Text(total.toString()+'ر.س')
                                ],
                              ),

                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('الفاتورة الكلية',style: TextStyle(fontWeight: FontWeight.w700),),
                                  Text((total-sold).toString(),style: TextStyle(fontWeight: FontWeight.w700),)
                                ],
                              ),
                            ],
                          ):Container(),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width*0.95,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  onTap: (){
                                    if(detail){setState(() {
                                      detail=false;
                                    });}
                                    else{setState(() {
                                      detail=true;
                                    });}
                                  },
                                  child: Text('تفاصيل المستلم',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),)),
                            ],
                          ),
                          detail?Column(
                            children: [
                              Container(
                                  decoration: kContainerDecoration,
                                  child: TextFormField(
                                    onSaved:(value) {
                                      _name.text=value;
                                    },
                                    controller: _name,
                                    decoration: kInputContainerDecoration.copyWith(hintText:"الاسم" ,hintStyle:kHintTextStyle ),)),
                              SizedBox(height: 10,),
                              Row(
                                children: [
                                  Flexible(
                                    flex: 3,
                                    child: Directionality(
                                      textDirection: TextDirection.ltr,
                                      child: Container(
                                          margin: EdgeInsets.only(left: 10),
                                          decoration: kContainerDecoration,
                                          child: TextFormField(
                                            onSaved:(value) {
                                              _phone.text=value;
                                            },
                                            controller: _phone,
                                            decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle ),)),
                                    ),
                                  ),
                                  Flexible(
                                      flex: 1,
                                      child: Directionality(
                                        textDirection: TextDirection.ltr,
                                        child: Container(
                                            margin: EdgeInsets.only(left: 0),
                                            decoration: kContainerDecoration,
                                            child: TextFormField(
                                              onSaved:(value) {
                                                _num.text=value;
                                              },
                                              keyboardType: TextInputType.phone,
                                              //validator: validatePass2,
                                              controller: _num,
                                              decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle,prefixText: '+' ),)),
                                      )
                                  ),
                                ],
                              ),
                            ],
                          ):Container(),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width*0.95,
                      child:Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  onTap: (){
                                    if(info){setState(() {
                                      info=false;
                                    });}
                                    else{setState(() {
                                      info=true;
                                    });}
                                  },
                                  child: Text('معلومات الطلب',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),)),
                            ],
                          ),
                          info?Column(
                            children: [
                              loadingShipping?Text('الرجاء الانتظار'):problemShipping==""?Container(
                                  decoration: kContainerDecoration5,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 15),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        hint: Text('الرجاء اختيار طريقة الشحن'),
                                        iconSize: 0.0,
                                        value: selectedShip,
                                        onChanged: (value) {
                                          setState(() {
                                            selectedShip = value;
                                          });
                                        },
                                        items: shippings.shippings.map((map) => DropdownMenuItem(
                                          child: Text(map.title),
                                          value: map,
                                        ),
                                        ).toList(),
                                      ),
                                    ),
                                  ),
                              ):Column(children: [Text(problemShipping,style: TextStyle(fontSize: 9,color: Colors.red),),IconButton(icon: Icon(Icons.refresh), onPressed: getShipping)],),
                              SizedBox(height: 10,),
                              Container(
                                  decoration: kContainerDecoration,
                                  child: TextFormField(
                                    onSaved:(value) {
                                      _address.text=value;
                                    },
                                    controller: _address,
                                    decoration: kInputContainerDecoration.copyWith(hintText:"أدخل عنوانك المفضل لعملية التوصيل" ,hintStyle:kHintTextStyle ,suffixIcon: IconButton(icon:Icon(CupertinoIcons.location),onPressed:() {
                                      FocusScope.of(context).requestFocus(FocusNode());
                                      //_navigateAndDisplaySelection(context);
                                      getLocationWithNominatim();
                                    },)),)),
                            ],
                          ):Container(),


                        ],
                      ),
                    ),
                  ),
                  Card(
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width*0.95,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  onTap: (){
                                    if(date){setState(() {
                                      date=false;
                                    });}
                                    else{setState(() {
                                      date=true;
                                    });}
                                  },
                                  child: Text('تاريخ الطلب',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),)),
                            ],
                          ),
                          date?Column(
                            children: [
                              Container(
                                child: Text('سيتم توصيل طلبك خلال سبعة ايام ابتداءا من تاريخ تقديم الطلب اليوم'),),
                              Container(
                                  decoration: kContainerDecoration,
                                  child:Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(right:20.0),
                                        child: Text(selectedDate.toString().substring(0,10)),
                                      ),
                                      IconButton(icon:Icon(Icons.calendar_today),onPressed: (){
                                    DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: DateTime(2018, 3, 5),
                                    maxTime: DateTime(2030, 6, 7), onChanged: (date) {
                                    print('change $date');
                                    }, onConfirm: (date) {
                                          setState(() {
                                            selectedDate=date;
                                          });
                                    }, currentTime: DateTime.now(), locale: LocaleType.ar);
                                    },
                                    ),

                                    ],
                                   )
                              ),
                              SizedBox(height: 10,),
                              Container(
                                  decoration: kContainerDecoration,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(right:20.0),
                                        child: Text(selectedTime.toString().substring(10,19)),
                                      ),
                                      IconButton(icon: Icon(CupertinoIcons.time),
                                    onPressed: (){
                                      DatePicker.showTime12hPicker(context,
                                          showTitleActions: true,
                                          onChanged: (date) {
                                            print('change $date');
                                          }, onConfirm: (date) {
                                            setState(() {
                                              selectedTime=date;
                                            });
                                          }, currentTime: DateTime.now(), locale: LocaleType.ar);
                                    },
                                  )
                                    ],
                                  ),
                              ),

                            ],
                          ):Container(),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width*0.95,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  onTap: (){
                                    if(instruction){setState(() {
                                      instruction=false;
                                    });}
                                    else{setState(() {
                                      instruction=true;
                                    });}
                                  },
                                  child: Text('تعليمات الطلب',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700),)),
                            ],
                          ),
                          instruction?Column(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    contact=!contact;
                                  });
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      width: 30,
                                      height: 30,
                                      child: contact?Image.asset('images/checked.png'):Image.asset('images/check.png'),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                      child: Text('الاتصال قبل ايصال الطلب',),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 10,),
                              Container(
                                  decoration: kContainerDecoration,
                                  child: TextFormField(
                                    onChanged: (val){
                                      setState(() {
                                        message=val;
                                      });

                                    },
                                    maxLines: 5,decoration: kInputContainerDecoration.copyWith(hintText:"الرسالة" ,hintStyle:kHintTextStyle),)),
                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                 Text((message.split(' ').length-1).toString()+' من اصل 100 كلمة',style: TextStyle(color: Colors.grey),),
                                ],
                              )
                            ],
                          ):Container(),
                        ],
                      ),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.symmetric(vertical:10),
                    width: MediaQuery.of(context).size.width*0.95,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex:1,
                          child: Padding(
                            padding: const EdgeInsets.only(left:10),
                            child: GestureDetector(
                              onTap: (){
                                FocusScope.of(context).requestFocus(FocusNode());
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>HomeScreen()));},
                              child: Container(
                                padding: MediaQuery.of(context).size.height>800?EdgeInsets.all(10):EdgeInsets.all(5),
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height>800?45:35,
                                decoration: kContainerButtonDecoration2,
                                child: Text('إلغاء',textAlign: TextAlign.center,),
                              ),
                            ),
                          ),),
                        Flexible(
                          flex: 2,
                          child:GestureDetector(
                            onTap: message.length==0?_handleClickMe:createOrder,
                            child: Container(
                              padding: MediaQuery.of(context).size.height>800?EdgeInsets.all(10):EdgeInsets.all(5),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height>800?45:35,
                              decoration: kContainerButtonDecoration,
                              child: Text('الدفع الآن',style:TextStyle(color: Colors.white) ,textAlign: TextAlign.center,),
                            ),
                          ) ,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

