import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/billing_screen.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'constant.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter/services.dart';
import 'package:selvium/services/api.dart';
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:selvium/models/item.dart';
import 'package:selvium/models/product.dart';
import 'package:selvium/models/customer.dart';

class BasketScreen extends StatefulWidget {
  @override
  _BasketScreenState createState() => _BasketScreenState();
}

class _BasketScreenState extends State<BasketScreen> {
  List<Item> items = [];
  List<Product> products = [];
  List<int> prices = [];
  bool loading = false;
  double total = 0;
  double finalPrice;
  int numberOfPerfume = 0;
  Customer customer;
  Future<void> _handleClickMe(String key) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('حذف العنصر'),
          content: Text('هل تريد بالتأكيد حذف هذا العنصر من السلة الخاصة بك؟'),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(
                'لا',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
              child: Text('نعم', style: TextStyle(color: Colors.black)),
              onPressed: () {
                //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>HomeScreen()));
                deleteFromCart(key);
                getBasket();
              },
            ),
          ],
        );
      },
    );
  }

  List productIds;
  bool tap = false;
  String problem = "";
  Flushbar flush;
  bool loadingClear = false;
  deleteFromCart(String key) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tkn = jsonDecode(localStorage.getString('token'));
    ApiProvider _provider = ApiProvider();
    var data = {"cart_item_key": key, "quantity": '0'};
    print(data);
    try {
      final response = await _provider.post3("wp-json/cocart/v1/item", data);
      if (response["code"] == null) {
        flush = Flushbar(
            title: 'تم الحذف',
            message: 'تم الحذف من السلة بنجاح',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.green,
            boxShadows: [
              BoxShadow(
                color: Colors.green[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: response["code"],
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  getPrice() {
    if (items.length != 0) {
      for (int i = 0; i < products.length; i++) {
        setState(() {
          total = total + (double.parse(products[i].price) * items[i].quantity);
        });
      }
    }
  }

  getNum() {
    for (int i = 0; i < items.length; i++) {
      setState(() {
        numberOfPerfume = numberOfPerfume + items[i].quantity;
      });
    }
  }

  getBasket() async {
    setState(() {
      loading = true;
      problem = "";
    });
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tkn = jsonDecode(localStorage.getString('token'));
    var uid = localStorage.getString('userId');
    ApiProvider _provider = ApiProvider();
    try {
      final user = await _provider.get("wp-json/wc/v2/customers/$uid", {
        ck: ckCode,
        cs: csCode,
      });
      setState(() {
        customer = Customer.fromJson(user);
      });
    } catch (e) {
      setState(() {
        loading = false;
        problem = e.toString();
      });
    }
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'getUserCart',
      'token': tkn.toString()
    };
    {
      final response = await _provider.get2("CustomAPI.php", queryParameters);
      setState(() {
        products = [];
        items = [];
        for (Map i in response) {
          items.add(Item.fromJson(i));
        }
      });
      for (int i = 0; i < items.length; i++) {
        var id = items[i].productId;
        final data = await _provider.get("wp-json/wc/v2/products/$id", {
          ck: ckCode,
          cs: csCode,
        });
        setState(() {
          products.add(Product.fromJson(data));
        });
      }
      if (items.length != 0) {
        getNum();
        getPrice();
      }
      setState(() {
        loading = false;
      });
    }
  }

  clearBasket() async {
    setState(() {
      loadingClear = true;
    });
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tkn = jsonDecode(localStorage.getString('token'));
    ApiProvider _provider = ApiProvider();
    var data = {};
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
    };
    try {
      final response = await _provider.post3("wp-json/cocart/v1/clear", data);
      getBasket();
      if (response == 'Cart is cleared.') {
        flush = Flushbar(
            title: 'تمت افراغ السلة',
            message: 'تم افراغ السلة بنجاح',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.green,
            boxShadows: [
              BoxShadow(
                color: Colors.green[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: 'يرجى المحاولة مرة أخرى',
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
    setState(() {
      loadingClear = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBasket();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void slideSheet() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            child: Container(
              height: 180,
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: viewAll, width: 2.0, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                ),
              ),
              // decoration: BoxDecoration(
              //   boxShadow: [
              //     BoxShadow(
              //       color: Colors.grey.withOpacity(0.5),
              //       spreadRadius: 5,
              //       blurRadius: 7,
              //       offset: Offset(0, 3), // changes position of shadow
              //     ),
              //   ],
              //   borderRadius: BorderRadius.only(
              //       topLeft: Radius.circular(15),
              //       topRight: Radius.circular(15)),
              //   color: Colors.white,
              // ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Container(
                          height: 5,
                          width: 50,
                          child: Image.asset('images/tap.PNG'),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('المجموع الكلي  ' +
                                numberOfPerfume.toString() +
                                ' عناصر'),
                            Text(
                              total.toString() + " ر.س",
                              style: TextStyle(fontWeight: FontWeight.w700),
                            )
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Container(
                                padding:
                                    MediaQuery.of(context).size.height > 800
                                        ? EdgeInsets.all(10)
                                        : EdgeInsets.all(5),
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height > 800
                                    ? 45
                                    : 35,
                                decoration: kContainerButtonDecoration2,
                                child: Text(
                                  'الغاء',
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                prices = [];
                              });
                              for (int i = 0; i < products.length; i++) {
                                prices.add(int.parse(products[i].price) *
                                    items[i].quantity);
                              }
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BillingScreen(
                                          customer, items, prices)));
                            },
                            child: Container(
                              padding: MediaQuery.of(context).size.height > 800
                                  ? EdgeInsets.all(10)
                                  : EdgeInsets.all(5),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height > 800
                                  ? 45
                                  : 35,
                              decoration: kContainerButtonDecoration,
                              child: Text(
                                'الدفع الآن',
                                style: TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        leading: loadingClear
            ? Padding(
                padding: const EdgeInsets.all(10.0),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              )
            : IconButton(
                icon: Icon(
                  CupertinoIcons.delete_solid,
                  size: 30,
                ),
                onPressed: clearBasket,
                color: Colors.white,
              ),
        title: Text(
          'السلة',
          style: kTitleTextStyle3,
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[kColor2, kColor1])),
        ),
      ),
      body: loading
          ? Loading(
              loadingMessage: 'تحميل بيانات السلة',
            )
          : problem != ""
              ? Error(
                  errorMessage: problem,
                  onRetryPressed: getBasket,
                )
              : products.length != 0
                  ? Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20.0),
                          child: ListView.builder(
                            itemCount: products.length,
                            itemBuilder: (context, index) {
                              return Slidable(
                                actionPane: SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                actions: <Widget>[
                                  SlideAction(
                                    child: CircleAvatar(
                                      backgroundColor: Colors.red,
                                      radius: 30,
                                      child: CircleAvatar(
                                        radius: 28,
                                        backgroundColor: Colors.white,
                                        child: Icon(
                                          CupertinoIcons.delete,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ),
                                    color: Colors.white,
                                    onTap: () {
                                      _handleClickMe(items[index].key);
                                    },
                                  ),
                                ],
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: viewAll,
                                        width: 1.0,
                                        style: BorderStyle.solid),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                    // borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: viewAll,
                                                width: 1.0,
                                                style: BorderStyle.solid),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8.0)),
                                            // borderRadius: BorderRadius.circular(5.0),
                                          ),
                                          elevation: 10,
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.25,
                                            child: Column(
                                              children: [
                                                Container(
                                                  height: MediaQuery.of(context)
                                                              .orientation ==
                                                          Orientation.portrait
                                                      ? MediaQuery.of(context)
                                                              .size
                                                              .height *
                                                          0.06
                                                      : MediaQuery.of(context)
                                                              .size
                                                              .width *
                                                          0.1,
                                                  width: MediaQuery.of(context)
                                                              .orientation ==
                                                          Orientation.portrait
                                                      ? MediaQuery.of(context)
                                                              .size
                                                              .width *
                                                          0.25
                                                      : MediaQuery.of(context)
                                                              .size
                                                              .width *
                                                          0.1,
                                                  child: Image.network(
                                                      products[index]
                                                          .images[0]
                                                          .src),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 10.0,
                                                              bottom: 3),
                                                      child: Icon(
                                                        CupertinoIcons
                                                            .heart_solid,
                                                        color: viewAll,
                                                        size: MediaQuery.of(
                                                                        context)
                                                                    .orientation ==
                                                                Orientation
                                                                    .portrait
                                                            ? MediaQuery.of(context)
                                                                        .size
                                                                        .height >
                                                                    900
                                                                ? 30
                                                                : 20
                                                            : MediaQuery.of(context)
                                                                        .size
                                                                        .width >
                                                                    900
                                                                ? 30
                                                                : 20,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.6,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                products[index].name,
                                                style: TextStyle(fontSize: 15),
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    products[index].price +
                                                        " ر.س",
                                                    style: TextStyle(
                                                        color: viewAll,
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Row(
                                                    children: [
                                                      GestureDetector(
                                                        onTap: () {
                                                          if (items[index]
                                                                  .quantity <
                                                              10000) {
                                                            setState(() {
                                                              items[index]
                                                                  .quantity = items[
                                                                          index]
                                                                      .quantity +
                                                                  1;
                                                              numberOfPerfume =
                                                                  0;
                                                              total = 0;
                                                            });
                                                            getNum();
                                                            getPrice();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 30,
                                                          width: 30,
                                                          child: Image.asset(
                                                              "images/plus.PNG"),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Text(items[index]
                                                            .quantity
                                                            .toString()),
                                                      ),
                                                      GestureDetector(
                                                        onTap: () {
                                                          if (items[index]
                                                                  .quantity >
                                                              1) {
                                                            setState(() {
                                                              items[index]
                                                                  .quantity = items[
                                                                          index]
                                                                      .quantity -
                                                                  1;
                                                              numberOfPerfume =
                                                                  0;
                                                              total = 0;
                                                            });
                                                            getNum();
                                                            getPrice();
                                                          }
                                                        },
                                                        child: Container(
                                                          height: 30,
                                                          width: 30,
                                                          child: Image.asset(
                                                              "images/min.PNG"),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          child: GestureDetector(
                            onTap: slideSheet,
                            child: Container(
                              child: Column(
                                children: [
                                  Container(
                                    height: 5,
                                    width: 50,
                                    child: Image.asset('images/tap.PNG'),
                                  ),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                    height: 60,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(15),
                                          topRight: Radius.circular(15)),
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('المجموع الكلي  ' +
                                              numberOfPerfume.toString() +
                                              ' عناصر'),
                                          Text(
                                            total.toString() + " ر.س",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w700),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  : Center(
                      child: Text('لا يوجد عناصر بالسلة الخاصة بك'),
                    ),
    );
  }
}
