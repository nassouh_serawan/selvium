import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:selvium/constant.dart';

class CurvePainter extends CustomPainter {
  void paint(Canvas canvas, Size size) {
    Path path = Path();
    Paint paint = Paint();

    path.lineTo(0, size.height * 0.75);
    path.quadraticBezierTo(size.width * 0.20, size.height * 1.70,
        size.width * 0.2, size.height * 1.2);
    path.quadraticBezierTo(
        size.width * 0.20, size.height, size.width * 0.25, size.height * 0.60);
    path.quadraticBezierTo(size.width * 0.40, size.height * 0,
        size.width * 0.50, size.height * 0.95);
    path.quadraticBezierTo(size.width * 0.60, size.height * 1.4,
        size.width * 0.65, size.height * 1.50);
    path.quadraticBezierTo(
        size.width * 0.70, size.height * 1.90, size.width, 0);
    path.close();

    paint.shader = RadialGradient(
            colors: [kColor1.withOpacity(0.1), kColor2.withOpacity(0.1)])
        .createShader(Rect.fromCircle(
      center: new Offset(150.0, 130.0),
      radius: 200.0,
    ));
    canvas.drawPath(path, paint);

    path = Path();
    path.lineTo(0, size.height * 0.70);
    path.quadraticBezierTo(size.width * 0.10, size.height * 0.80,
        size.width * 0.15, size.height * 0.60);
    path.quadraticBezierTo(size.width * 0.20, size.height * 0.45,
        size.width * 0.27, size.height * 0.60);
    path.quadraticBezierTo(
        size.width * 0.45, size.height, size.width * 0.50, size.height * 0.90);
    path.quadraticBezierTo(size.width * 0.55, size.height * 0.45,
        size.width * 0.75, size.height * 0.75);
    path.quadraticBezierTo(
        size.width * 0.85, size.height * 0.93, size.width, size.height * 0.9);
    path.lineTo(size.width, 0);
    path.close();

    paint.shader = RadialGradient(
            colors: [kColor1.withOpacity(0.5), kColor2.withOpacity(0.5)])
        .createShader(Rect.fromCircle(
      center: new Offset(150.0, 130.0),
      radius: 200.0,
    ));
    canvas.drawPath(path, paint);

    path = Path();
    path.lineTo(0, size.height * 0.95);
    path.quadraticBezierTo(size.width * 0.30, size.height * 0.55,
        size.width * 0.41, size.height * 0.70);
    path.quadraticBezierTo(size.width * 0.35, size.height * 0.90,
        size.width * 0.40, size.height * 0.75);
    path.quadraticBezierTo(size.width * 0.55, size.height * 0.50,
        size.width * 0.65, size.height * 0.70);
    path.quadraticBezierTo(
        size.width * 0.75, size.height * 0.85, size.width, size.height * 0.60);
    path.lineTo(size.width, 0);
    path.close();

    paint.shader =
        RadialGradient(colors: [kColor1, kColor2]).createShader(Rect.fromCircle(
      center: new Offset(150.0, 130.0),
      radius: 200.0,
    ));

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
  // @override
  // void paint(Canvas canvas, Size size) {
  //   var paint = Paint();
  //   paint.color = Color.fromARGB(255, 54, 15, 167);
  //   paint.style = PaintingStyle.fill; // Change this to fill

  //   var path = Path();

  //   path.moveTo(0, size.height * 0.999);
  //   path.quadraticBezierTo(
  //       size.width / 2.2, size.height / 0.7, size.width, size.height * 0.999);
  //   path.lineTo(size.width, 0);
  //   path.lineTo(0, 0);

  //   canvas.drawPath(path, paint);
  // }

  // @override
  // bool shouldRepaint(CustomPainter oldDelegate) {
  //   return true;
  // }
}
