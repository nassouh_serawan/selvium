import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/single_product_repo.dart';
import 'package:selvium/models/product.dart';

class SingleProductBloc {
  SingleProductRepository _productRepository;
  StreamController _productDataController;

  StreamSink<Response<Product>> get productDataSink =>
      _productDataController.sink;

  Stream<Response<Product>> get productDataStream =>
      _productDataController.stream;

  SingleProductBloc(int id) {
    _productDataController = StreamController<Response<Product>>();
    _productRepository = SingleProductRepository();
    fetchProductData(id);
  }

  fetchProductData(int id) async {
    productDataSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Product pro = await _productRepository.fetchProductData(id);
      productDataSink.add(Response.completed(pro));
    } catch (e) {
      productDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _productDataController?.close();
  }
}