import 'dart:async';
import 'package:selvium/repository/all_attribute_repo.dart';
import 'package:selvium/models/attribute.dart';
import 'event.dart';
import 'attribute_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class AllAttributeBloc extends Bloc<Event, AttributeState> {
  final AllAttributeRepository allAttributeRepository;
  int page = 1;
  String id;
  bool isFetching = false;
  AllAttributeBloc({
    @required this.allAttributeRepository,
    @required this.id
  }) : super(AttributeInitialState());

  @override
  Stream<AttributeState> mapEventToState(Event event) async* {
    if (event is FetchEvent) {
      yield AttributeLoadingState(message: 'تحميل التصنيفات');
      final response = await allAttributeRepository.getAttributes(page: page.toString(),id: id);
      print(response);
      if (response is http.Response) {
        if (response.statusCode == 200) {
          final attributes = jsonDecode(response.body.substring(11)) as List;
          yield AttributeSuccessState(
            attributes: attributes.map((att) => Attribute.fromJson(att)).toList(),
          );
          page++;
        } else {
          yield AttributeErrorState(error: response.body);
        }
      } else if (response is String) {
        yield AttributeErrorState(error: response);
      }
    }
  }
}