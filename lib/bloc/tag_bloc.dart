import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/tag_repo.dart';
import 'package:selvium/models/tag.dart';

class TagBloc {
  TagRepository _tagRepository;
  StreamController _tagListController;

  StreamSink<Response<Tags>> get tagListSink =>
      _tagListController.sink;

  Stream<Response<Tags>> get tagListStream =>
      _tagListController.stream;

  TagBloc() {
    _tagListController = StreamController<Response<Tags>>();
    _tagRepository = TagRepository();
    fetchTags();
  }

  fetchTags() async {
    tagListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Tags tags =
      await _tagRepository.fetchTagData();
      tagListSink.add(Response.completed(tags));
    } catch (e) {
      tagListSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _tagListController?.close();
  }
}