import 'package:selvium/models/attribute.dart';
import 'package:flutter/foundation.dart';

abstract class AttributeState {
  const AttributeState();
}

class AttributeInitialState extends AttributeState {
  const AttributeInitialState();
}

class AttributeLoadingState extends AttributeState {
  final String message;

  const AttributeLoadingState({
    @required this.message,
  });
}

class AttributeSuccessState extends AttributeState {
  final List<Attribute> attributes;

  const AttributeSuccessState({
    @required this.attributes,
  });
}

class AttributeErrorState extends AttributeState {
  final String error;

  const AttributeErrorState({
    @required this.error,
  });
}