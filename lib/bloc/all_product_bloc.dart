import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/all_product_repo.dart';
import 'package:selvium/models/product.dart';

class AllProductBloc {
  AllProductRepository _productRepository;
  StreamController _productListController;

  StreamSink<Response<Products>> get productListSink =>
      _productListController.sink;

  Stream<Response<Products>> get productListStream =>
      _productListController.stream;

  AllProductBloc(String parameter,String value) {
    _productListController = StreamController<Response<Products>>();
    _productRepository = AllProductRepository();
    fetchAllProducts(parameter,value);
  }
  fetchAllProducts(String parameter,String value) async {
    productListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Products cats =
      await _productRepository.fetchAllProductData(parameter,value);
      productListSink.add(Response.completed(cats));
    } catch (e) {
      productListSink.add(Response.error(e.toString()));
      print(e);
    }
  }
  dispose() {
    _productListController?.close();
  }
}