import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/category_repo.dart';
import 'package:selvium/models/category.dart';

class CategoryBloc {
  CategoryRepository _categoryRepository;
  StreamController _categoryListController;

  StreamSink<Response<Categories>> get categoryListSink =>
      _categoryListController.sink;

  Stream<Response<Categories>> get categoryListStream =>
      _categoryListController.stream;

  CategoryBloc() {
    _categoryListController = StreamController<Response<Categories>>();
    _categoryRepository = CategoryRepository();
    fetchCategories();

  }

  fetchCategories() async {
    categoryListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Categories cats =
      await _categoryRepository.fetchCategoryData();
      categoryListSink.add(Response.completed(cats));
    } catch (e) {
      categoryListSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _categoryListController?.close();
  }
}