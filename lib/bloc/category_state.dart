import 'package:selvium/models/category.dart' as category;
import 'package:flutter/foundation.dart';

abstract class CategoryState {
  const CategoryState();
}

class CategoryInitialState extends CategoryState {
  const CategoryInitialState();
}

class CategoryLoadingState extends CategoryState {
  final String message;

  const CategoryLoadingState({
    @required this.message,
  });
}

class CategorySuccessState extends CategoryState {
  final List<category.Category> categories;

  const CategorySuccessState({
    @required this.categories,
  });
}

class CategoryErrorState extends CategoryState {
  final String error;

  const CategoryErrorState({
    @required this.error,
  });
}