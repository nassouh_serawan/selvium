import 'dart:async';
import 'package:selvium/repository/all_category_repo.dart';
import 'package:selvium/models/category.dart' as category;
import 'event.dart';
import 'category_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class AllCategoryBloc extends Bloc<Event, CategoryState> {
  final AllCategoryRepository allCategoryRepository;
  int page = 1;
  bool isFetching = false;
  AllCategoryBloc({
    @required this.allCategoryRepository,

  }) : super(CategoryInitialState());

  @override
  Stream<CategoryState> mapEventToState(Event event) async* {
    if (event is FetchEvent) {
      yield CategoryLoadingState(message: 'تحميل التصنيفات');
      final response = await allCategoryRepository.getCategories(page: page.toString(),);
      print(response);
      if (response is http.Response) {
        if (response.statusCode == 200) {
          final categories = jsonDecode(response.body.substring(11)) as List;
          yield CategorySuccessState(
            categories: categories.map((cat) => category.Category.fromJson(cat)).toList(),
          );
          page++;
        } else {
          yield CategoryErrorState(error: response.body);
        }
      } else if (response is String) {
        yield CategoryErrorState(error: response);
      }
    }
  }
}