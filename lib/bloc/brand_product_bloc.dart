import 'dart:async';
import 'package:selvium/repository/brand_product_repo.dart';
import 'package:selvium/models/product.dart';
import 'event.dart';
import 'product_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class BrandProductBloc extends Bloc<Event, ProductState> {
  final BrandProductRepository brandProductRepository;
  int page = 1;
  bool isFetching = false;
  String parameter1;
  String parameter2;
  String value1;
  String value2;
  BrandProductBloc({
    @required this.brandProductRepository,
    @required this.parameter1,@required this.value1,this.parameter2,this.value2
  }) : super(ProductInitialState());

  @override
  Stream<ProductState> mapEventToState(Event event) async* {
    if (event is FetchEvent) {
      yield ProductLoadingState(message: 'تحميل المنتجات');
      final response = await brandProductRepository.getBrandProduct(page: page.toString(),parameter1: parameter1,value1: value1,parameter2: parameter2,value2: value2);
      if (response is http.Response) {
        if (response.statusCode == 200) {
          final products = jsonDecode(response.body.substring(11)) as List;
          yield ProductSuccessState(
            products: products.map((product) => Product.fromJson(product)).toList(),
          );
          page++;
        } else {
          yield ProductErrorState(error: response.body);
        }
      } else if (response is String) {
        yield ProductErrorState(error: response);
      }
    }
  }
  dispose() {
    this?.close();
  }
}

