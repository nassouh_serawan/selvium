import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/product_repo.dart';
import 'package:selvium/models/product.dart';

class ProductBloc {
  ProductRepository _productRepository;
  StreamController _productListController;

  StreamSink<Response<Products>> get productListSink =>
      _productListController.sink;

  Stream<Response<Products>> get productListStream =>
      _productListController.stream;

  ProductBloc(String parameter,String value) {
    _productListController = StreamController<Response<Products>>();
    _productRepository = ProductRepository();
    fetchProducts(parameter,value);
  }

  fetchProducts(String parameter,String value) async {
    productListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Products cats =
      await _productRepository.fetchProductData(parameter,value);
      productListSink.add(Response.completed(cats));
    } catch (e) {
      productListSink.add(Response.error(e.toString()));
      print(e);
    }
  }
  dispose() {
    _productListController?.close();
  }
}