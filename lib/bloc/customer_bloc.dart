import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/customer_repo.dart';
import 'package:selvium/models/customer.dart';

class CustomerBloc {
  CustomerRepository _customerRepository;
  StreamController _customerDataController;

  StreamSink<Response<Customer>> get customerDataSink =>
      _customerDataController.sink;

  Stream<Response<Customer>> get customerDataStream =>
      _customerDataController.stream;

  CustomerBloc() {
    _customerDataController = StreamController<Response<Customer>>();
    _customerRepository = CustomerRepository();
    fetchCustomerData();
  }

  fetchCustomerData() async {
    customerDataSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Customer cus = await _customerRepository.fetchCustomerData();
      customerDataSink.add(Response.completed(cus));
    } catch (e) {
      customerDataSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _customerDataController?.close();
  }
}