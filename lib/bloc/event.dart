
abstract class Event {
  const Event();
}

class FetchEvent extends Event {
  const FetchEvent();
}