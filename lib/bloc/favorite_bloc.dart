import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/favorite_repo.dart';
import 'package:selvium/models/favorite.dart';

class FavoriteBloc {
  FavoriteRepository _favoriteRepository;
  StreamController _favoriteListController;

  StreamSink<Response<Favorites>> get favoriteListSink =>
      _favoriteListController.sink;

  Stream<Response<Favorites>> get favoriteListStream =>
      _favoriteListController.stream;

  FavoriteBloc() {
    _favoriteListController = StreamController<Response<Favorites>>();
    _favoriteRepository = FavoriteRepository();
    fetchFavorites();
  }

  fetchFavorites() async {
    favoriteListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Favorites cats =
      await _favoriteRepository.fetchFavoriteData();
      favoriteListSink.add(Response.completed(cats));
    } catch (e) {
      favoriteListSink.add(Response.error(e.toString()));
      print(e);
    }
  }
  dispose() {
    _favoriteListController?.close();
  }
}