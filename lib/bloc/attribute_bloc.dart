import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/attribute_repo.dart';
import 'package:selvium/models/attribute.dart';

class AttributeBloc {
  AttributeRepository _attributeRepository;
  StreamController _attributeListController;

  StreamSink<Response<Attributes>> get attributeListSink =>
      _attributeListController.sink;

  Stream<Response<Attributes>> get attributeListStream =>
      _attributeListController.stream;

  AttributeBloc(int id) {
    _attributeListController = StreamController<Response<Attributes>>();
    _attributeRepository = AttributeRepository();
    fetchAttributes(id);
  }

  fetchAttributes(int id) async {
    attributeListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Attributes attr =
      await _attributeRepository.fetchAttributeData(id);
      attributeListSink.add(Response.completed(attr));
    } catch (e) {
      attributeListSink.add(Response.error(e.toString()));
      print(e);
    }
  }
  dispose() {
    _attributeListController?.close();
  }
}