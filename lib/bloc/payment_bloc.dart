import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/payment_repo.dart';
import 'package:selvium/models/payment.dart';

class PaymentBloc {
  PaymentRepository _paymentRepository;
  StreamController _paymentListController;

  StreamSink<Response<Payments>> get paymentListSink =>
      _paymentListController.sink;

  Stream<Response<Payments>> get paymentListStream =>
      _paymentListController.stream;

  PaymentBloc() {
    _paymentListController = StreamController<Response<Payments>>();
    _paymentRepository = PaymentRepository();
    fetchPayment();

  }

  fetchPayment() async {
    paymentListSink.add(Response.loading('جاري تحميل البيانات'));
    try {
      Payments cats =
      await _paymentRepository.fetchPayment();
      paymentListSink.add(Response.completed(cats));
    } catch (e) {
      paymentListSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _paymentListController?.close();

  }
}