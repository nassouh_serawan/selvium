import 'dart:async';
import 'package:selvium/services/response.dart';
import 'package:selvium/repository/search_product_repo.dart';
import 'package:selvium/models/product.dart';

class SearchProductBloc {
  SearchProductRepository _productRepository;
  StreamController _productListController;

  StreamSink<Response<Products>> get productListSink =>
      _productListController.sink;

  Stream<Response<Products>> get productListStream =>
      _productListController.stream;

  SearchProductBloc(String value) {
    _productListController = StreamController<Response<Products>>();
    _productRepository = SearchProductRepository();
    fetchSearchProducts(value);
  }
  fetchSearchProducts(String value) async {
    productListSink.add(Response.loading('جاري البحث'));
    try {
      Products cats =
      await _productRepository.fetchSearchProductData(value);
      productListSink.add(Response.completed(cats));
    } catch (e) {
      productListSink.add(Response.error(e.toString()));
      print(e);
    }
  }
  dispose() {
    _productListController?.close();
  }
}