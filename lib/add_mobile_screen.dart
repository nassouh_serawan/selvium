import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/services/api.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_screen.dart';
import 'package:selvium/bloc/customer_bloc.dart';
import 'constant.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/services/response.dart';
class AddMobileScreen extends StatefulWidget {
  final int customerId;
  AddMobileScreen(this.customerId);
  @override
  _AddMobileScreenState createState() => _AddMobileScreenState();
}

class _AddMobileScreenState extends State<AddMobileScreen> {
  CustomerBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = CustomerBloc();
  }
  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
          leading:IconButton(icon: SvgPicture.asset('images/Back-Arrow-Black.svg',height: 30,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title:Text('اضافة رقم موبايل',style: kTitleTextStyle)
      ),
      body:  RefreshIndicator(
        onRefresh: () => _bloc.fetchCustomerData(),
        child: StreamBuilder<Response<Customer>>(
          stream: _bloc.customerDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return MobileView(customer: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () =>
                        _bloc.fetchCustomerData(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}
class MobileView extends StatefulWidget {
  final Customer customer;
  const MobileView({Key key, this.customer}) : super(key: key);
  @override
  _MobileViewState createState() => _MobileViewState();
}

class _MobileViewState extends State<MobileView> {
  bool agreed=true;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  Flushbar flush;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _num = TextEditingController();
  String validatePass(value){
    if (value.length != 10)
      return 'لطفا" أدخل عشرة أرقام';
    else
      return null;
  }
  String validatePass2(value){
    if (value.length < 1)
      return 'مطلوب';
    else
      return null;
  }
  changeNumber()async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      var data = {"billing": {
        "phone": _num.text+_phone.text
      },};
      try{
        final response = await _provider.put(
            'wp-json/wc/v3/customers/' + widget.customer.id.toString(),
            {ck: ckCode, cs: csCode},
            data);
        var cus = Customer.fromJson(response);
        if (cus != null) {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'تم تغيير رقم الهاتف',
              message: 'تم تغيير رقم الهاتف بنجاح',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.green,
              boxShadows: [
                BoxShadow(
                  color: Colors.green[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        } else {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'حدث خطأ ما',
              message: 'الرجاء المحاولة مرة أخرى',
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.red,
              boxShadows: [
                BoxShadow(
                  color: Colors.red[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        }
      }catch(e){
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: e.toString(),
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading = false;
        });
      }
    }else{setState(() {
      _autoValidate = true;
    });}
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _num.text=widget.customer.billing.phone.length<3?'':widget.customer.billing.phone.substring(0,3);
    _phone.text=widget.customer.billing.phone.length<4?'':widget.customer.billing.phone.substring(3);
  }
  @override
  Widget build(BuildContext context) {
    return loading?Loading(loadingMessage: 'الرجاء الانتظار',): Container(
      height:
      MediaQuery.of(context).orientation==Orientation.portrait?
      MediaQuery.of(context).size.height>800?
      MediaQuery.of(context).size.height*0.35:
      MediaQuery.of(context).size.height*0.5:
      MediaQuery.of(context).size.width>800?
      MediaQuery.of(context).size.width*0.3:
      MediaQuery.of(context).size.width*0.4,

      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        color: Color(0xffffffff),
        borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
      ),
      child:SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child:
                Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(height:
                    MediaQuery.of(context).size.height>900?
                    MediaQuery.of(context).size.height*0.06:
                    MediaQuery.of(context).size.height*0.015,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('ما هو رقم الموبايل الخاص بك',style: TextStyle(fontWeight: FontWeight.w700),)
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('أدخل رقم الموبايل للمتابعة',style: TextStyle(fontWeight: FontWeight.w100,fontSize: 11),)
                      ],
                    ),
                    Form(
                      key: _formKey,
                      child: Row(
                        children: [
                          Flexible(
                            flex: 3,
                            child: Directionality(
                              textDirection: TextDirection.ltr,
                              child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  decoration: kContainerDecoration,
                                  child: TextFormField(
                                    onSaved:(value) {
                                      _phone.text=value;
                                    },
                                    keyboardType: TextInputType.number,
                                    validator: validatePass,
                                    controller: _phone,
                                    decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle ),)),
                            ),
                          ),
                          Flexible(
                              flex: 1,
                              child: Directionality(
                                textDirection: TextDirection.ltr,
                                child: Container(
                                    margin: EdgeInsets.only(left: 10),
                                    decoration: kContainerDecoration,
                                    child: TextFormField(
                                      onSaved:(value) {
                                        _num.text=value;
                                      },
                                      keyboardType: TextInputType.phone,
                                      validator: validatePass2,
                                      controller: _num,
                                      decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle,prefixText: '+' ),)),
                              )
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              agreed=!agreed;
                            });
                          },
                          child: Row(
                            children: [
                              Container(
                                width: 30,
                                height: 30,
                                child: agreed?Image.asset('images/checked.png'):Image.asset('images/check.png'),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                child: Text('أوافق على جميع الشروط',),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FloatingActionButton(
                            child: Container(
                                width: double.infinity,
                                height: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        kColor2,kColor1
                                      ]),
                                ),
                                child: Icon(Icons.check)),
                            onPressed: agreed?changeNumber:null,
                          ),
                        ),

                      ],
                    ),
                  ],),

              ),
            )
          ],
        ),
      ) ,
    );
  }
}
