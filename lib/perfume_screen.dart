import 'package:flutter/widgets.dart';
import 'package:selvium/add_comment_screen.dart';
import 'package:selvium/billing_screen.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:selvium/rates_screen.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/cupertino.dart';
import 'bloc/single_product_bloc.dart';
import 'models/product.dart';
import 'services/response.dart';
import 'widgets/network_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:selvium/services/api.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/models/item.dart';
import 'dart:core';
import 'dart:async';
import 'dart:convert';
import 'package:flushbar/flushbar.dart';

int number = 1;
Customer customer;
List<Item> items = [];
List<int> prices = [];

class PerfumeScreen extends StatefulWidget {
  final int id;
  PerfumeScreen(this.id);
  @override
  _PerfumeScreenState createState() => _PerfumeScreenState();
}

class _PerfumeScreenState extends State<PerfumeScreen> {
  ApiProvider _provider = ApiProvider();
  String uid;
  double rate = 0;

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    uid = localStorage.getString('userId');
    final response = await _provider.get("wp-json/wc/v2/customers/$uid", {
      ck: ckCode,
      cs: csCode,
    });
    setState(() {
      customer = Customer.fromJson(response);
    });
    print(uid);
    print(customer);
    getReview();
  }

  getReview() async {
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'getRateSystem',
      'user_id': uid
    };
    final response = await _provider.get2("CustomAPI.php", queryParameters);
    setState(() {
      rate = double.tryParse(response.toString());
    });
  }

  SingleProductBloc _perfumeBloc;
  ScrollController _myController = ScrollController();
  bool checked = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
    _perfumeBloc = SingleProductBloc(widget.id);
    setState(() {
      items = [];
      prices = [];
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _perfumeBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //Timer(Duration(milliseconds: 100), () => _myController.jumpTo(_myController.position.maxScrollExtent));
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        leading: IconButton(
            icon: SvgPicture.asset(
              'images/Back-Arrow-Black.svg',
              height: 20,
              color: Colors.white,
            ),
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "العطر",
          style: kTitleTextStyle3,
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[kColor2, kColor1])),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () => _perfumeBloc.fetchProductData(widget.id),
        child: StreamBuilder<Response<Product>>(
          stream: _perfumeBloc.productDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return PerfumeView(
                    product: snapshot.data.data,
                    rate: rate,
                    customer: customer,
                  );
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () =>
                        _perfumeBloc.fetchProductData(widget.id),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class PerfumeView extends StatefulWidget {
  final Product product;
  final double rate;
  final Customer customer;
  PerfumeView({this.product, this.rate, this.customer});

  @override
  _PerfumeViewState createState() => _PerfumeViewState();
}

class _PerfumeViewState extends State<PerfumeView> {
  bool loadingCart = false;
  bool loadingFav = false;
  Flushbar flush;
  addToWish(int id, String price, var context) async {
    setState(() {
      loadingFav = true;
    });
    Flushbar flush;
    ApiProvider _provider = ApiProvider();
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'addWishItem',
      'prod_id': '$id',
      'user_id': widget.customer.id.toString(),
      'wish_id': '1',
      'quantity': '1',
      'original_price': '$price',
      'original_currency': 'SAR'
    };
    try {
      final response = await _provider.get2("CustomAPI.php", queryParameters);
      if (response == 1) {
        flush = Flushbar(
            titleText: Text(
              'تمت الاضافة للمفضلة',
              style: TextStyle(color: Colors.green),
            ),
            messageText: Text(
              'تم اضافة عنصر جديد الى المفضلة بنجاح',
              style: TextStyle(color: Colors.green),
            ),
            // showProgressIndicator: true,
            duration: Duration(seconds: 4),
            // leftBarIndicatorColor: viewAll,
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.green,
            backgroundColor: Colors.white,
            // backgroundGradient: LinearGradient(
            //         begin: Alignment.centerLeft,
            //         end: Alignment.centerRight,
            //         colors: <Color>[kColor2, kColor1]),
            boxShadows: [
              BoxShadow(
                color: Colors.green,
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.green,
              ),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else if (response == 2) {
        flush = Flushbar(
            titleText: Text(
              'تم الحذف من المفضلة',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'تم حذف العنصر من المفضلة بنجاح',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.redAccent.shade700,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            titleText: Text(
              'حدث خطأ ما',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'الرجاء المحاولة مرة أخرى',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.red,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
      setState(() {
        loadingFav = false;
      });
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
      setState(() {
        loadingFav = false;
      });
    }
  }

  addToCart() async {
    setState(() {
      loadingCart = true;
    });
    ApiProvider _provider = ApiProvider();
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tkn = jsonDecode(localStorage.getString('token'));
    var queryParameters = {'fun': 'addProducttoCart', 'token': "$tkn"};
    var data = {
      "p_id": widget.product.id.toString(),
      "quantity": number.toString(),
      "variation": widget.product.variations.isEmpty
          ? "1"
          : widget.product.variations.last.toString()
    };

    try {
      print(queryParameters);
      print(data);
      final response =
          await _provider.post4("CustomAPI.php", queryParameters, data);
      if (response == 1) {
        flush = Flushbar(
            titleText: Text(
              'تمت الاضافة للمفضلة',
              style: TextStyle(color: Colors.green),
            ),
            messageText: Text(
              'تم اضافة عنصر جديد الى المفضلة بنجاح',
              style: TextStyle(color: Colors.green),
            ),
            // showProgressIndicator: true,
            duration: Duration(seconds: 4),
            // leftBarIndicatorColor: viewAll,
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.green,
            backgroundColor: Colors.white,
            // backgroundGradient: LinearGradient(
            //         begin: Alignment.centerLeft,
            //         end: Alignment.centerRight,
            //         colors: <Color>[kColor2, kColor1]),
            boxShadows: [
              BoxShadow(
                color: Colors.green,
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.green,
              ),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else if (response == 2) {
        flush = Flushbar(
            titleText: Text(
              'تم الحذف من المفضلة',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            messageText: Text(
              'تم حذف العنصر من المفضلة بنجاح',
              style: TextStyle(color: Colors.redAccent.shade700),
            ),
            duration: Duration(seconds: 4),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.redAccent.shade700,
            backgroundColor: Colors.white,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close, color: Colors.redAccent.shade700),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: 'لا يمكن اضافة هذا العنصر للسلة',
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
      setState(() {
        loadingCart = false;
      });
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
      setState(() {
        loadingCart = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: <Color>[kColor2, kColor1])),
        child: SafeArea(
          child: MediaQuery.of(context).orientation == Orientation.portrait
              ? Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Positioned(
                      bottom: 20,
                      child: Card(
                        elevation: 8,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: MediaQuery.of(context).size.height * 0.6,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  children: [
                                    Container(
                                      height: MediaQuery.of(context)
                                                  .size
                                                  .height >
                                              600
                                          ? MediaQuery.of(context).size.height *
                                              0.08
                                          : MediaQuery.of(context).size.height *
                                              0.12,
                                    ),
                                    Center(
                                      child: Text(widget.product.name,
                                          style: kTitleTextStyle,
                                          textAlign: TextAlign.center),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          widget.product.price + ' ر.س',
                                          style: TextStyle(
                                              // color: viewAll,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    AddCommentScreen(
                                                      productId:
                                                          widget.product.id,
                                                      rate: double.tryParse(
                                                          widget.product
                                                              .averageRating),
                                                      customer: widget.customer,
                                                    )));
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            double.tryParse(widget.product
                                                        .averageRating) >
                                                    0
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 30
                                                : 20,
                                            color: viewAll,
                                          ),
                                          Icon(
                                            double.tryParse(widget.product
                                                        .averageRating) >
                                                    1
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 30
                                                : 20,
                                            color: viewAll,
                                          ),
                                          Icon(
                                            double.tryParse(widget.product
                                                        .averageRating) >
                                                    2
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 30
                                                : 20,
                                            color: viewAll,
                                          ),
                                          Icon(
                                            double.tryParse(widget.product
                                                        .averageRating) >
                                                    3
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 30
                                                : 20,
                                            color: viewAll,
                                          ),
                                          Icon(
                                            double.tryParse(widget.product
                                                        .averageRating) >
                                                    4
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 30
                                                : 20,
                                            color: viewAll,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                MediaQuery.of(context).size.height > 600
                                    ? SizedBox(
                                        height: 30,
                                      )
                                    : SizedBox(
                                        height: 10,
                                      ),
                                Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('العدد'),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            if (number > 1)
                                              setState(() {
                                                number = number - 1;
                                              });
                                          },
                                          child: Image.asset(
                                            'images/min.PNG',
                                            height: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 30,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Text(
                                          number.toString(),
                                          style: TextStyle(
                                              fontSize: MediaQuery.of(context)
                                                          .size
                                                          .height >
                                                      800
                                                  ? 30
                                                  : 20),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            if (number < 100)
                                              setState(() {
                                                number = number + 1;
                                              });
                                          },
                                          child: Image.asset(
                                            'images/plus.PNG',
                                            height: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 30,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                MediaQuery.of(context).size.height > 600
                                    ? SizedBox(
                                        height: 30,
                                      )
                                    : SizedBox(
                                        height: 10,
                                      ),
                                Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              items = [];
                                              prices = [];
                                            });
                                            items.add(Item(
                                                productId: widget.product.id,
                                                quantity: number));
                                            prices.add(int.parse(
                                                    widget.product.price) *
                                                number);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        BillingScreen(customer,
                                                            items, prices)));
                                          },
                                          child: Container(
                                            padding: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? EdgeInsets.all(5)
                                                : EdgeInsets.all(1),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.5,
                                            height: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 30,
                                            decoration:
                                                kContainerButtonDecoration2,
                                            child: Text(
                                              'الشراء الآن',
                                              textAlign: TextAlign.center,
                                              style: kTitleTextStyle.copyWith(
                                                  color: Colors.red,
                                                  fontSize: 17),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        GestureDetector(
                                          onTap: addToCart,
                                          child: Container(
                                            padding: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? EdgeInsets.all(5)
                                                : EdgeInsets.all(1),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.5,
                                            height: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 30,
                                            decoration:
                                                kContainerButtonDecoration,
                                            child: loadingCart
                                                ? Text(
                                                    'الرجاء الانتظار',
                                                    style: kTitleTextStyle3
                                                        .copyWith(fontSize: 13),
                                                    textAlign: TextAlign.center,
                                                  )
                                                : Text(
                                                    'اضافة إلى السلة',
                                                    textAlign: TextAlign.center,
                                                    style: kTitleTextStyle2
                                                        .copyWith(fontSize: 17),
                                                  ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0,
                      child: Card(
                        elevation: 8,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              color: viewAll,
                              width: 1.0,
                              style: BorderStyle.solid),
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                          // borderRadius: BorderRadius.circular(5.0),
                        ),
                        color: Colors.white,
                        // shape: RoundedRectangleBorder(
                        //     borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: MediaQuery.of(context).size.width * 0.6,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.4,
                                  height:
                                      MediaQuery.of(context).size.width * 0.4,
                                  child: Image.network(
                                      widget.product.images[0].src)),
                              GestureDetector(
                                onTap: () {
                                  addToWish(widget.product.id,
                                      widget.product.price, context);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 8.0, right: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      loadingFav
                                          ? CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      viewAll),
                                            )
                                          : Icon(
                                              CupertinoIcons.heart,
                                              color: viewAll,
                                              size: MediaQuery.of(context)
                                                          .orientation ==
                                                      Orientation.portrait
                                                  ? MediaQuery.of(context)
                                                              .size
                                                              .height >
                                                          600
                                                      ? 30
                                                      : 20
                                                  : MediaQuery.of(context)
                                                              .size
                                                              .width >
                                                          800
                                                      ? 30
                                                      : 20,
                                            ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Positioned(
                      right: 10,
                      child: Card(
                        elevation: 8,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                              color: viewAll,
                              width: 1.0,
                              style: BorderStyle.solid),
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                          // borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: MediaQuery.of(context).size.height * 0.7,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                          child: Text(
                                        widget.product.name,
                                        style: kTitleTextStyle4,
                                        textAlign: TextAlign.center,
                                      )),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        widget.product.price + ' ر.س',
                                        style: TextStyle(
                                            color: viewAll,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ],
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AddCommentScreen(
                                                      productId:
                                                          widget.product.id,
                                                      rate: double.tryParse(
                                                          widget.product
                                                              .averageRating),
                                                      customer:
                                                          widget.customer)));
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          double.tryParse(widget
                                                      .product.averageRating) >
                                                  0
                                              ? Icons.star
                                              : Icons.star_border,
                                          size: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 30
                                              : 20,
                                        ),
                                        Icon(
                                          double.tryParse(widget
                                                      .product.averageRating) >
                                                  1
                                              ? Icons.star
                                              : Icons.star_border,
                                          color: viewAll,
                                          size: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 30
                                              : 20,
                                        ),
                                        Icon(
                                          double.tryParse(widget
                                                      .product.averageRating) >
                                                  2
                                              ? Icons.star
                                              : Icons.star_border,
                                          size: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 30
                                              : 20,
                                        ),
                                        Icon(
                                          double.tryParse(widget
                                                      .product.averageRating) >
                                                  3
                                              ? Icons.star
                                              : Icons.star_border,
                                          size: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 30
                                              : 20,
                                        ),
                                        Icon(
                                          double.tryParse(widget
                                                      .product.averageRating) >
                                                  4
                                              ? Icons.star
                                              : Icons.star_border,
                                          size: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 30
                                              : 20,
                                        ),
                                      ],
                                    ),
                                  ),
                                  MediaQuery.of(context).size.height > 800
                                      ? SizedBox(
                                          height: 20,
                                        )
                                      : SizedBox(
                                          height: 0,
                                        ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text('العدد'),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          if (number > 1)
                                            setState(() {
                                              number = number - 1;
                                            });
                                        },
                                        child: Image.asset(
                                          'images/min.PNG',
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 40
                                              : 20,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        number.toString(),
                                        style: TextStyle(
                                            fontSize: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 30
                                                : 15),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          if (number < 100)
                                            setState(() {
                                              number = number + 1;
                                            });
                                        },
                                        child: Image.asset(
                                          'images/plus.PNG',
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 40
                                              : 20,
                                        ),
                                      ),
                                    ],
                                  ),
                                  MediaQuery.of(context).size.height > 800
                                      ? SizedBox(
                                          height: 20,
                                        )
                                      : SizedBox(
                                          height: 5,
                                        ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            items = [];
                                            prices = [];
                                          });
                                          items.add(Item(
                                              productId: widget.product.id,
                                              quantity: number));
                                          prices.add(
                                              int.parse(widget.product.price) *
                                                  number);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      BillingScreen(customer,
                                                          items, prices)));
                                        },
                                        child: Container(
                                          padding: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? EdgeInsets.all(5)
                                              : EdgeInsets.all(1),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.4,
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 35
                                              : 25,
                                          decoration:
                                              kContainerButtonDecoration2,
                                          child: Text(
                                            'الشراء الآن',
                                            textAlign: TextAlign.center,
                                            style: kTitleTextStyle4.copyWith(
                                                fontSize: 14),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: addToCart,
                                        child: Container(
                                          padding: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? EdgeInsets.all(5)
                                              : EdgeInsets.all(1),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.4,
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height >
                                                  800
                                              ? 35
                                              : 25,
                                          decoration:
                                              kContainerButtonDecoration,
                                          child: loadingCart
                                              ? Text(
                                                  'الرجاء الانتظار',
                                                  style: kTitleTextStyle3
                                                      .copyWith(fontSize: 13),
                                                  textAlign: TextAlign.center,
                                                )
                                              : Text(
                                                  'اضافة إلى السلة',
                                                  style: kTitleTextStyle3
                                                      .copyWith(fontSize: 13),
                                                  textAlign: TextAlign.center,
                                                ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      child: Card(
                        elevation: 8,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  height:
                                      MediaQuery.of(context).size.height * 0.3,
                                  child: Image.network(
                                      widget.product.images[0].src)),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 8.0, right: 8),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        addToWish(widget.product.id,
                                            widget.product.price, context);
                                      },
                                      child: loadingFav
                                          ? CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Colors.red),
                                            )
                                          : Icon(
                                              CupertinoIcons.heart,
                                              color: Colors.red,
                                              size: MediaQuery.of(context)
                                                          .orientation ==
                                                      Orientation.portrait
                                                  ? MediaQuery.of(context)
                                                              .size
                                                              .height >
                                                          600
                                                      ? 30
                                                      : 20
                                                  : MediaQuery.of(context)
                                                              .size
                                                              .width >
                                                          800
                                                      ? 30
                                                      : 20,
                                            ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
