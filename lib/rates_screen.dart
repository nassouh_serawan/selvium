import 'package:selvium/add_comment_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'constant.dart';
class RatesScreen extends StatefulWidget {
  @override
  _RatesScreenState createState() => _RatesScreenState();
}

class _RatesScreenState extends State<RatesScreen> {
  List rates=[1,2,3,4,5,6,7,8,9,10,11];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        leading:IconButton(icon: SvgPicture.asset('images/BackArrow-White-icon.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
        title: Text('التقييمات',style: kTitleTextStyle3,),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[
                    kColor2,kColor1
                  ])
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical:20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 9,
              child: ListView.builder(itemCount: rates.length,itemBuilder: (context, index) {
                return Card(
                child: ListTile(
                  leading: CircleAvatar(radius: 30,),
                  title: Text('ايلا وايت'),
                  subtitle:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                      children: [
                        Icon(Icons.star,size: MediaQuery.of(context).size.height>800?20:10,),
                        Icon(Icons.star,size: MediaQuery.of(context).size.height>800?20:10,),
                        Icon(Icons.star,size: MediaQuery.of(context).size.height>800?20:10,),
                        Icon(Icons.star,size: MediaQuery.of(context).size.height>800?20:10,),
                        Icon(Icons.star_border,size: MediaQuery.of(context).size.height>800?20:10,),
                        SizedBox(width: 5,),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text('اليوم'),
                        )
                      ],
                      ),
                      Text('رائع يستحق التقدير')
                    ],
                  ) ,
                ),
                );
    },),
            ),
            Flexible(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>AddCommentScreen()));},
                    child: Container(
                      padding: MediaQuery.of(context).size.height>800?EdgeInsets.all(5):EdgeInsets.all(2),
                      width: MediaQuery.of(context).size.width*0.7,
                      height: MediaQuery.of(context).size.height>800?35:25,
                      decoration: kContainerButtonDecoration2,
                      child: Text('اكتب تعليقا"',textAlign: TextAlign.center,style: TextStyle(color: Colors.red),),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
