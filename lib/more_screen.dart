import 'package:flutter/material.dart';
import 'package:selvium/account_screen.dart';
import 'package:selvium/add_mobile_screen.dart';
import 'package:selvium/category_screen.dart';
import 'package:selvium/favorite_screen.dart';
import 'package:selvium/rates_screen.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'سلفيوم',
          style: kTitleTextStyle3,
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[kColor2, kColor1])),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CategoryScreen(2541, 'جديدنا')));
              },
              child: ListTile(
                leading: SvgPicture.asset(
                  'images/New-icon.svg',
                  width: 30,
                ),
                title: Text('جديدنا', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CategoryScreen(2541, 'جديدنا')));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CategoryScreen(2546, 'المكياجات')));
              },
              child: ListTile(
                leading: SvgPicture.asset('images/makeup-Icon.svg', width: 30),
                title: Text('مكياجات', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CategoryScreen(2546, 'المكياجات')));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CategoryScreen(6591, 'التخفيضات')));
              },
              child: ListTile(
                leading: SvgPicture.asset('images/Sales-Icon.svg', width: 30),
                title: Text('التخفيضات', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CategoryScreen(6591, 'التخفيضات')));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CategoryScreen(2624, 'عطور المشاهير')));
              },
              child: ListTile(
                leading:
                    SvgPicture.asset('images/Famous-Perfume.svg', width: 30),
                title: Text('عطور المشاهير', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CategoryScreen(2624, 'عطور المشاهير')));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            CategoryScreen(6560, 'عروض اليوم الوطني')));
              },
              child: ListTile(
                leading:
                    SvgPicture.asset('images/Nation-Day-Icon.svg', width: 30),
                title: Text('عروض اليوم الوطني', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CategoryScreen(6560, 'عروض اليوم الوطني')));
                  },
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AccountScreen()));
              },
              child: ListTile(
                leading: SvgPicture.asset(
                  'images/My-Profile-Icon.svg',
                  width: 30,
                ),
                title: Text('حسابي', style: kTitleTextStyle),
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AccountScreen()));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
