import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/repository/all_attribute_repo.dart';
import 'package:selvium/search_screen.dart';
import 'constant.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/cupertino.dart';
import 'models/attribute.dart';
import 'services/response.dart';
import 'bloc/all_attribute_bloc.dart';
import 'package:selvium/brand_screen.dart';
import 'package:selvium/bloc/event.dart';
import 'package:selvium/bloc/attribute_state.dart';
import 'package:selvium/bloc/all_attribute_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BrandsScreen extends StatefulWidget {
  @override
  _BrandsScreenState createState() => _BrandsScreenState();
}

class _BrandsScreenState extends State<BrandsScreen> {
  double high = kToolbarHeight;
  bool search = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AllAttributeBloc(
          allAttributeRepository: AllAttributeRepository(), id: '2')
        ..add(FetchEvent()),
      child: Scaffold(
          backgroundColor: kBackgroundColor,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            centerTitle: true,
            leading: IconButton(
                icon: SvgPicture.asset(
                  'images/BackArrow-White-icon.svg',
                  height: 20,
                ),
                color: Colors.white,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => HomeScreen()));
                }),
            actions: [
              IconButton(
                icon: SvgPicture.asset(
                  'images/Search-white-icon.svg',
                  height: 20,
                ),
                color: Colors.white,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SearchScreen()));
                },
              ),
            ],
            title: Text(
              'الماركات',
              style: kTitleTextStyle3,
            ),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: <Color>[kColor2, kColor1])),
            ),
          ),
          body: AttributeBody()),
    );
  }
}

class AttributeBody extends StatelessWidget {
  final List<Attribute> _attributes = [];
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocConsumer<AllAttributeBloc, AttributeState>(
        listener: (context, attributeState) {
          if (attributeState is AttributeLoadingState) {
            // Scaffold.of(context)
            //   .showSnackBar(SnackBar(content: Text(attributeState.message)));
          } else if (attributeState is AttributeSuccessState &&
              attributeState.attributes.isEmpty) {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text('لا مزيد من الماركات')));
          } else if (attributeState is AttributeErrorState) {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text(attributeState.error)));
            context.read<AllAttributeBloc>().isFetching = false;
          }
          return;
        },
        builder: (context, attributeState) {
          if (attributeState is AttributeInitialState ||
              attributeState is AttributeLoadingState && _attributes.isEmpty) {
            return Loading(
              loadingMessage: 'جاري تحميل الماركات',
            );
          } else if (attributeState is AttributeSuccessState) {
            _attributes.addAll(attributeState.attributes);
            context.watch<AllAttributeBloc>().isFetching = false;
            Scaffold.of(context).hideCurrentSnackBar();
          } else if (attributeState is AttributeErrorState &&
              _attributes.isEmpty) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    context.read<AllAttributeBloc>()
                      ..isFetching = true
                      ..add(FetchEvent());
                  },
                  icon: Icon(Icons.refresh),
                ),
                const SizedBox(height: 15),
                Text(
                  attributeState.error,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red, fontSize: 9),
                ),
              ],
            );
          }
          return Column(
            children: [
              Flexible(
                child: GridView.builder(
                  controller: _scrollController
                    ..addListener(() {
                      if (_scrollController.offset ==
                              _scrollController.position.maxScrollExtent &&
                          !context.read<AllAttributeBloc>().isFetching) {
                        context.read<AllAttributeBloc>()
                          ..isFetching = true
                          ..add(FetchEvent());
                      }
                    }),
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? 2
                          : 4,
                      childAspectRatio: 0.8),
                  itemBuilder: (context, index) =>
                      AttributeListItem(_attributes[index]),
                  itemCount: _attributes.length,
                ),
              ),
              context.watch<AllAttributeBloc>().isFetching
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Loading(
                        loadingMessage: ' تحميل المزيد من الماركات',
                      ),
                    )
                  : Container(),
            ],
          );
        },
      ),
    );
  }
}

class AttributeListItem extends StatelessWidget {
  final Attribute attribute;

  const AttributeListItem(this.attribute);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    BrandScreen(attribute.id, attribute.name)));
      },
      child: Card(
        shape: RoundedRectangleBorder(
          side:
              BorderSide(color: viewAll, width: 1.0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          // borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Image.asset("images/p3.png"),
            ),
            Container(
              width: 150,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(attribute.name,
                      style: kTitleTextStyle4.copyWith(
                          fontSize: 15, fontWeight: FontWeight.w500)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
