import 'package:device_simulator/device_simulator.dart';
import 'package:flutter/material.dart';
import 'package:selvium/bloc/favorite_bloc.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/perfume_screen.dart';
import 'package:selvium/search_screen.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/cupertino.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/models/favorite.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'package:selvium/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'dart:convert';

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

class FavoriteScreen extends StatefulWidget {
  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  Favorites fav;
  bool search = false;
  String problem = "";
  bool loading = false;
  getFavorite() async {
    setState(() {
      loading = true;
      problem = "";
    });
    ApiProvider _provider = ApiProvider();
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    userId = jsonDecode(localStorage.getString('userId'));
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'getWishList',
      'user_id': '$userId'
    };
    try {
      final response = await _provider.get("CustomAPI.php", queryParameters);
      if (response == null) {
        setState(() {
          loading = false;
          problem = "_";
        });
      } else {
        setState(() {
          fav = Favorites.fromJson(response);
          loading = false;
        });
      }
    } catch (e) {
      setState(() {
        loading = false;
        problem = e.toString();
      });
    }
  }

  Future<void> handleClickMe(String id, String userId) async {
    return showDialog<void>(
      context: _scaffoldKey.currentContext,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('حذف العنصر'),
          content: Text(
              'هل تريد بالتأكيد حذف هذا العنصر من قائمة المفضلة الخاصة بك؟'),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(
                'لا',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
              child: Text('نعم', style: TextStyle(color: Colors.black)),
              onPressed: () {
                Navigator.pop(context);
                getFavorite();
                deleteWish(
                  id,
                  userId,
                );
              },
            ),
          ],
        );
      },
    );
  }

  deleteWish(String id, String userId) async {
    Flushbar flush;
    print(userId);
    ApiProvider _provider = ApiProvider();
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
      'fun': 'deleteWishItem',
      'item_id': '$id',
      'user_id': userId.toString(),
      'wish_id': '1',
      'quantity': '1',
      'original_currency': 'SAR'
    };
    try {
      final response = await _provider.get2("CustomAPI.php", queryParameters);
      if (response == 1) {
        flush = Flushbar(
            title: 'تم الحذف من المفضلة',
            message: 'تم حذف العنصر من المفضلة بنجاح',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.green,
            boxShadows: [
              BoxShadow(
                color: Colors.green[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else if (response == 2) {
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: 'يرجى المحاولة لاحقا"',
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: 'الرجاء المحاولة مرة أخرى',
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getFavorite();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: kBackgroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          leading: IconButton(
            icon: SvgPicture.asset(
              'images/BackArrow-White-icon.svg',
              height: 20,
            ),
            color: Colors.white,
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          actions: [
            IconButton(
              icon: SvgPicture.asset(
                'images/Search-white-icon.svg',
                height: 20,
              ),
              color: Colors.white,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              },
            ),
          ],
          title: Text(
            'المفضلة',
            style: kTitleTextStyle3,
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[kColor2, kColor1])),
          ),
        ),
        body: loading
            ? Loading(
                loadingMessage: 'تحميل بيانات المفضلة',
              )
            : problem != ""
                ? Error(
                    errorMessage: problem,
                    onRetryPressed: getFavorite,
                  )
                : fav.favorites.length > 0
                    ? GridView.builder(
                        itemCount: fav.favorites.length,
                        gridDelegate:
                            new SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount:
                                    MediaQuery.of(context).orientation ==
                                            Orientation.portrait
                                        ? 3
                                        : 4,
                                childAspectRatio: 0.65),
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onLongPress: () => handleClickMe(
                                fav.favorites[index].iD,
                                fav.favorites[index].userId),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PerfumeScreen(
                                          int.tryParse(
                                              fav.favorites[index].prodId))));
                            },
                            child: Column(
                              children: [
                                Card(
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: viewAll,
                                        width: 1.0,
                                        style: BorderStyle.solid),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                    // borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: MediaQuery.of(context)
                                                    .orientation ==
                                                Orientation.portrait
                                            ? MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    900
                                                ? MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.22
                                                : MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.09
                                            : MediaQuery.of(context)
                                                        .size
                                                        .width >
                                                    700
                                                ? MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.15
                                                : MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.1,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                        child:
                                            fav.favorites[index].image == null
                                                ? Image.asset("images/p3.png")
                                                : Image.network(
                                                    fav.favorites[index].image),
                                      ),
                                      Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 10),
                                                child: Icon(
                                                  CupertinoIcons.heart_solid,
                                                  color: viewAll,
                                                ),
                                              ),
                                              // Padding(
                                              //     padding:
                                              //         const EdgeInsets.only(
                                              //             bottom: 10),
                                              //     child: Text(fav
                                              //         .favorites[index]
                                              //         .quantity)),
                                            ],
                                          )),
                                    ],
                                  ),
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Text(
                                      fav.favorites[index].productName.length >
                                              18
                                          ? fav.favorites[index].productName
                                                  .substring(0, 18) +
                                              '...'
                                          : fav.favorites[index].productName,
                                      style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        fav.favorites[index].onSale != '0'
                                            ? Text(
                                                fav.favorites[index].onSale,
                                                textAlign: TextAlign.right,
                                                style: TextStyle(
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                    .size
                                                                    .height >
                                                                850
                                                            ? 11
                                                            : 8),
                                              )
                                            : Text(
                                                fav.favorites[index]
                                                    .originalPrice,
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: viewAll,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                    .size
                                                                    .height >
                                                                850
                                                            ? 10
                                                            : 7),
                                              ),
                                        fav.favorites[index].onSale != '0'
                                            ? Text(
                                                fav.favorites[index]
                                                    .originalPrice,
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                    .size
                                                                    .height >
                                                                850
                                                            ? 10
                                                            : 7,
                                                    decoration: TextDecoration
                                                        .lineThrough,
                                                    color: Colors.red),
                                              )
                                            : Container(),
                                      ],
                                    ))
                              ],
                            ),
                          );
                        })
                    : Center(
                        child: Text('لا يوجد عناصر في المفضلة الخاصة بك'),
                      ));
  }
}
