import 'package:selvium/account_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:selvium/bloc/customer_bloc.dart';
import 'package:selvium/home_screen.dart';
import 'constant.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/services/api.dart';
import 'package:flushbar/flushbar.dart';
class AccountEditScreen extends StatefulWidget {
  final int customerId;
  AccountEditScreen(this.customerId);
  @override
  _AccountEditScreenState createState() => _AccountEditScreenState();
}

class _AccountEditScreenState extends State<AccountEditScreen> {
  CustomerBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = CustomerBloc();
  }
  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
          leading: IconButton(icon: SvgPicture.asset('images/Back-Arrow-Black.svg',height: 20,),color: Colors.white,
              onPressed: () {Navigator.pop(context);}
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title:Text('سلفيوم',style: kTitleTextStyle)
      ),
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchCustomerData(),
        child: StreamBuilder<Response<Customer>>(
          stream: _bloc.customerDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return AccountView(customer: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () =>
                        _bloc.fetchCustomerData(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}
class AccountView extends StatefulWidget {
  final Customer customer;

  const AccountView({Key key, this.customer}) : super(key: key);
  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {

  final TextEditingController _email = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  Flushbar flush;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  final TextEditingController _num = TextEditingController();
  String validateUser(value){
    if (value.length < 1)
      return 'لطفا" أدخل اسم المستخدم';
    else
      return null;
  }
  String validateEmail(value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'الرجاء ادخال عنوان بريد الكتروني صالح';
    else
      return null;
  }
  String validatePass(value){
    if (value.length != 10)
      return 'لطفا" أدخل عشرة أرقام';
    else
      return null;
  }
  String validatePass2(value){
    if (value.length != 3)
      return 'ثلاث أرقام';
    else
      return null;
  }
  changeData()async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      var data = {
        'username':_name.text,
        'email':_email.text,
        "billing": {
        "phone": _num.text+_phone.text
      },};
      try{
        final response = await _provider.put(
            'wp-json/wc/v3/customers/' + widget.customer.id.toString(),
            {ck: ckCode, cs: csCode},
            data);
        if (response['code'] == null) {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'تم تعديل البيانات',
              message: 'تم تعديل البيانات بنجاح',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.green,
              boxShadows: [
                BoxShadow(
                  color: Colors.green[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        } else {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'حدث خطأ ما',
              message:
                  'هذا البريد الالكتروني مستخدم بالفعل يرجى استخدلم بريد آخر',
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.red,
              boxShadows: [
                BoxShadow(
                  color: Colors.red[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        }
      }catch(e){
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: e.toString(),
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading = false;
        });
      }
    }else{setState(() {
      _autoValidate = true;
    });}
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _num.text=widget.customer.billing.phone.length<3?'':widget.customer.billing.phone.substring(0,3);
    _phone.text=widget.customer.billing.phone.length<4?'':widget.customer.billing.phone.substring(3);
    _name.text=widget.customer.username;
    _email.text=widget.customer.email;

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:loading?Loading(loadingMessage: 'الرجاء الانتظار',):Container(
        height:
        MediaQuery.of(context).orientation==Orientation.portrait?
        MediaQuery.of(context).size.height>800?
        MediaQuery.of(context).size.height*0.5:
        MediaQuery.of(context).size.height*0.65:
        MediaQuery.of(context).size.width>800?
        MediaQuery.of(context).size.width*0.3:
        MediaQuery.of(context).size.width*0.4,

        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          color: Color(0xffffffff),
          borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
        ),
        child:SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: MediaQuery.of(context).orientation==Orientation.portrait?
                  Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius:
                          MediaQuery.of(context).size.width>500?
                          MediaQuery.of(context).size.width*0.1:
                          MediaQuery.of(context).size.width*0.15,
                          child: ClipOval(
                            child:Image(image: NetworkImage(widget.customer.avatarUrl),fit: BoxFit.cover,)
                            ,),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                        Column(children: [
                          Directionality(
                            textDirection: TextDirection.ltr,
                            child: Container(
                                decoration: kContainerDecoration,
                                child: TextFormField(
                                  onSaved:(value) {
                                    _name.text=value;
                                  },
                                  validator: validateUser,
                                  enabled: false,
                                  controller: _name,
                                  decoration: kInputContainerDecoration.copyWith(hintText:"االاسم" ,hintStyle:kHintTextStyle ),)),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                          Directionality(
                            textDirection: TextDirection.ltr,
                            child: Container(
                                decoration: kContainerDecoration,
                                child: TextFormField(
                                  onSaved:(value) {
                                    _email.text=value;
                                  },
                                  validator: validateEmail,
                                  controller: _email,
                                  decoration: kInputContainerDecoration.copyWith(hintText:"البريد الالكتروني" ,hintStyle:kHintTextStyle ),)),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                          Row(
                            children: [
                              Flexible(
                                flex: 3,
                                child: Directionality(
                                  textDirection: TextDirection.ltr,
                                  child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      decoration: kContainerDecoration,
                                      child: TextFormField(
                                        onSaved:(value) {
                                          _phone.text=value;
                                        },
                                        validator: validatePass,
                                        controller: _phone,
                                        decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle ),)),
                                ),
                              ),
                              Flexible(
                                  flex: 1,
                                  child: Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: Container(
                                        margin: EdgeInsets.only(left: 0),
                                        decoration: kContainerDecoration,
                                        child: TextFormField(
                                          onSaved:(value) {
                                            _num.text=value;
                                          },
                                          keyboardType: TextInputType.phone,
                                          validator: validatePass2,
                                          controller: _num,
                                          decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle,prefixText: '+' ),)),
                                  )
                              ),
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FloatingActionButton(
                                  child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        gradient: LinearGradient(
                                            begin: Alignment.centerLeft,
                                            end: Alignment.centerRight,
                                            colors: [
                                              kColor2,kColor1
                                            ]),
                                      ),
                                      child: Icon(Icons.check)),
                                  onPressed: changeData,
                                ),
                              ),

                            ],
                          ),
                        ],),

                      ],
                    ),
                  ):
                  Form(
                    key: _formKey,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Flexible(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Directionality(
                                textDirection: TextDirection.ltr,
                                child: Container(
                                    decoration: kContainerDecoration,
                                    child: TextFormField(
                                      onSaved:(value) {
                                        _name.text=value;
                                      },
                                      enabled: false,
                                      validator: validateUser,
                                      controller: _name,
                                      decoration: kInputContainerDecoration.copyWith(hintText:"الاسم" ,hintStyle:kHintTextStyle ),)),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                              Directionality(
                                textDirection: TextDirection.ltr,
                                child: Container(
                                    decoration: kContainerDecoration,
                                    child: TextFormField(
                                      onSaved:(value) {
                                        _email.text=value;
                                      },
                                      validator: validateEmail,
                                      controller: _email,
                                      decoration: kInputContainerDecoration.copyWith(hintText:"البريد الالكتروني" ,hintStyle:kHintTextStyle ),)),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height*0.01,),
                              Row(
                                children: [
                                  Flexible(
                                    flex: 3,
                                    child: Directionality(
                                      textDirection: TextDirection.ltr,
                                      child: Container(
                                          margin: EdgeInsets.only(left: 10),
                                          decoration: kContainerDecoration,
                                          child: TextFormField(
                                            controller: _phone,
                                            decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle ),)),
                                    ),
                                  ),
                                  Flexible(
                                      flex: 1,
                                      child: Directionality(
                                        textDirection: TextDirection.ltr,
                                        child: Container(
                                            margin: EdgeInsets.only(left: 0),
                                            decoration: kContainerDecoration,
                                            child: TextFormField(
                                              onSaved:(value) {
                                                _num.text=value;
                                              },
                                              keyboardType: TextInputType.phone,
                                              validator: validatePass2,
                                              controller: _num,
                                              decoration: kInputContainerDecoration.copyWith(hintText:"رقم الهاتف" ,hintStyle:kHintTextStyle,prefixText: '+' ),)),
                                      )
                                  ),
                                ],
                              ),

                            ],),
                        ),
                        Flexible(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius:
                                MediaQuery.of(context).size.width>500?
                                MediaQuery.of(context).size.width*0.1:
                                MediaQuery.of(context).size.width*0.15,
                                child: ClipOval(
                                  child:Image(image: NetworkImage(widget.customer.avatarUrl),fit: BoxFit.cover,)
                                  ,),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FloatingActionButton(
                                      child: Container(
                                          width: double.infinity,
                                          height: double.infinity,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(30),
                                            gradient: LinearGradient(
                                                begin: Alignment.centerLeft,
                                                end: Alignment.centerRight,
                                                colors: [
                                                  kColor2,kColor1
                                                ]),
                                          ),
                                          child: Icon(Icons.check)),
                                      onPressed: changeData,
                                    ),
                                  ),

                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),

                ),
              )
            ],
          ),
        ) ,
      ),
    );
  }
}