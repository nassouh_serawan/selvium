import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:selvium/perfume_screen.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/bloc/customer_bloc.dart';
import 'package:selvium/services/response.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'package:selvium/models/customer.dart';
import 'package:selvium/services/api.dart';
import 'package:flushbar/flushbar.dart';

class AddCommentScreen extends StatefulWidget {
  final Customer customer;
  final double rate;
  final int productId;
  AddCommentScreen({this.customer, this.rate, this.productId});
  @override
  _AddCommentScreenState createState() => _AddCommentScreenState();
}

class _AddCommentScreenState extends State<AddCommentScreen> {
  CustomerBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = CustomerBloc();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: SvgPicture.asset(
              'images/Back-Arrow-Black.svg',
              height: 30,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            color: Colors.white,
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: <Color>[kColor2, kColor1])),
          ),
          // backgroundColor: Color(0xffffffff),
          // decoration: BoxDecoration(
          //     gradient: LinearGradient(
          //         begin: Alignment.centerLeft,
          //         end: Alignment.centerRight,
          //         colors: <Color>[kColor2, kColor1])): ,
          title: Text('اكتب تعليقا',
              style: kTitleTextStyle3.copyWith(fontWeight: FontWeight.w700))),
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchCustomerData(),
        child: StreamBuilder<Response<Customer>>(
          stream: _bloc.customerDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return AccountView(
                    customer: snapshot.data.data,
                    rate: widget.rate,
                    productId: widget.productId,
                  );
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchCustomerData(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class AccountView extends StatefulWidget {
  final Customer customer;
  final double rate;
  final int productId;
  const AccountView({Key key, this.customer, this.rate, this.productId})
      : super(key: key);
  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  double rate = 0;
  String message = 'لا تعليق';
  Flushbar flush;
  ApiProvider _provider = ApiProvider();
  updateReview() async {
    var queryParameters = {
      ck: ckCode,
      cs: csCode,
    };
    var data = {
      "product_id": widget.productId,
      "review": message.toString(),
      "reviewer": widget.customer.username,
      "reviewer_email": widget.customer.email,
      "rating": rate
    };
    try {
      final response = await _provider.post2(
          "wp-json/wc/v3/products/reviews", queryParameters, data);
      print(response);
      if (response["id"] != null) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => PerfumeScreen(widget.productId)));
        flush = Flushbar(
            title: 'تم اضافة تقييم',
            message: 'تم اضافة تقييم بنجاح',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.green,
            boxShadows: [
              BoxShadow(
                color: Colors.green[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => PerfumeScreen(widget.productId)));
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: response["message"],
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //rate=widget.rate;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).orientation == Orientation.portrait
            ? MediaQuery.of(context).size.height > 800
                ? MediaQuery.of(context).size.height * 0.5
                : MediaQuery.of(context).size.height * 0.65
            : MediaQuery.of(context).size.width > 800
                ? MediaQuery.of(context).size.width * 0.3
                : MediaQuery.of(context).size.width * 0.4,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          color: Color(0xffffffff),
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20.0),
              bottomLeft: Radius.circular(20.0)),
        ),
        child: SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: MediaQuery.of(context).orientation ==
                          Orientation.portrait
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Card(
                              shape: CircleBorder(
                                side: BorderSide(
                                    color: viewAll,
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                              child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: MediaQuery.of(context).size.width > 500
                                    ? MediaQuery.of(context).size.width * 0.1
                                    : MediaQuery.of(context).size.width * 0.15,
                                child: ClipOval(
                                  child: Image(
                                    image:
                                        NetworkImage(widget.customer.avatarUrl),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Text(
                              widget.customer.username,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        rate = 1;
                                      });
                                    },
                                    child: Icon(
                                      rate > 0 ? Icons.star : Icons.star_border,
                                      size: MediaQuery.of(context).size.height >
                                              800
                                          ? 40
                                          : 20,
                                      color: viewAll,
                                    )),
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        rate = 2;
                                      });
                                    },
                                    child: Icon(
                                      rate > 1 ? Icons.star : Icons.star_border,
                                      size: MediaQuery.of(context).size.height >
                                              800
                                          ? 40
                                          : 20,
                                      color: viewAll,
                                    )),
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        rate = 3;
                                      });
                                    },
                                    child: Icon(
                                      rate > 2 ? Icons.star : Icons.star_border,
                                      size: MediaQuery.of(context).size.height >
                                              800
                                          ? 40
                                          : 20,
                                      color: viewAll,
                                    )),
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        rate = 4;
                                      });
                                    },
                                    child: Icon(
                                      rate > 3 ? Icons.star : Icons.star_border,
                                      size: MediaQuery.of(context).size.height >
                                              800
                                          ? 40
                                          : 20,
                                      color: viewAll,
                                    )),
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        rate = 5;
                                      });
                                    },
                                    child: Icon(
                                      rate > 4 ? Icons.star : Icons.star_border,
                                      size: MediaQuery.of(context).size.height >
                                              800
                                          ? 40
                                          : 20,
                                      color: viewAll,
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Container(
                                decoration: ShapeDecoration(
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: Colors.black,
                                        width: 1.0,
                                        style: BorderStyle.solid),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                  ),
                                ),
                                child: TextFormField(
                                    onChanged: (val) {
                                      setState(() {
                                        message = val;
                                      });
                                    },
                                    maxLines: 5,
                                    decoration: kInputContainerDecoration)),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    (message.split(' ').length - 1).toString() +
                                        'من اصل 100 كلمة',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FloatingActionButton(
                                    child: Container(
                                        width: double.infinity,
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          gradient: LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight,
                                              colors: [kColor2, kColor1]),
                                        ),
                                        child: Icon(Icons.check)),
                                    onPressed: () {
                                      SpinKitCircle(
                                        color: viewAll,
                                        size: 60.0,
                                      );
                                      updateReview();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Flexible(
                              flex: 2,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              rate = 1;
                                            });
                                          },
                                          child: Icon(
                                            rate > 0
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 20,
                                          )),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              rate = 2;
                                            });
                                          },
                                          child: Icon(
                                            rate > 1
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 20,
                                          )),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              rate = 3;
                                            });
                                          },
                                          child: Icon(
                                            rate > 2
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 20,
                                          )),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              rate = 4;
                                            });
                                          },
                                          child: Icon(
                                            rate > 3
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 20,
                                          )),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              rate = 5;
                                            });
                                          },
                                          child: Icon(
                                            rate > 4
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: MediaQuery.of(context)
                                                        .size
                                                        .height >
                                                    800
                                                ? 40
                                                : 20,
                                          )),
                                    ],
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.03,
                                  ),
                                  Container(
                                      decoration: ShapeDecoration(
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                              color: Colors.black,
                                              width: 1.0,
                                              style: BorderStyle.solid),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5.0)),
                                        ),
                                      ),
                                      child: TextFormField(
                                        onChanged: (val) {
                                          setState(() {
                                            message = val;
                                            print(message);
                                          });
                                        },
                                        maxLines: 5,
                                        decoration: kInputContainerDecoration,
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      (message.split(' ').length - 1)
                                              .toString() +
                                          'من اصل 100 كلمة',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Flexible(
                              flex: 1,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CircleAvatar(
                                    backgroundColor: Colors.transparent,
                                    radius: MediaQuery.of(context).size.width >
                                            500
                                        ? MediaQuery.of(context).size.width *
                                            0.1
                                        : MediaQuery.of(context).size.width *
                                            0.15,
                                    child: ClipOval(
                                      child: Image(
                                        image: NetworkImage(
                                            widget.customer.avatarUrl),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01,
                                  ),
                                  Text(widget.customer.username),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FloatingActionButton(
                                          child: Container(
                                              width: double.infinity,
                                              height: double.infinity,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(30),
                                                gradient: LinearGradient(
                                                    begin: Alignment.centerLeft,
                                                    end: Alignment.centerRight,
                                                    colors: [kColor2, kColor1]),
                                              ),
                                              child: Icon(Icons.check)),
                                          onPressed: updateReview,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
