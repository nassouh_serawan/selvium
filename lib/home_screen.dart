import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:selvium/account_screen.dart';
import 'package:selvium/basket_screen.dart';
import 'package:selvium/brands_screen.dart';
import 'package:selvium/favorite_screen.dart';
import 'package:selvium/market_screen.dart';
import 'package:selvium/more_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:device_simulator/device_simulator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'constant.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int pageIndex = 0;
  PageController pageController = PageController(initialPage: 2);
  onPageChanged(int pageIndex) {
    setState(() {
      this.pageIndex = pageIndex;
    });
  }

  onTap(int pageIndex) {
    pageController.jumpToPage(
      pageIndex,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
          children: <Widget>[
            BrandsScreen(),
            BasketScreen(),
            MarketScreen(),
            FavoriteScreen(),
            MoreScreen(),
          ],
          controller: pageController,
          onPageChanged: onPageChanged,
          physics: NeverScrollableScrollPhysics(),
        ),
        bottomNavigationBar: ConvexAppBar(
          style: TabStyle.textIn,

          // curveSize: 50.0,
          // backgroundColor: LinearGradient(
          //         begin: Alignment.centerLeft,
          //         end: Alignment.centerRight,
          //         colors: <Color>[
          //           kColor2,kColor1
          //         ])
          // ),,
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[kColor2, kColor1]),
          items: [
            // TabItem(
            //     icon: pageIndex == 0
            //         ? Padding(
            //             padding: const EdgeInsets.only(top: 5),
            //             child: SvgPicture.asset(
            //               'images/Store-Red-icon.svg',
            //               height: 30,
            //             ),
            //           )
            //         : Padding(
            //             padding: const EdgeInsets.only(top: 5),
            //             child: SvgPicture.asset(
            //               'images/Store-icon.svg',
            //               height: 30,
            //             ),
            //           ),
            //     title: 'المتجر'),

            TabItem(
                activeIcon: Padding(
                  padding: EdgeInsets.all(8),
                  child: SvgPicture.asset(
                    'images/Brand-Icon.svg',
                    color: Colors.white,
                  ),
                ),
                icon: SvgPicture.asset(
                  'images/Brand-Icon.svg',
                  color: Colors.white,
                ),
                title: "الماركات"),

            TabItem(
                activeIcon: Padding(
                  padding: EdgeInsets.all(8),
                  child: SvgPicture.asset(
                    'images/Cart-icon.svg',
                    color: Colors.white,
                  ),
                ),
                icon: SvgPicture.asset(
                  'images/Cart-icon.svg',
                  color: Colors.white,
                ),
                title: "السلة"),
            TabItem(
                activeIcon: Padding(
                  padding: EdgeInsets.all(8),
                  child: SvgPicture.asset(
                    'images/Store-icon.svg',
                    color: Colors.white,
                  ),
                ),
                icon: SvgPicture.asset(
                  'images/Store-icon.svg',
                  color: Colors.white,
                ),
                title: "المتجر"),
            TabItem(
                activeIcon: Padding(
                  padding: EdgeInsets.all(8),
                  child: SvgPicture.asset(
                    'images/Heart-Fill.svg',
                    color: Colors.white,
                  ),
                ),
                icon: SvgPicture.asset(
                  'images/Heart-Fill.svg',
                  color: Colors.white,
                ),
                title: "المفضلة"),
            TabItem(
                activeIcon: Padding(
                  padding: EdgeInsets.all(8),
                  child: SvgPicture.asset(
                    'images/more-Icon.svg',
                    color: Colors.white,
                  ),
                ),
                icon: SvgPicture.asset(
                  'images/more-Icon.svg',
                  color: Colors.white,
                ),
                title: "المزيد"),
          ],
          initialActiveIndex: 2, //optional, default as 0

          onTap: onTap,
        )
        /*bottomNavigationBar: CupertinoTabBar(
        currentIndex: pageIndex,
        onTap: onTap,
        activeColor: kColor2,
        inactiveColor:kColor2,
        items: [
          BottomNavigationBarItem(icon:pageIndex==0?Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Store-Red-icon.svg',height: 30,),
          ):Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Store-icon.svg',height: 30,),
          ),title: Text('المتجر')),
          BottomNavigationBarItem(icon:pageIndex==1?Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/my-Profile-Red-Icon.svg',height: 30,),
          ):Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/My-Profile-Icon.svg',height: 30,),
          ),title: Text('حسابي')),
          BottomNavigationBarItem(icon:pageIndex==2?Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Brand-Red-Icon.svg',height: 30,),
          ):Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Brand-Icon.svg',height: 30,),
          ),title: Text('الماركات')),
          BottomNavigationBarItem(icon:pageIndex==3?Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Cart-Red-icon.svg',height: 30,),
          ):Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Cart-icon.svg',height: 30,),
          ),title: Text('السلة')),
          BottomNavigationBarItem(icon:pageIndex==4?Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Heart-Fill.svg',height: 30,),
          ):Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/Heart-Border.svg',height: 30,),
          ),title: Text('المفضلة')),
          BottomNavigationBarItem(icon:pageIndex==5?Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/More-red-icon.svg',width: 30,),
          ):Padding(
            padding: const EdgeInsets.only(top:5),
            child: SvgPicture.asset('images/more-Icon.svg',width: 30,),
          ),title: Text('المزيد')),
        ],
      ),
      */
        );
  }
}
