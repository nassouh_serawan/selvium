import 'package:selvium/Newstyle/curve.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/sign_in_screen.dart';
import 'package:selvium/sign_up_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:selvium/widgets/network_widget.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool loading = true;
  checkLogin() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    if (token != null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: CustomPaint(
        //     size: Size(MediaQuery.of(context).size.width,
        //         MediaQuery.of(context).size.height),
        //     child: Container(
        //         // margin: EdgeInsets.fromLTRB(0, 100, 0, 0),

        //         alignment: Alignment.topCenter,
        //         child: Container(
        //           margin: EdgeInsets.only(top: 40),
        //           child: SvgPicture.asset(
        //             'images/LLog.svg',
        //             height: 100,
        //             width: 100,
        //           ),
        //         )),
        //     painter: CurvePainter(),
        //   ),
        // ),
        body: loading
            ? Loading(
                loadingMessage: 'مرحبا بك',
              )
            : Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: <Color>[Colors.white,viewAll])),
                child: SafeArea(
                    child: CustomScrollView(
                  slivers: [
                    SliverFillRemaining(
                      hasScrollBody: true,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          // Flexible(
                          //   flex: 0,
                          //   child: CustomPaint(
                          //     size: Size(MediaQuery.of(context).size.width,
                          //         MediaQuery.of(context).size.height),
                          //     child: Container(
                          //         // margin: EdgeInsets.fromLTRB(0, 100, 0, 0),

                          //         alignment: Alignment.topCenter,
                          //         child: Container(
                          //           margin: EdgeInsets.only(top: 40),
                          //           child: SvgPicture.asset(
                          //             'images/LLog.svg',
                          //             height: 100,
                          //             width: 100,
                          //           ),
                          //         )),
                          //     painter: CurvePainter(),
                          //   ),
                          // ),
                          Flexible(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.12,
                                ),
                                Center(
                                    child: SvgPicture.asset('images/logo.svg',
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.5)),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'مرحبا بك',
                                  style: kTitleTextStyle5.copyWith(color: Colors.white,fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                ),
                                ButtonTheme(
                                  minWidth:
                                      MediaQuery.of(context).size.width * 0.75,
                                  child: RaisedButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignInScreen()));
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    padding: const EdgeInsets.all(0.0),
                                    child: Ink(
                                      decoration: const BoxDecoration(
                                        gradient:
                                            LinearGradient(colors: <Color>[
                                          kColor2,
                                          kColor1,
                                        ]),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.75,
                                        height: 45,
                                        constraints: const BoxConstraints(
                                          minWidth: 10,
                                        ), // min sizes for Material buttons
                                        alignment: Alignment.center,
                                        child: const Text(
                                          'تسجيل الدخول',
                                          textAlign: TextAlign.center,
                                          style: kTitleTextStyle2,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                ButtonTheme(
                                  buttonColor: Colors.white,
                                  minWidth:
                                      MediaQuery.of(context).size.width * 0.75,
                                  child: RaisedButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignUpScreen()));
                                    },
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        side: BorderSide(
                                            color: kColor1,
                                            width: 1,
                                            style: BorderStyle.solid)),
                                    padding: const EdgeInsets.all(0.0),
                                    child: Ink(
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.75,
                                        height: 45,
                                        constraints: const BoxConstraints(
                                          minWidth: 10,
                                        ), // min sizes for Material buttons
                                        alignment: Alignment.center,
                                        child: const Text(
                                          'التسجيل',
                                          textAlign: TextAlign.center,
                                          style: kTitleTextStyle5,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )),
              ));
  }
}
