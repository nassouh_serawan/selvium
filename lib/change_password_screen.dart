import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:selvium/home_screen.dart';
import 'package:selvium/services/api.dart';
import 'package:selvium/widgets/network_widget.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_screen.dart';
import 'package:selvium/models/customer.dart';
class ChangePasswordScreen extends StatefulWidget {
  final int customerId;
  ChangePasswordScreen(this.customerId);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  Flushbar flush;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _passConfirm = TextEditingController();
  String validatePass(value){
    if (value.length < 6)
      return 'كلمة السر يجب ان تكون أكثر من 6 أحرف';
    else
      return null;
  }
  String validateConfPass(value){
    if(value != _pass.text)
      return 'كلمتي السر والتأكيد يجب أن تكونا متشابهتين';
    else
      return null;
  }
  changePassword()async{
    FocusScope.of(context).requestFocus(FocusNode());
    if(_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });
      var data = {"password": _pass.text};
      try{
        final response = await _provider.put(
            'wp-json/wc/v3/customers/' + widget.customerId.toString(),
            {ck: ckCode, cs: csCode},
            data);
        var cus = Customer.fromJson(response);
        if (cus != null) {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'كلمة المرور تغيرت',
              message: 'تم تغيير كلمة المرور بنجاح',
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.green,
              boxShadows: [
                BoxShadow(
                  color: Colors.green[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        } else {
          Navigator.pop(context);
          flush = Flushbar(
              title: 'حدث خطأ ما',
              message: 'الرجاء المحاولة مرة أخرى',
              duration: Duration(seconds: 3),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              backgroundColor: Colors.red,
              boxShadows: [
                BoxShadow(
                  color: Colors.red[800],
                  offset: Offset(0.0, 2.0),
                  blurRadius: 3.0,
                )
              ],
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
          setState(() {
            loading = false;
          });
        }
      }catch(e){
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: e.toString(),
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
        setState(() {
          loading = false;
        });
      }
    }else{setState(() {
      _autoValidate = true;
    });}
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar:AppBar(
          leading:IconButton(icon: SvgPicture.asset('images/Back-Arrow-Black.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Color(0xffffffff),
          title:Text('تغيير كلمة السر',style: kTitleTextStyle)
      ),
      body:loading?Loading(loadingMessage:'الرجاء الانتظار'): Container(
        height:
        MediaQuery.of(context).orientation==Orientation.portrait?
        MediaQuery.of(context).size.height>800?
        MediaQuery.of(context).size.height*0.35:
        MediaQuery.of(context).size.height*0.5:
        MediaQuery.of(context).size.width>800?
        MediaQuery.of(context).size.width*0.3:
        MediaQuery.of(context).size.width*0.4,

        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          color: Color(0xffffffff),
          borderRadius: BorderRadius.only(bottomRight:Radius.circular(20.0) ,bottomLeft:Radius.circular(20.0) ),
        ),
        child:SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child:
                  Form(
                    key: _formKey,
                    child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(height:
                        MediaQuery.of(context).size.height>900?
                        MediaQuery.of(context).size.height*0.05:
                        MediaQuery.of(context).size.height*0.015,),
                      Container(
                          decoration: kContainerDecoration,
                          child: TextFormField(
                            onSaved:(value) {
                              _pass.text=value;
                            },
                            obscureText: true,
                            validator: validatePass,
                            controller: _pass,
                            decoration: kInputContainerDecoration.copyWith(hintText:"كلمة السر الجديدة" ,hintStyle:kHintTextStyle ),)),
                      SizedBox(height: MediaQuery.of(context).size.height*0.005,),
                      Container(
                          decoration: kContainerDecoration,
                          child: TextFormField(
                            onSaved:(value) {
                              _passConfirm.text=value;
                            },
                            obscureText: true,
                            validator: validateConfPass,
                            controller: _passConfirm,
                            decoration: kInputContainerDecoration.copyWith(hintText:"تأكيد كلمة السر الجديدة" ,hintStyle:kHintTextStyle ),)),
                      SizedBox(height: MediaQuery.of(context).size.height*0.005,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FloatingActionButton(
                              child: Container(
                                  width: double.infinity,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          kColor2,kColor1
                                        ]),
                                  ),
                                  child: Icon(Icons.check)),
                              onPressed: changePassword,
                            ),
                          ),

                        ],
                      ),
                    ],),
                  ),

                ),
              )
            ],
          ),
        ) ,
      ),
    );
  }
}
