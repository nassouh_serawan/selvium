class Coupons {
  final List<Coupon> coupons;

  Coupons({
    this.coupons,
  });

  factory Coupons.fromJson(List<dynamic> parsedJson) {

    List<Coupon> coupons = new List<Coupon>();
    coupons = parsedJson.map((i)=>Coupon.fromJson(i)).toList();

    return new Coupons(
        coupons: coupons
    );
  }
}

class Coupon {
  int id;
  String code;
  String amount;
  String dateCreated;
  String dateCreatedGmt;
  String dateModified;
  String dateModifiedGmt;
  String discountType;
  String description;
  int usageCount;
  bool individualUse;
  int usageLimit;
  int usageLimitPerUser;
  bool freeShipping;
  bool excludeSaleItems;
  String minimumAmount;
  String maximumAmount;

  Links lLinks;
  Coupon(
      {this.id,
        this.code,
        this.amount,
        this.dateCreated,
        this.dateCreatedGmt,
        this.dateModified,
        this.dateModifiedGmt,
        this.discountType,
        this.description,

        this.usageCount,
        this.individualUse,

        this.usageLimit,
        this.usageLimitPerUser,
        this.freeShipping,

        this.excludeSaleItems,
        this.minimumAmount,
        this.maximumAmount,

        this.lLinks});

  Coupon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    amount = json['amount'];
    dateCreated = json['date_created'];
    dateCreatedGmt = json['date_created_gmt'];
    dateModified = json['date_modified'];
    dateModifiedGmt = json['date_modified_gmt'];
    discountType = json['discount_type'];
    description = json['description'];
    usageCount = json['usage_count'];
    individualUse = json['individual_use'];

    usageLimit = json['usage_limit'];
    usageLimitPerUser = json['usage_limit_per_user'];
    freeShipping = json['free_shipping'];


    excludeSaleItems = json['exclude_sale_items'];
    minimumAmount = json['minimum_amount'];
    maximumAmount = json['maximum_amount'];

    lLinks = json['_links'] != null ? new Links.fromJson(json['_links']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['amount'] = this.amount;
    data['date_created'] = this.dateCreated;
    data['date_created_gmt'] = this.dateCreatedGmt;
    data['date_modified'] = this.dateModified;
    data['date_modified_gmt'] = this.dateModifiedGmt;
    data['discount_type'] = this.discountType;
    data['description'] = this.description;

    data['usage_count'] = this.usageCount;
    data['individual_use'] = this.individualUse;


    data['usage_limit'] = this.usageLimit;
    data['usage_limit_per_user'] = this.usageLimitPerUser;

    data['free_shipping'] = this.freeShipping;


    data['exclude_sale_items'] = this.excludeSaleItems;
    data['minimum_amount'] = this.minimumAmount;
    data['maximum_amount'] = this.maximumAmount;


    if (this.lLinks != null) {
      data['_links'] = this.lLinks.toJson();
    }
    return data;
  }
}



class Links {
  List<Self> self;
  List<Self> collection;

  Links({this.self, this.collection});

  Links.fromJson(Map<String, dynamic> json) {
    if (json['self'] != null) {
      self = new List<Self>();
      json['self'].forEach((v) {
        self.add(new Self.fromJson(v));
      });
    }
    if (json['collection'] != null) {
      collection = new List<Self>();
      json['collection'].forEach((v) {
        collection.add(new Self.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.self != null) {
      data['self'] = this.self.map((v) => v.toJson()).toList();
    }
    if (this.collection != null) {
      data['collection'] = this.collection.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Self {
  String href;

  Self({this.href});

  Self.fromJson(Map<String, dynamic> json) {
    href = json['href'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['href'] = this.href;
    return data;
  }
}
