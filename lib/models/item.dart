class Item {
  int productId;
  int quantity;
  String key;
  Item({this.productId, this.quantity,this.key});
  Item.fromJson(Map<String, dynamic> json) {
    productId = json["product_id"];
    quantity = json["quantity"];
    key = json["key"];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["product_id"] = this.productId;
    data["quantity"] = this.quantity;
    data["key"] = this.key;
    return data;
  }
}


