class Products {
  final List<Product> products;

  Products({
    this.products,
  });

  factory Products.fromJson(List<dynamic> parsedJson) {
    List<Product> products = new List<Product>();
    products = parsedJson.map((i) => Product.fromJson(i)).toList();

    return new Products(products: products);
  }
}

class Product {
  int id;
  String name;
  String slug;
  String averageRating;
  int ratingCount;
  String type;
  String status;
  bool featured;

  String sku;
  String price;
  String regularPrice;
  String salePrice;
  String priceHtml;
  bool onSale;
  List<AttributesData> attributesData;
  List<Images> images;
  List<Attrs> attributes;
  Links lLinks;
  List<DefaultAttributes> defaultAttributes;
  List<int> variations;
  List<Cats> categories;
  List<Cats> tags;
  List<Cats> brands;
  List<int> relatedIds;
  Product(
      {this.id,
      this.name,
      this.slug,
      this.type,
      this.status,
      this.featured,
      this.onSale,
      this.sku,
      this.price,
      this.regularPrice,
      this.salePrice,
      this.relatedIds,
      this.priceHtml,
      this.images,
      this.categories,
      this.tags,
      this.brands,
      this.variations,
      this.averageRating,
      this.ratingCount,
      this.attributesData,
      this.lLinks});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];

    type = json['type'];
    status = json['status'];
    featured = json['featured'];
    onSale = json['on_sale'];
    sku = json['sku'];
    price = json['price'];
    regularPrice = json['regular_price'];
    salePrice = json['sale_price'];
    priceHtml = json['price_html'];
    averageRating = json['average_rating'];
    ratingCount = json['rating_count'];

    relatedIds = json['related_ids'].cast<int>();

    if (json['brands'] != null) {
      brands = new List<Cats>();
      json['brands'].forEach((v) {
        brands.add(new Cats.fromJson(v));
      });
    }

    variations = json['variations'].cast<int>();
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
    if (json['attributes'] != null) {
      attributes = new List<Attrs>();
      json['attributes'].forEach((v) {
        attributes.add(new Attrs.fromJson(v));
      });
    }
    if (json['default_attributes'] != null) {
      defaultAttributes = new List<DefaultAttributes>();
      json['default_attributes'].forEach((v) {
        defaultAttributes.add(new DefaultAttributes.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = new List<Cats>();
      json['categories'].forEach((v) {
        categories.add(new Cats.fromJson(v));
      });
    }
    if (json['tags'] != null) {
      tags = new List<Cats>();
      json['tags'].forEach((v) {
        tags.add(new Cats.fromJson(v));
      });
    }

    if (json['attributesData'] != null) {
      attributesData = new List<AttributesData>();
      json['attributesData'].forEach((v) {
        attributesData.add(new AttributesData.fromJson(v));
      });
    }
    lLinks = json['_links'] != null ? new Links.fromJson(json['_links']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;

    data['type'] = this.type;
    data['status'] = this.status;
    data['featured'] = this.featured;
    data['on_sale'] = this.onSale;
    data['sku'] = this.sku;
    data['price'] = this.price;
    data['regular_price'] = this.regularPrice;
    data['sale_price'] = this.salePrice;
    data['price_html'] = this.priceHtml;
    data['average_rating'] = this.averageRating;
    data['rating_count'] = this.ratingCount;

    if (this.brands != null) {
      data['brands'] = this.brands.map((v) => v.toJson()).toList();
    }

    if (this.tags != null) {
      data['tags'] = this.tags.map((v) => v.toJson()).toList();
    }
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    if (this.attributes != null) {
      data['attributes'] = this.attributes.map((v) => v.toJson()).toList();
    }
    if (this.defaultAttributes != null) {
      data['default_attributes'] =
          this.defaultAttributes.map((v) => v.toJson()).toList();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }

    data['related_ids'] = this.relatedIds;

    data['variations'] = this.variations;

    if (this.attributesData != null) {
      data['attributesData'] =
          this.attributesData.map((v) => v.toJson()).toList();
    }
    if (this.lLinks != null) {
      data['_links'] = this.lLinks.toJson();
    }
    return data;
  }
}

class Dimensions {
  String length;
  String width;
  String height;

  Dimensions({this.length, this.width, this.height});

  Dimensions.fromJson(Map<String, dynamic> json) {
    length = json['length'];
    width = json['width'];
    height = json['height'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['length'] = this.length;
    data['width'] = this.width;
    data['height'] = this.height;
    return data;
  }
}

class Cats {
  int id;
  String name;
  String slug;

  Cats({this.id, this.name, this.slug});

  Cats.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    return data;
  }
}

class Images {
  int id;
  String dateCreated;
  String dateCreatedGmt;
  String dateModified;
  String dateModifiedGmt;
  String src;
  String name;
  String alt;
  int position;

  Images(
      {this.id,
      this.dateCreated,
      this.dateCreatedGmt,
      this.dateModified,
      this.dateModifiedGmt,
      this.src,
      this.name,
      this.alt,
      this.position});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    dateCreated = json['date_created'];
    dateCreatedGmt = json['date_created_gmt'];
    dateModified = json['date_modified'];
    dateModifiedGmt = json['date_modified_gmt'];
    src = json['src'];
    name = json['name'];
    alt = json['alt'];
    position = json['position'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date_created'] = this.dateCreated;
    data['date_created_gmt'] = this.dateCreatedGmt;
    data['date_modified'] = this.dateModified;
    data['date_modified_gmt'] = this.dateModifiedGmt;
    data['src'] = this.src;
    data['name'] = this.name;
    data['alt'] = this.alt;
    data['position'] = this.position;
    return data;
  }
}

class Attrs {
  int id;
  String name;
  int position;
  bool visible;
  bool variation;
  List<String> options;

  Attrs(
      {this.id,
      this.name,
      this.position,
      this.visible,
      this.variation,
      this.options});

  Attrs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    position = json['position'];
    visible = json['visible'];
    variation = json['variation'];
    options = json['options'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['position'] = this.position;
    data['visible'] = this.visible;
    data['variation'] = this.variation;
    data['options'] = this.options;
    return data;
  }
}

class DefaultAttributes {
  int id;
  String name;
  String option;

  DefaultAttributes({this.id, this.name, this.option});

  DefaultAttributes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    option = json['option'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['option'] = this.option;
    return data;
  }
}

class MetaData {
  int id;
  String key;
  String value;

  MetaData({this.id, this.key, this.value});

  MetaData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['key'] = this.key;
    data['value'] = this.value;
    return data;
  }
}

class GpfData {
  Null description;
  Null availability;
  Null isBundle;
  Null availabilityDate;
  Null condition;
  Null brand;
  Null mpn;
  Null productType;
  Null googleProductCategory;
  Null taxCategory;
  Null gtin;
  Null gender;
  Null ageGroup;
  Null color;
  Null size;
  Null sizeType;
  Null sizeSystem;
  Null unitPricingMeasure;
  Null unitPricingBaseMeasure;
  Null multipack;
  Null installment;
  Null material;
  Null pattern;
  Null adult;
  Null identifierExists;
  Null adwordsGrouping;
  Null adwordsLabels;
  Null bingCategory;
  Null deliveryLabel;
  Null transitTimeLabel;
  Null minHandlingTime;
  Null maxHandlingTime;
  Null energyEfficiencyClass;
  Null minEnergyEfficiencyClass;
  Null maxEnergyEfficiencyClass;
  Null energyLabelImageLink;
  Null costOfGoodsSold;
  Null includedDestination;
  Null excludedDestination;
  Null consumerNotice;
  Null productHighlight;
  Null productDetail;
  Null consumerDatasheet;
  Null productFee;
  Null sellOnGoogleQuantity;
  Null purchaseQuantityLimit;
  Null googleFundedPromotionEligibility;
  Null returnAddressLabel;
  Null returnPolicyLabel;
  Null pickupMethod;
  Null pickupSla;
  Null customLabel0;
  Null customLabel1;
  Null customLabel2;
  Null customLabel3;
  Null customLabel4;
  Null promotionId;
  Null shippingprice;
  Null shippingcountryprice;
  Null shippingcountryserviceprice;
  Null excludeProduct;

  GpfData(
      {this.description,
      this.availability,
      this.isBundle,
      this.availabilityDate,
      this.condition,
      this.brand,
      this.mpn,
      this.productType,
      this.googleProductCategory,
      this.taxCategory,
      this.gtin,
      this.gender,
      this.ageGroup,
      this.color,
      this.size,
      this.sizeType,
      this.sizeSystem,
      this.unitPricingMeasure,
      this.unitPricingBaseMeasure,
      this.multipack,
      this.installment,
      this.material,
      this.pattern,
      this.adult,
      this.identifierExists,
      this.adwordsGrouping,
      this.adwordsLabels,
      this.bingCategory,
      this.deliveryLabel,
      this.transitTimeLabel,
      this.minHandlingTime,
      this.maxHandlingTime,
      this.energyEfficiencyClass,
      this.minEnergyEfficiencyClass,
      this.maxEnergyEfficiencyClass,
      this.energyLabelImageLink,
      this.costOfGoodsSold,
      this.includedDestination,
      this.excludedDestination,
      this.consumerNotice,
      this.productHighlight,
      this.productDetail,
      this.consumerDatasheet,
      this.productFee,
      this.sellOnGoogleQuantity,
      this.purchaseQuantityLimit,
      this.googleFundedPromotionEligibility,
      this.returnAddressLabel,
      this.returnPolicyLabel,
      this.pickupMethod,
      this.pickupSla,
      this.customLabel0,
      this.customLabel1,
      this.customLabel2,
      this.customLabel3,
      this.customLabel4,
      this.promotionId,
      this.shippingprice,
      this.shippingcountryprice,
      this.shippingcountryserviceprice,
      this.excludeProduct});

  GpfData.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    availability = json['availability'];
    isBundle = json['is_bundle'];
    availabilityDate = json['availability_date'];
    condition = json['condition'];
    brand = json['brand'];
    mpn = json['mpn'];
    productType = json['product_type'];
    googleProductCategory = json['google_product_category'];
    taxCategory = json['tax_category'];
    gtin = json['gtin'];
    gender = json['gender'];
    ageGroup = json['age_group'];
    color = json['color'];
    size = json['size'];
    sizeType = json['size_type'];
    sizeSystem = json['size_system'];
    unitPricingMeasure = json['unit_pricing_measure'];
    unitPricingBaseMeasure = json['unit_pricing_base_measure'];
    multipack = json['multipack'];
    installment = json['installment'];
    material = json['material'];
    pattern = json['pattern'];
    adult = json['adult'];
    identifierExists = json['identifier_exists'];
    adwordsGrouping = json['adwords_grouping'];
    adwordsLabels = json['adwords_labels'];
    bingCategory = json['bing_category'];
    deliveryLabel = json['delivery_label'];
    transitTimeLabel = json['transit_time_label'];
    minHandlingTime = json['min_handling_time'];
    maxHandlingTime = json['max_handling_time'];
    energyEfficiencyClass = json['energy_efficiency_class'];
    minEnergyEfficiencyClass = json['min_energy_efficiency_class'];
    maxEnergyEfficiencyClass = json['max_energy_efficiency_class'];
    energyLabelImageLink = json['energy_label_image_link'];
    costOfGoodsSold = json['cost_of_goods_sold'];
    includedDestination = json['included_destination'];
    excludedDestination = json['excluded_destination'];
    consumerNotice = json['consumer_notice'];
    productHighlight = json['product_highlight'];
    productDetail = json['product_detail'];
    consumerDatasheet = json['consumer_datasheet'];
    productFee = json['product_fee'];
    sellOnGoogleQuantity = json['sell_on_google_quantity'];
    purchaseQuantityLimit = json['purchase_quantity_limit'];
    googleFundedPromotionEligibility =
        json['google_funded_promotion_eligibility'];
    returnAddressLabel = json['return_address_label'];
    returnPolicyLabel = json['return_policy_label'];
    pickupMethod = json['pickup_method'];
    pickupSla = json['pickup_sla'];
    customLabel0 = json['custom_label_0'];
    customLabel1 = json['custom_label_1'];
    customLabel2 = json['custom_label_2'];
    customLabel3 = json['custom_label_3'];
    customLabel4 = json['custom_label_4'];
    promotionId = json['promotion_id'];
    shippingprice = json['shippingprice'];
    shippingcountryprice = json['shippingcountryprice'];
    shippingcountryserviceprice = json['shippingcountryserviceprice'];
    excludeProduct = json['exclude_product'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['availability'] = this.availability;
    data['is_bundle'] = this.isBundle;
    data['availability_date'] = this.availabilityDate;
    data['condition'] = this.condition;
    data['brand'] = this.brand;
    data['mpn'] = this.mpn;
    data['product_type'] = this.productType;
    data['google_product_category'] = this.googleProductCategory;
    data['tax_category'] = this.taxCategory;
    data['gtin'] = this.gtin;
    data['gender'] = this.gender;
    data['age_group'] = this.ageGroup;
    data['color'] = this.color;
    data['size'] = this.size;
    data['size_type'] = this.sizeType;
    data['size_system'] = this.sizeSystem;
    data['unit_pricing_measure'] = this.unitPricingMeasure;
    data['unit_pricing_base_measure'] = this.unitPricingBaseMeasure;
    data['multipack'] = this.multipack;
    data['installment'] = this.installment;
    data['material'] = this.material;
    data['pattern'] = this.pattern;
    data['adult'] = this.adult;
    data['identifier_exists'] = this.identifierExists;
    data['adwords_grouping'] = this.adwordsGrouping;
    data['adwords_labels'] = this.adwordsLabels;
    data['bing_category'] = this.bingCategory;
    data['delivery_label'] = this.deliveryLabel;
    data['transit_time_label'] = this.transitTimeLabel;
    data['min_handling_time'] = this.minHandlingTime;
    data['max_handling_time'] = this.maxHandlingTime;
    data['energy_efficiency_class'] = this.energyEfficiencyClass;
    data['min_energy_efficiency_class'] = this.minEnergyEfficiencyClass;
    data['max_energy_efficiency_class'] = this.maxEnergyEfficiencyClass;
    data['energy_label_image_link'] = this.energyLabelImageLink;
    data['cost_of_goods_sold'] = this.costOfGoodsSold;
    data['included_destination'] = this.includedDestination;
    data['excluded_destination'] = this.excludedDestination;
    data['consumer_notice'] = this.consumerNotice;
    data['product_highlight'] = this.productHighlight;
    data['product_detail'] = this.productDetail;
    data['consumer_datasheet'] = this.consumerDatasheet;
    data['product_fee'] = this.productFee;
    data['sell_on_google_quantity'] = this.sellOnGoogleQuantity;
    data['purchase_quantity_limit'] = this.purchaseQuantityLimit;
    data['google_funded_promotion_eligibility'] =
        this.googleFundedPromotionEligibility;
    data['return_address_label'] = this.returnAddressLabel;
    data['return_policy_label'] = this.returnPolicyLabel;
    data['pickup_method'] = this.pickupMethod;
    data['pickup_sla'] = this.pickupSla;
    data['custom_label_0'] = this.customLabel0;
    data['custom_label_1'] = this.customLabel1;
    data['custom_label_2'] = this.customLabel2;
    data['custom_label_3'] = this.customLabel3;
    data['custom_label_4'] = this.customLabel4;
    data['promotion_id'] = this.promotionId;
    data['shippingprice'] = this.shippingprice;
    data['shippingcountryprice'] = this.shippingcountryprice;
    data['shippingcountryserviceprice'] = this.shippingcountryserviceprice;
    data['exclude_product'] = this.excludeProduct;
    return data;
  }
}

class AttributesData {
  int id;
  String name;
  List<Options> options;
  int position;
  bool visible;
  bool variation;
  int isVisible;
  int isVariation;
  int isTaxonomy;
  String value;
  String label;

  AttributesData(
      {this.id,
      this.name,
      this.options,
      this.position,
      this.visible,
      this.variation,
      this.isVisible,
      this.isVariation,
      this.isTaxonomy,
      this.value,
      this.label});

  AttributesData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['options'] != null) {
      options = new List<Options>();
      json['options'].forEach((v) {
        options.add(new Options.fromJson(v));
      });
    }
    position = json['position'];
    visible = json['visible'];
    variation = json['variation'];
    isVisible = json['is_visible'];
    isVariation = json['is_variation'];
    isTaxonomy = json['is_taxonomy'];
    value = json['value'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.options != null) {
      data['options'] = this.options.map((v) => v.toJson()).toList();
    }
    data['position'] = this.position;
    data['visible'] = this.visible;
    data['variation'] = this.variation;
    data['is_visible'] = this.isVisible;
    data['is_variation'] = this.isVariation;
    data['is_taxonomy'] = this.isTaxonomy;
    data['value'] = this.value;
    data['label'] = this.label;
    return data;
  }
}

class Options {
  int termId;
  String name;
  String slug;
  int termGroup;
  int termTaxonomyId;
  String taxonomy;
  String description;
  int parent;
  int count;
  String filter;

  Options(
      {this.termId,
      this.name,
      this.slug,
      this.termGroup,
      this.termTaxonomyId,
      this.taxonomy,
      this.description,
      this.parent,
      this.count,
      this.filter});

  Options.fromJson(Map<String, dynamic> json) {
    termId = json['term_id'];
    name = json['name'];
    slug = json['slug'];
    termGroup = json['term_group'];
    termTaxonomyId = json['term_taxonomy_id'];
    taxonomy = json['taxonomy'];
    description = json['description'];
    parent = json['parent'];
    count = json['count'];
    filter = json['filter'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['term_id'] = this.termId;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['term_group'] = this.termGroup;
    data['term_taxonomy_id'] = this.termTaxonomyId;
    data['taxonomy'] = this.taxonomy;
    data['description'] = this.description;
    data['parent'] = this.parent;
    data['count'] = this.count;
    data['filter'] = this.filter;
    return data;
  }
}

class Links {
  List<Self> self;
  List<Self> collection;

  Links({this.self, this.collection});

  Links.fromJson(Map<String, dynamic> json) {
    if (json['self'] != null) {
      self = new List<Self>();
      json['self'].forEach((v) {
        self.add(new Self.fromJson(v));
      });
    }
    if (json['collection'] != null) {
      collection = new List<Self>();
      json['collection'].forEach((v) {
        collection.add(new Self.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.self != null) {
      data['self'] = this.self.map((v) => v.toJson()).toList();
    }
    if (this.collection != null) {
      data['collection'] = this.collection.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Self {
  String href;

  Self({this.href});

  Self.fromJson(Map<String, dynamic> json) {
    href = json['href'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['href'] = this.href;
    return data;
  }
}
