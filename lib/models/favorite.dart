class Favorites {
  final List<Favorite> favorites;

  Favorites({
    this.favorites,
  });

  factory Favorites.fromJson(List<dynamic> parsedJson) {

    List<Favorite> favorites = new List<Favorite>();
    favorites = parsedJson.map((i)=>Favorite.fromJson(i)).toList();

    return new Favorites(
        favorites: favorites
    );
  }
}
class Favorite {
  String iD;
  String prodId;
  String quantity;
  String userId;
  String wishlistId;
  String dateadded;
  String position;
  String originalPrice;
  String originalCurrency;
  String onSale;
  String productName;
  String image;

  Favorite(
      {this.iD,
        this.prodId,
        this.quantity,
        this.userId,
        this.wishlistId,
        this.dateadded,
        this.position,
        this.originalPrice,
        this.originalCurrency,
        this.onSale,
        this.productName,
        this.image});
  Favorite.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    prodId = json['prod_id'];
    quantity = json['quantity'];
    userId = json['user_id'];
    wishlistId = json['wishlist_id'];
    dateadded = json['dateadded'];
    position = json['position'];
    originalPrice = json['original_price'];
    originalCurrency = json['original_currency'];
    onSale = json['on_sale'];
    productName = json['product_name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['prod_id'] = this.prodId;
    data['quantity'] = this.quantity;
    data['user_id'] = this.userId;
    data['wishlist_id'] = this.wishlistId;
    data['dateadded'] = this.dateadded;
    data['position'] = this.position;
    data['original_price'] = this.originalPrice;
    data['original_currency'] = this.originalCurrency;
    data['on_sale'] = this.onSale;
    data['product_name'] = this.productName;
    data['image'] = this.image;
    return data;
  }
}
