class Notification {
  String id;
  String title;
  String message;
  String active;

  Notification({this.id, this.title, this.message, this.active});

  Notification.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    message = json['message'];
    active = json['active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['message'] = this.message;
    data['active'] = this.active;
    return data;
  }
}
