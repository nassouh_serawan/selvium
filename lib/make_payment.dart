import 'package:flutter/material.dart';
import 'package:myfatoorah_flutter/myfatoorah_flutter.dart';
import 'package:selvium/constant.dart';
import 'package:selvium/home_screen.dart';
import 'package:toast/toast.dart';
import 'package:flushbar/flushbar.dart';
import 'services/api.dart';

class MakePayment extends StatefulWidget {
  final String amount;
  final String orderId;
  MakePayment(this.amount, this.orderId);
  @override
  _MakePaymentState createState() => _MakePaymentState();
}

class _MakePaymentState extends State<MakePayment> {
  String mAPIKey =
      "ixKa1dKOWIA0cGqg3zLd28OGbCBfdwnu15A9sHuzSNPHULMWvv3GRlGsFrlDpai4bNKuAq8JEaJ1dgwVSG8oRhw9JcqhyUZhsVV7Qt8HnZ0zW83Gd2StN-k6CIOy-LF8TVkV6HodlqCjIfDqLJXJadLy41zlDMziK9V_s_VUeZBLnvqLb27bMhFzPSScO_Vn6a15npITn7LcVCrmGIt4JGUNLv8U1iwanWj7HYGUd4Dnt23snGGR4qBXyF-CP9eoRQCOi9SLIZyEYlTomcF5OVcA36pM4kZV8aCcQkF2cyzQlkeqLKPzitjvszm4hH-AUP1TfqhZwoYVxLp1PiM43tXrmk8rkahRuYVBAapOHlw3yIyh-IOz95Dp7GokA_OZXFWBxserb6Fm_yAITlvE4UDoGnldvBt6HhPHq84sL5fXwG3eWLYM2-daLN32GydLSONPyUOYEVVboamR2Kj2KjlRV9laAmD8ilXorL9sZVaFcnFD_9L1fowwan-0KL8dzKIvggyt_AL90HV1PiEjZPhSsqoSFYFBShXyfs-IcFXFwxa4Q_UzDdgRMSmtQAaVb9XcNOuH4ReonC2JIFY-Kk2y78U312K7m426CkUDtoksxf-f_0pS185GDCs4Rxl0DQjq1-d2-FXBtv08wA1VXFTqiXAXG_HU-LQdcpU22faT0QfaXDh5ObbtbLWSOCvUd42jwQ";
  String baseUrl = "https://api.myfatoorah.com";
  String baseTest = "https://apitest.myfatoorah.com";
  String keyTest =
      "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL";
  String _response = '';
  String _loading = "جاري تحميل البيانات";

  List<PaymentMethods> paymentMethods = [];
  List<bool> isSelected = [];
  int selectedPaymentMethodIndex = -1;

  String amount;
  String cardNumber = "2223000000000007";
  String expiryMonth = "5";
  String expiryYear = "21";
  String securityCode = "100";
  String cardHolderName = "Mahmoud Ibrahim";
  bool visibilityObs = false;
  @override
  void initState() {
    super.initState();
    setState(() {
      amount = widget.amount;
    });
    if (mAPIKey.isEmpty) {
      setState(() {
        _response =
            "Missing API Key.. You can get it from here: https://myfatoorah.readme.io/docs/demo-information";
      });
      return;
    }

    // TODO, don't forget to init the MyFatoorah Plugin with the following line
    MFSDK.init(baseUrl, mAPIKey);

    // (Optional) un comment the following lines if you want to set up properties of AppBar.

    MFSDK.setUpAppBar(
        title: "الدفع في سلفيوم",
        titleColor: Colors.white, // Color(0xFFFFFFFF)
        backgroundColor: Colors.red, // Color(0xFF000000)
        isShowAppBar: true); // For Android platform only

    // (Optional) un comment this line, if you want to hide the AppBar.
    // Note, if the platform is iOS, this line will not affected

//    MFSDK.setUpAppBar(isShowAppBar: false);

    initiatePayment();
  }

  /*
    Send Payment
   */
  void sendPayment() {
    var request = MFSendPaymentRequest(
        invoiceValue: double.parse(amount),
        customerName: "Customer name",
        notificationOption: MFNotificationOption.LINK);

    var invoiceItem =
        new InvoiceItem(itemName: "item1", quantity: 3, unitPrice: 3.5);
    var listItems = new List<InvoiceItem>();
    listItems.add(invoiceItem);
    request.invoiceItems = listItems;

    MFSDK.sendPayment(
        MFAPILanguage.EN,
        request,
        (MFResult<MFSendPaymentResponse> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(result.response.toJson());
                    _response = result.response.toJson().toString();
                  })
                }
              else
                {
                  setState(() {
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Initiate Payment
   */
  void initiatePayment() {
    var request = new MFInitiatePaymentRequest(
        double.parse(amount), MFCurrencyISO.SAUDI_ARABIA_SAR);

    MFSDK.initiatePayment(
        request,
        MFAPILanguage.AR,
        (MFResult<MFInitiatePaymentResponse> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(result.response.toJson());
                    _response = ""; //result.response.toJson().toString();
                    paymentMethods.addAll(result.response.paymentMethods);
                    for (int i = 0; i < paymentMethods.length; i++)
                      isSelected.add(false);
                  })
                }
              else
                {
                  setState(() {
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Execute Regular Payment
   */
  void executeRegularPayment(String paymentMethodId) {
    var request = new MFExecutePaymentRequest(paymentMethodId, amount);

    MFSDK.executePayment(
        context,
        request,
        MFAPILanguage.EN,
        (String invoiceId, MFResult<MFPaymentStatusResponse> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(invoiceId);
                    print(result.response.toJson());
                    updateOrder();
                  })
                }
              else
                {
                  setState(() {
                    print(invoiceId);
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Execute Direct Payment
   */
  void executeDirectPayment(String paymentMethodId) {
    var request = new MFExecutePaymentRequest(paymentMethodId, amount);

//    var mfCardInfo = new MFCardInfo(cardToken: "Put your token here");

    var mfCardInfo = new MFCardInfo(
        cardNumber: cardNumber,
        expiryMonth: expiryMonth,
        expiryYear: expiryYear,
        securityCode: securityCode,
        cardHolderName: cardHolderName,
        bypass3DS: true,
        saveToken: false);

    MFSDK.executeDirectPayment(
        context,
        request,
        mfCardInfo,
        MFAPILanguage.EN,
        (String invoiceId, MFResult<MFDirectPaymentResponse> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(invoiceId);
                    print(result.response.toJson());
                    _response = result.response.toJson().toString();
                  })
                }
              else
                {
                  setState(() {
                    print(invoiceId);
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Execute Direct Payment with Recurring
   */
  void executeDirectPaymentWithRecurring() {
    // The value "2" is the paymentMethodId of Visa/Master payment method.
    // You should call the "initiatePayment" API to can get this id and the ids of all other payment methods
    String paymentMethod = "2";

    var request = new MFExecutePaymentRequest(paymentMethod, amount);

    var mfCardInfo = new MFCardInfo(
        cardNumber: cardNumber,
        expiryMonth: expiryMonth,
        expiryYear: expiryYear,
        securityCode: securityCode,
        bypass3DS: true,
        saveToken: false);

    int intervalDays = 5;

    MFSDK.executeDirectPaymentWithRecurring(
        context,
        request,
        mfCardInfo,
        intervalDays,
        MFAPILanguage.EN,
        (String invoiceId, MFResult<MFDirectPaymentResponse> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(invoiceId);
                    print(result.response.toJson());
                    _response = result.response.toJson().toString();
                  })
                }
              else
                {
                  setState(() {
                    print(invoiceId);
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Payment Enquiry
   */
  void getPaymentStatus() {
    var request = MFPaymentStatusRequest(invoiceId: "12345");

    MFSDK.getPaymentStatus(
        MFAPILanguage.EN,
        request,
        (MFResult<MFPaymentStatusResponse> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(result.response.toJson());
                    _response = result.response.toJson().toString();
                  })
                }
              else
                {
                  setState(() {
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Cancel Token
   */
  void cancelToken() {
    MFSDK.cancelToken(
        "Put your token here",
        MFAPILanguage.EN,
        (MFResult<bool> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(result.response.toString());
                    _response = result.response.toString();
                  })
                }
              else
                {
                  setState(() {
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  /*
    Cancel Recurring Payment
   */
  void cancelRecurringPayment() {
    MFSDK.cancelRecurringPayment(
        "Put RecurringId here",
        MFAPILanguage.EN,
        (MFResult<bool> result) => {
              if (result.isSuccess())
                {
                  setState(() {
                    print(result.response.toString());
                    _response = result.response.toString();
                  })
                }
              else
                {
                  setState(() {
                    print(result.error.toJson());
                    _response = result.error.message;
                  })
                }
            });

    setState(() {
      _response = _loading;
    });
  }

  void setPaymentMethodSelected(int index, bool value) {
    for (int i = 0; i < isSelected.length; i++) {
      if (i == index) {
        isSelected[i] = value;
        if (value) {
          selectedPaymentMethodIndex = index;
          visibilityObs = paymentMethods[index].isDirectPayment;
        } else {
          selectedPaymentMethodIndex = -1;
          visibilityObs = false;
        }
      } else
        isSelected[i] = false;
    }
  }

  void pay() async {
    if (selectedPaymentMethodIndex == -1) {
      Toast.show("Please select payment method first", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
    } else {
      if (amount.isEmpty) {
        Toast.show("Set the amount", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
      } else if (paymentMethods[selectedPaymentMethodIndex].isDirectPayment) {
        if (cardNumber.isEmpty ||
            expiryMonth.isEmpty ||
            expiryYear.isEmpty ||
            securityCode.isEmpty)
          Toast.show("Fill all the card fields", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
        else
          executeDirectPayment(paymentMethods[selectedPaymentMethodIndex]
              .paymentMethodId
              .toString());
      } else
        executeRegularPayment(paymentMethods[selectedPaymentMethodIndex]
            .paymentMethodId
            .toString());
    }
  }

  updateOrder() async {
    Flushbar flush;
    ApiProvider _provider = ApiProvider();
    var data = {
      "set_paid": true,
    };
    try {
      final response = await _provider.put(
          'wp-json/wc/v3/orders/' + widget.orderId,
          {ck: ckCode, cs: csCode},
          data);
      if (response['id'] != null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()),
            (route) => false);
        flush = Flushbar(
            title: 'تمت العملية بنجاح',
            message: 'تمت عملية الدفع للطلب بنجاح',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.green,
            boxShadows: [
              BoxShadow(
                color: Colors.green[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()),
            (route) => false);
        flush = Flushbar(
            title: 'حدث خطأ ما',
            message: 'الرجاء المحاولة مرة أخرى',
            duration: Duration(seconds: 3),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            backgroundColor: Colors.red,
            boxShadows: [
              BoxShadow(
                color: Colors.red[800],
                offset: Offset(0.0, 2.0),
                blurRadius: 3.0,
              )
            ],
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
    } catch (e) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
      flush = Flushbar(
          title: 'حدث خطأ ما',
          message: e.toString(),
          duration: Duration(seconds: 3),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          backgroundColor: Colors.red,
          boxShadows: [
            BoxShadow(
              color: Colors.red[800],
              offset: Offset(0.0, 2.0),
              blurRadius: 3.0,
            )
          ],
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(10.0),
          child: Column(children: [
            TextField(
              keyboardType: TextInputType.number,
              enabled: false,
              controller: TextEditingController(text: amount + " ر.س"),
              decoration: InputDecoration(
                  labelText: "مبلغ الدفع", labelStyle: kTitleTextStyle),
              onChanged: (value) {
                amount = value;
              },
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Text(
              "الرجاء اختيار طريقة الدفع",
              style: kTitleTextStyle,
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: paymentMethods.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          leading: Image.network(paymentMethods[index].imageUrl,
                              width: 40.0, height: 40.0),
                          trailing: Checkbox(
                              activeColor: Colors.red,
                              value: isSelected[index],
                              onChanged: (bool value) {
                                setState(() {
                                  setPaymentMethodSelected(index, value);
                                });
                              }),
                        ),
                      );
                    })),
            visibilityObs
                ? Container(
                    child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(5.0),
                      ),
                      TextField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: "Card Number"),
                        controller: TextEditingController(text: cardNumber),
                        onChanged: (value) {
                          cardNumber = value;
                        },
                      ),
                      TextField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: "Expiry Month"),
                        controller: TextEditingController(text: expiryMonth),
                        onChanged: (value) {
                          expiryMonth = value;
                        },
                      ),
                      TextField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: "Expiry Year"),
                        controller: TextEditingController(text: expiryYear),
                        onChanged: (value) {
                          expiryYear = value;
                        },
                      ),
                      TextField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(labelText: "Security Code"),
                        controller: TextEditingController(text: securityCode),
                        onChanged: (value) {
                          securityCode = value;
                        },
                      ),
                      TextField(
                        keyboardType: TextInputType.name,
                        decoration:
                            InputDecoration(labelText: "Card Holder Name"),
                        controller: TextEditingController(text: cardHolderName),
                        onChanged: (value) {
                          cardHolderName = value;
                        },
                      ),
                    ],
                  ))
                : Container(),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: EdgeInsets.all(5.0),
                ),
                RaisedButton(
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Text('الدفع الآن'),
                  onPressed: () async {
                    pay();
                  },
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                ),
              ],
            ),
            Container(
              child: Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Text(_response),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
