import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selvium/account_screen.dart';
class BillingMethodsScreen extends StatefulWidget {
  @override
  _BillingMethodsScreenState createState() => _BillingMethodsScreenState();
}

class _BillingMethodsScreenState extends State<BillingMethodsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        leading:IconButton(icon: SvgPicture.asset('images/BackArrow-White-icon.svg',height: 20,),onPressed: (){Navigator.pop(context);},color: Colors.white,),
        title: Text('طرق الدفع',style: kTitleTextStyle3,),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[
                    kColor2,kColor1
                  ])
          ),
        ),


      ),
      backgroundColor: kBackgroundColor,
      body:SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
              child: Container(
                width:MediaQuery.of(context).size.width*0.9 ,
                height:
                MediaQuery.of(context).orientation==Orientation.portrait?
                MediaQuery.of(context).size.height>800&&MediaQuery.of(context).size.aspectRatio>0.2?
                MediaQuery.of(context).size.height*0.3:
                MediaQuery.of(context).size.height*0.42:
                MediaQuery.of(context).size.width>800&&MediaQuery.of(context).size.aspectRatio>0.2?
                MediaQuery.of(context).size.width*0.3:
                MediaQuery.of(context).size.width*0.4,
                child: Card(
                  shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text('البنك العقاري',style: TextStyle(fontSize: 20),),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text('رقم البطاقة',style: TextStyle(fontSize: 12,fontWeight: FontWeight.w100),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text('34567 **** **** ****',style: TextStyle(fontSize: 15),),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text('الشهر/السنة',style: TextStyle(fontSize: 12,fontWeight: FontWeight.w100),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20,bottom: 10),
                        child: Text('2020/2',style: TextStyle(fontSize: 15),),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width*0.09,
                                height: MediaQuery.of(context).size.height*0.055,
                                child: Image.asset('images/visa.png')),
                            Text('Mr. Mohammed',style: TextStyle(fontSize: 15),),
                          ],
                        ),
                      )

                    ],
                  ),
                ),
              ),
            ),

          ],
        ),
      )
    );
  }
}
