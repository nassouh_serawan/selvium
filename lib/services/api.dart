import 'custom_exception.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:selvium/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:selvium/models/category.dart';
class ApiProvider {
  final String _baseUrl = baseUrl;
  var token;
  _setHeaders() => {
    'Content-type' : 'application/json',
    'Accept' : 'application/json',
    'bearerAuth': '$token'
  };
  _setHeaders2() => {
    'Content-type' : 'application/x-www-form-urlencoded',
    //'Accept' : 'application/json',
    //'Authorization': 'Bearer $token'
  };
  _setHeaders3() => {
    'Content-type' : 'application/x-www-form-urlencoded',
    //'Accept' : 'application/json',
    'Authorization': 'Bearer $token'
  };
  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token=jsonDecode(localStorage.getString('token'));
    print(token);
  }
  Future<dynamic> get(String url,queryParameters) async {
    _getToken();
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.get(
          fullUrl,
          headers: _setHeaders(),
      );
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> get2(String url,queryParameters) async {
    _getToken();
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.get(
        fullUrl,
        headers: _setHeaders(),
      );
      responseJson = _responseB(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> getList(String url,queryParameters) async {
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
      return  await http.get(
        fullUrl,
        headers: _setHeaders(),
      );

  }
  Future<dynamic> put(String url,queryParameters,data) async {
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.put(
          fullUrl,
          headers: _setHeaders(),
          body: jsonEncode(data)
      );
      responseJson =  _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> post(String url,queryParameters,data) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token=jsonDecode(localStorage.getString('token'));
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.post(
          fullUrl,
          headers: _setHeaders2(),
          body: jsonEncode(data)
      );
      responseJson =  _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> post2(String url,queryParameters,data) async {
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.post(
          fullUrl,
          headers: _setHeaders(),
          body: jsonEncode(data)
      );
      responseJson =  _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> post3(String url,data) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token=jsonDecode(localStorage.getString('token'));
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url);
    print(fullUrl);
    try {
      final response = await http.post(
          fullUrl,
          headers: _setHeaders3(),
          body: data
      );
      responseJson =  _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> post4(String url,queryParameters,data) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token=jsonDecode(localStorage.getString('token'));
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.post(
          fullUrl,
          headers: _setHeaders2(),
          body: data
      );
      responseJson =  _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> register(String url,queryParameters,data) async {
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , url, queryParameters);
    try {
      final response = await http.post(
        fullUrl,
        headers: _setHeaders(),
        body: jsonEncode(data)
      );
      responseJson =  _responseAuth(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  Future<dynamic> login(data) async {
    var responseJson;
    var fullUrl = Uri.https(_baseUrl , 'wp-json/jwt-auth/v1/token');
    try {
      var response = await http.post(
        fullUrl,
        headers: {'Authorization': 'Basic cml5YWRoQGRpZ2l0YWxwbGFuLnNhOkYwR3MlJSVwajNVJiklRm95SygwaypeMA==',},
        body: data,
      );
      
      responseJson =  _responseAuth(response);

    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }
  dynamic _response(http.Response response) {
    var r;
    if(response.body.toString().length>11)
    {
      if (response.body.toString().substring(0, 11) == "bool(false)")
        r = response.body.toString().substring(11);
      else
        r = response.body.toString();
    }
    else{
      r=response.body.toString();
    }
    switch (response.statusCode) {
      case 200:
       print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 201:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 400:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 404:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 401:
        throw BadRequestException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 409:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 500:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
  dynamic _responseB(http.Response response) {
    var
      r=response.body.toString();
    switch (response.statusCode) {
      case 200:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 201:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 400:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 401:
        throw BadRequestException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 409:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      case 500:
        print(response.body);
        var responseJson = json.decode(r);
        return responseJson;
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
  dynamic _responseAuth(http.Response response) {

    switch (response.statusCode) {
      case 200:
        print(response.body);
        var responseJson = json.decode(response.body.toString().substring(11));
        return responseJson;
      case 201:
        print(response.body);
        var responseJson = json.decode(response.body.toString().substring(11));
        return responseJson;
      case 400:
        print(response.body);
        var responseJson = json.decode(response.body.toString().substring(11));
        return responseJson;
      case 401:
        throw BadRequestException(response.body.toString().substring(11));
      case 403:
        print(response.body);
        var responseJson = json.decode(response.body.toString().substring(0,response.body.toString().length-1));
        return responseJson;
      case 503:
        return {
          "success":false,
          "code": "incorrect_user",
        };

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
